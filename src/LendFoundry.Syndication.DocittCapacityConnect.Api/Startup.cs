﻿using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.DocittCapacityConnect.Proxy;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentManager.Client;
using Docitt.RequiredCondition.Client;
using Docitt.Questionnaire.Client;
using LendFoundry.TemplateManager.Client;
using Docitt.FileThis.Client;
using Docitt.BusinessRules.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;

namespace LendFoundry.Syndication.DocittCapacityConnect.Api
{
    internal class Startup
    {
        public Startup(IHostingEnvironment env) { }

        public void ConfigureServices(IServiceCollection services)
        {
               // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittCapacityConnect"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Syndication.DocittCapacityConnect.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // interface implements
            services.AddConfigurationService<DocittCapacityConnectConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<DocittCapacityConnectConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddLookupService();
            services.AddDataAttributes();
            services.AddDocumentManager();
            services.AddRequiredConditionService();
            services.AddQuestionnaireService();
            services.AddTemplateManagerService();
            services.AddDccFileThisService();
            services.AddBusinessRuleService();

            // Configuration factory
            services.AddTransient(p =>
            {
                var configuration = p.GetService<IConfigurationService<DocittCapacityConnectConfiguration>>().Get();
                return configuration as IDocittCapacityConnectConfiguration;
            });

            services.AddTransient<IDccProxyFactory, DccProxyFactory>();
            services.AddTransient<IDocittCapacityConnectProxy, DocittCapacityConnectProxy>();
            services.AddTransient<IDocittCapacityConnectService, DocittCapacityConnectService>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
             app.UseHealthCheck();
		     app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Docitt capacity connect service");
            });
            
            app.UseErrorHandling();
            app.UseRequestLogging(new RequestLoggingMiddlewareOptions(new string[] {"CustomerId","Secret","UserName","Pin","AccountId","ConnectionId","Password"}));
            app.UseMvc(); 
            app.UseConfigurationCacheDependency();
        }
    }
}
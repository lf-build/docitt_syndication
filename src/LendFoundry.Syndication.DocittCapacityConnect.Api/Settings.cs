﻿using System;

namespace LendFoundry.Syndication.DocittCapacityConnect.Api
{
    /// <summary>
    /// Settings class
    /// </summary>
    public class Settings
    {   
        /// <summary>
        /// ServiceName Property
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "docittcapacityconnect";        
    }
}
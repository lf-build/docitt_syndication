﻿using Docitt.RequiredCondition;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using LendFoundry.Syndication.DocittCapacityConnect.Proxy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace LendFoundry.Syndication.DocittCapacityConnect.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    [Route ("/")]
    public class ApiController : ExtendedController {
        private readonly IDocittCapacityConnectConfiguration Configuration;

        private static NoContentResult NoContentResult { get; } = new NoContentResult ();

        /// <summary>
        /// ApiController class
        /// </summary>
        /// <param name="service"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="lookup"></param>
        /// <returns></returns>
        public ApiController (
            IDocittCapacityConnectService service, 
            IDocittCapacityConnectConfiguration configuration, 
            ILogger logger, 
            IEventHubClient eventHubClient, 
            ILookupService lookup) : base (logger) {
            
            Configuration = configuration;
            // set for temporary basis
            Configuration.CompanyUserName = "testuser";
            Configuration.CompanyPassword = "Test@1235";
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private IDocittCapacityConnectService Service { get; }
        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        #region Docitt Capacity Connect

        #region Institutions

        /// <summary>
        /// Gets Institutions
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("institutions/{type?}")]
        [ProducesResponseType (typeof (IGetInstitutionResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Institutions ([FromRoute] string type) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.GetAllInstitutions (type);
                    return Ok (institutionResult);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }
                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetAllInstitutions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// Search Institutions
        /// </summary>
        /// <param name="type"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("institutions/{type}/search")]
        [ProducesResponseType (typeof (IGetInstitutionResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SearchInstitutions ([FromRoute] string type, [FromQuery] string keyword = "") {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.GetAllInstitutions (type, keyword);

                    return Ok (institutionResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetAllInstitutions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Search List Institutions
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [Route ("institutions/{type}/search/list")]
        [ProducesResponseType (typeof (IGetInstitutionResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SearchListInstitutions ([FromRoute] string type) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.GetSearchListOfInstitutions (type);

                    return Ok (institutionResult);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetAllInstitutions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// Get Institution By Id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("institutions/{type}/{institutionId}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> InstitutionById ([FromRoute] string type, [FromRoute] string institutionId) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.GetInstitutionDetailById (institutionId, type);

                    return Ok (institutionResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetInstitutionById() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Institutions Of Customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [Route ("customer/{customerId}/institution/{type?}")]
        [ProducesResponseType (typeof (IGetCustomerInstitutionResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> InstitutionsOfCustomer ([FromRoute] string customerId, [FromRoute] string type) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.GetAllInstitutionsOfCustomer (customerId, type);

                    return Ok (institutionResult);

                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method InstitutionsOfCustomer() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetAllInstitutionsOfCustomer() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// Gets ConnectionState
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route ("customer/{customerId}/institution/{type}/state/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetConnectionState ([FromRoute] string customerId, [FromRoute] string type, [FromBody] ConnectionStateRequest request, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var connectionStateResult = await Service.GetConnectionState (customerId, type, request, entityId);

                    return Ok (connectionStateResult);

                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method GetConnectionState() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.NoContent:
                                Response.StatusCode = StatusCodes.Status204NoContent;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetConnectionState() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        #region Company

        /// <summary>
        /// save Company
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>   
        [HttpPost ("company")]
        [ProducesResponseType (typeof (IGetCompanyByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Company ([FromBody] CompanyRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    var companyResult = await Service.CreateCompany (request);

                    return Ok (companyResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method CreateCompany() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Updates Invitation
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("company/{companyId}/invitation/")]
        [ProducesResponseType (typeof (IGetInvitationByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> UpdateInvitation ([FromRoute] string companyId, [FromBody] Invitation request) {
            return await ExecuteAsync (async () => {
                try {
                    var customerInvitationResult = await Service.UpdateCompanyInvitation (companyId, request);

                    return Ok (customerInvitationResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method UpdateCompanyInvitation() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Creates Company Configuration
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("company/{companyId}/invitation/configuration")]
        [ProducesResponseType (typeof (IGetCompanyConfigurationByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> CreateCompanyConfiguration ([FromRoute] string companyId, [FromBody] CompanyConfigurationRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    var companyConfigurationResult = await Service.CreateCompanyConfiguration (companyId, request);

                    return Ok (companyConfigurationResult);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method CreateCompanyConfiguration() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        #endregion

        #region Customer

        /// <summary>
        /// Save Customer
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("customer")]
        [ProducesResponseType (typeof (IGetCustomerByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Customer ([FromBody] CustomerRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    // CreateCustomerWithInvitationConfig
                    var customerResult = await Service.CreateCustomerWithInvitationConfig (Configuration.CompanyId, request);
                    return Ok (customerResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method CreateCustomer() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Gets Customer by username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [Route ("customer/{userName}")]
        [ProducesResponseType (typeof (IGetCustomerByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Customer ([FromRoute] string userName) {
            return await ExecuteAsync (async () => {
                try {
                    // CreateCustomerWithInvitationConfig
                    var customerResult = await Service.GetCustomer (Configuration.CompanyId, userName);

                    return Ok (customerResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomer() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Customer Invitation
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/invitation")]
        [ProducesResponseType (typeof (IGetInvitationByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> CustomerInvitation ([FromRoute] string customerId, [FromBody] InvitationRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    var customerInvitationResult = await Service.CreateCustomerInvitation (customerId, request);

                    return Ok (customerInvitationResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method CreateCustomerInvitation() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Request for Invitation Accept
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/invitation/accept")]
        [ProducesResponseType (typeof (IGetInvitationByIdResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> InvitationAccept ([FromRoute] string customerId, [FromBody] InvitationAcceptRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    var invitationResult = await Service.AcceptInvitation (customerId, request);

                    return Ok (invitationResult);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method AcceptInvitation() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Request for Invitation Approve
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="invitationId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/invitation/{invitationId}/approve")]
        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> InvitationApprove ([FromRoute] string customerId, [FromRoute] string invitationId) {
            return await ExecuteAsync (async () => {
                try {
                    var invitationResult = await Service.ApproveInvitation (customerId, invitationId);

                    return Ok (invitationResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method ApproveInvitation() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        #endregion

        /// <summary>
        /// Add Institution
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/institution/{type}/{institutionId}/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Institution ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string institutionId, [FromBody] InstitutionRequest request, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    Logger.Debug("Inside Institution");
                    var institutionResult = await Service.AddInstitution(customerId, type, institutionId, request, entityId);
                    return Ok (institutionResult);
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method Institution() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method AddInstitution() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Add Institution
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <param name="metaData"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/institution/{type}/{institutionId}/link/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> InstitutionLink ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string institutionId, [FromBody] object metaData, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.AddInstitutionLink(customerId, type, institutionId, metaData, entityId);
                    return Ok (institutionResult);

                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method InstitutionLink() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;

                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method AddInstitution() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Get Configuration Keys
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet ("type/{type}/configuration")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetConfigurations(string type) {
             return await ExecuteAsync (async () => {
                try {
                    var result = await Service.GetConfigurations(type);
                    return Ok (result);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomerLargeDepositTransactions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Update Institution
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPut ("customer/{customerId}/institution/{type}/{institutionId}/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> UpdateInstitution ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string institutionId, [FromBody] InstitutionRequest request, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.UpdateInstitution (customerId, type, institutionId, request, entityId);
                    return Ok (institutionResult);

                }
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method UpdateInstitution() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                } 
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method UpdateInstitution() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Map UserName
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpPost ("mapusername/customer/{customerId}/invite/{inviteId}")]
        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> MapUsername (string inviteId, string customerId) {
            return await ExecuteAsync (async () => {
                    var result = await Service.MapUsername (inviteId, customerId);

                    return Ok (result);
            });
        }

        /// <summary>
        /// Add ManualBank Insitution
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/institution/bank-detail/{entityId?}")]
        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> AddManualBankInsitution ([FromRoute] string customerId, [FromBody] BankAccountInfoRequest request, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var institutionResult = await Service.AddManualBank (customerId, request, entityId);

                    return Ok (institutionResult);

                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method AddManualBankInsitution() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                } 
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method AddManualBank() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Challenge
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="type"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("customer/{customerId}/institution/{institutionId}/{type}/challenge/{entityId}/account/{accountId}/connection/{connectionId}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Challenge ([FromRoute] string customerId, [FromRoute] string institutionId, [FromRoute] string type, [FromRoute] string entityId, [FromRoute] string accountId, [FromRoute] string connectionId) {
            return await ExecuteAsync (async () => {
                try {
                    var request = new ConnectionStateRequest();
                    request.AccountId = accountId;
                    request.ConnectionId = connectionId;
                    
                    var challengeResult = await Service.GetChallenges (institutionId, customerId, type, request, entityId);

                    return Ok (challengeResult);
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method Challenge() raised an error: {ex.Message}",ex);
                    throw new Exception($"Information not found");
                }
                catch (Exception ex) 
                {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetChallenges() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Reply Challenge
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <param name="type"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/institution/{type}/challenge/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> ReplyChallenge ([FromRoute] string customerId, [FromBody] object request, [FromRoute] string type, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var challengeResponseResult = await Service.ReplyChallenge(customerId, request, type, entityId);

                    return Ok (challengeResponseResult);
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method ReplyChallenge() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) 
                {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method ReplyChallenge() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Add Manual
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="files"></param>
        /// <param name="metadata"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/{type}/manual/{entityId?}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> AddManual ([FromRoute] string customerId, [FromRoute] string type, IFormFile[] files, string metadata, [FromRoute] string entityId) {
            IList<DocumentDetails> Documents = new List<DocumentDetails> ();
            //object metadataJson = null;
            //if (!string.IsNullOrWhiteSpace(metadata))
            //    metadataJson = JsonConvert.DeserializeObject<object>(metadata);

            foreach (var file in files) {
                MemoryStream ms = new MemoryStream ();
                file.OpenReadStream ().CopyTo (ms);
                ms.Position = 0;
                /* var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                                         .FileName
                                         .Trim();
*/
                var fileName = ContentDispositionHeaderValue
                    .Parse (file.ContentDisposition).FileName.ToString ().Trim ('"');

                Documents.Add (new DocumentDetails () { FileName = fileName.ToString (), Content = ms });
            }

            return await ExecuteAsync (async () => {
                await Service.ManualUploadWithMetaData (customerId, type, Documents, metadata, entityId);
                return NoContentResult;

            });

        }

        /// <summary>
        /// AllDone
        /// </summary>
        /// <param name="customerId">Applicant UserName</param>
        /// <param name="type">Institution Type</param>
        /// <param name="entityId">Application Number</param>
        /// <param name="request">payload</param>
        /// <returns></returns>
        [HttpPost ("customer/{customerId}/institution/{type}/all-done/{entityId?}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> AllDone ([FromRoute] string customerId, [FromRoute] string type, [FromBody] AllDoneRequest request, [FromRoute] string entityId) {

            return await ExecuteAsync (async () => {
                try {
                    await Service.AllDoneSendToRequiredConditions (customerId, type, request, entityId);
                    return Ok ("");
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method AllDone() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method AllDoneSendToRequiredConditions() raised an error: {message}");
                    throw new NotFoundException($"Information not found");
                }
            });

        }

        #endregion

        #region Login

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("login")]
        [ProducesResponseType (typeof (IGetLoginResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> Login ([FromBody] LoginRequest request) {
            return await ExecuteAsync (async () => {
                try {
                    var loginResult = await Service.GetLogin (new LoginRequest () { UserName = Configuration.CompanyUserName, Password = Configuration.CompanyPassword });

                    return Ok (loginResult);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetLogin() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        #endregion

        #region Accounts / documents

        /// <summary>
        /// SyncCustomerAccountDocument
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPut]
        [HttpGet]
        [Route ("customer/{customerId}/institution/{type}/{institutionId}/account/sync/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SyncCustomerAccountDocument ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string institutionId, [FromRoute] string entityId) {

            return await ExecuteAsync (async () => {
                try {
                    var result = new object ();
                    var accountResult = await Service.SyncCustomerAccountDocument (customerId, type, institutionId, entityId);

                    return Ok (accountResult);
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method SyncCustomerAccountDocument() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) 
                {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method SyncCustomerAccountDocument() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// SyncCustomerDocuments
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="institutionId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route ("customer/{customerId}/institution/{type}/{institutionId}/sync/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SyncCustomerDocuments ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string institutionId, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var result = new object ();
                    var accountResult = await Service.SyncCustomerDocuments (customerId, type, institutionId, entityId);

                    return Ok (accountResult);
                } 
                catch(System.Net.WebException ex)
                {   
                    Logger.Error ($"System.Net.WebException The method SyncCustomerDocuments() raised an error: {ex.Message}",ex);
                    throw new Exception("Some error has occurred");
                }
                catch (Exception ex) 
                {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method SyncCustomerDocuments() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// CustomerAccounts
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("customer/{customerId}/{type}/account/{applicationNumber?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> CustomerAccounts ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string applicationNumber) {
            InstitutionType typeOfInstitution = Service.EnsureInstitutionType (type);

            return await ExecuteAsync (async () => {
                try {
                    var customerAccountsResult = new object ();
                    switch (typeOfInstitution) {
                        case InstitutionType.Banking:
                        case InstitutionType.FileThisBanking:
                            customerAccountsResult = await Service.GetCustomerAccounts (customerId, type, applicationNumber);
                            break;
                        case InstitutionType.TaxReturn:
                        case InstitutionType.W2PayStub:
                            customerAccountsResult = await Service.GetCustomerDocumentInfo (customerId, type, entityId:applicationNumber);
                            break;
                        default:
                            break;
                    }

                    return Ok (customerAccountsResult);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomerAccounts() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });
        }

        /// <summary>
        /// Delete CustomerAccountDocument
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <param name="entiyType"></param>
        /// <param name="accountId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpDelete ("customer/{customerId}/{type}/{entiyType}/{accountId}/{entityId?}")]
        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> DeleteCustomerAccountDocument ([FromRoute] string customerId, [FromRoute] string type, [FromRoute] string entiyType, [FromRoute] string accountId, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var result = await Service.DeleteCustomerAccountDocument (customerId, type, entiyType, accountId, entityId);

                    return Ok (result);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method DeleteCustomerAccountDocument() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// Get Customer Account Transactions
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("customer/{customerId}/banking/account/{accountId}/transactions/{entityId?}")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetCustomerAccountTransactions ([FromRoute] string customerId, [FromRoute] string accountId, [FromRoute] string entityId) {
            return await ExecuteAsync (async () => {
                try {
                    var result = await Service.GetCustomerAccountTransactions (customerId, accountId, entityId);

                    return Ok (result);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomerAccountTransactions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// Get Customer LargeDeposit Transactions
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route ("customer/{customerId}/{applicationNumber}/transactions/large-deposits")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetCustomerLargeDepositTransactions ([FromRoute] string customerId, [FromRoute] string applicationNumber) {
            return await ExecuteAsync (async () => {
                try {
                    var result = await Service.GetCustomerLargeDepositTransactions (customerId, applicationNumber, true);

                    return Ok (result);
                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomerLargeDepositTransactions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        /// <summary>
        /// GetCustomerhasDepositTransactions
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [Route ("customer/{customerId}/{applicationNumber}/has-large-deposits")]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (object), 200)]
        public async Task<IActionResult> GetCustomerhasDepositTransactions ([FromRoute] string customerId, [FromRoute] string applicationNumber) {
            return await ExecuteAsync (async () => {
                try {
                    var result = await Service.GetCustomerLargeDepositTransactions (customerId, applicationNumber);

                    return Ok (result);

                } catch (Exception ex) {
                    if (ex is InvalidArgumentException || ex is ArgumentNullException) {
                        Logger.Error (ex.Message);
                        throw ex;
                    }
                    var message = ex.Message;

                    if (ex is DocittCapacityConnectNetworkException) {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        Logger.Error (networkEx.Response);
                        message = networkEx.Response;


                        switch (networkEx.HttpStatusCode) {
                            case System.Net.HttpStatusCode.UnsupportedMediaType:
                                Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;
                                return Content (networkEx.Response);
                            case System.Net.HttpStatusCode.BadRequest:
                                return BadRequest (networkEx.Response);
                            case System.Net.HttpStatusCode.NotFound:
                                return NotFound (networkEx.Response);
                            case System.Net.HttpStatusCode.Unauthorized:
                                return Unauthorized ();
                            case System.Net.HttpStatusCode.Forbidden:
                                Response.StatusCode = StatusCodes.Status403Forbidden;
                                return Content (networkEx.Response);
                        }

                    }

                    if (ex.InnerException is DocittCapacityConnectException) {
                        message = ((DocittCapacityConnectException) ex.InnerException).Message;
                    }

                    Logger.Error ($"The method GetCustomerLargeDepositTransactions() raised an error: {message}");
                    throw new NotFoundException ($"Information not found");
                }
            });

        }

        #endregion

        #endregion

    }
}
﻿
namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class CompanyRequest : ICompanyRequest
    {
        public string Name { get; set; }
        public string[] RequestedScopes { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class CustomerInstitutionRequest: ICustomerInstitutionRequest
    {
        public string CustomerId { get; set; }
        public string InvitationId { get; set; }
        public string InstitutionId { get; set; }
        public string Secert { get; set; }
        public string UserName { get; set; }
    }
}

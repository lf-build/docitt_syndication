﻿using Docitt.RequiredCondition;
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IDocittCapacityConnectService
    {
        #region Institutions

        Task<IGetInstitutionResponse> GetAllInstitutions( string type, string keyword = "");

        Task<IGetInstitutionResponse> GetSearchListOfInstitutions( string type);

        Task<IGetInstitutionByIdResponse> GetInstitutionById(string institutionId,  string type);

        Task<object> GetInstitutionDetailById(string institutionId,  string type);

        Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomer(string customerId,  string type);

        Task<object> GetConnectionState(string customerId, string type, IConnectionStateRequest request, string entityId = null);

        Task<IGetCompanyByIdResponse> CreateCompany(ICompanyRequest request);

        Task<IGetCustomerByIdResponse> CreateCustomer(ICustomerRequest request);

        Task<IGetCustomerByIdResponse> GetCustomer(string companyId, string userName);

        Task<IGetInvitationByIdResponse> CreateCustomerInvitation(string customerId, IInvitationRequest request);

        Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfiguration(string companyId, ICompanyConfigurationRequest request);

        Task<IGetInvitationByIdResponse> AcceptInvitation(string customerId, IInvitationAcceptRequest request);

        Task<string> ApproveInvitation(string customerId, string invitationId);

        Task<object> AddInstitution(string customerId, string type, string institutionId, IInstitutionRequest request, string entityId = null);

        Task<object> AddInstitutionLink(string customerId, string type, string institutionId, object metaData, string entityId = null);
        
        Task<object> UpdateInstitution(string customerId, string type, string institutionId, IInstitutionRequest request, string entityId = null);

        Task<bool> MapUsername(string inviteId,string username);

        Task<string> AddManualBank(string customerId, IBankAccountInfoRequest request, string entityId = null);

        Task<object> GetChallenges(string institutionId, string customerId,  string type, IConnectionStateRequest request, string entityId = null);

        Task<IGetInvitationByIdResponse> UpdateCompanyInvitation(string companyId, IInvitation request);

        Task<object> ReplyChallenge(string customerId, object request, string type, string entityId = null);

        Task<IGetCustomerAccountResponse> GetCustomerAccounts(string customerId,  string type, string applicationNumber = "");

        Task<bool> DeleteCustomerAccountDocument(string customerId,  string type, string entiyType, string accountId, string entityId = null);

        Task<object> GetCustomerAccountTransactions(string customerId,  string accountId, string entityId = null);

        Task<object> GetCustomerLargeDepositTransactions(string customerId,  string applicationNumber, bool returnTrnsactions = false);

        Task<object> GetCustomerDocumentInfo(string customerId, string type, string connectionId = default(string), string entityId = null);

        Task<Stream> DownloadCustomerDocument(string customerId,  string documentId);

        Task AllDoneSendToRequiredConditions(string customerId, string type,  IAllDoneRequest request, string entityId = null);

        Task<object> SyncCustomerAccountDocument(string customerId,  string type, string institutionId, string entityId);

        Task<object> SyncCustomerDocuments(string customerId,  string type, string institutionId, string entityId = null);

        #endregion

        #region Assets

        Task<IGetCustomerByIdResponse> CreateCustomerWithInvitationConfig(string companyId, ICustomerRequest request);


        #endregion

        #region Login
        Task<IGetLoginResponse> GetLogin(ILoginRequest request);

        #endregion

        #region "Manual"
        Task ManualUploadWithMetaData(string customerId, string type, IList<DocumentDetails> files, string metaData, string entityId = null);
        #endregion

        InstitutionType EnsureInstitutionType(string type);

        //Get the configuration information
        Task<object> GetConfigurations(string type);
    }
}

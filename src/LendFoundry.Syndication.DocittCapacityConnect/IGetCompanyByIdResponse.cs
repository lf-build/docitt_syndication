﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetCompanyByIdResponse
    {
        ICompany Result { get; set; }
    }
}

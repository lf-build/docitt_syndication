﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerTaxInfoResponse : IGetCustomerTaxInfoResponse
    {
        public IEnumerable<ICustomerTaxInfo> Result { get; set; }
    }
}

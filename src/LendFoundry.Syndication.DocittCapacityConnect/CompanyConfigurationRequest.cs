﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class CompanyConfigurationRequest : ICompanyConfigurationRequest
    {
        public int DaysOfData { get; set; }
        public string Name { get; set; }
        public bool RealDocs { get; set; }
        public string InvitationId { get; set; }
        public int VerifiedAssetsRequirement { get; set; }
        public string[] RulesList { get; set; }
    }
}

﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DocittCapacityConnectProxy : IDocittCapacityConnectProxy
    {
        public DocittCapacityConnectProxy(IDocittCapacityConnectConfiguration configuration,
                                        ITokenReader tokenReader, ITokenHandler tokenParser)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));
            if (tokenParser == null)
                throw new ArgumentNullException(nameof(tokenParser));

            Configuration = configuration;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            SetRestClient();
            //client = new RestClient();

            //Configuration.ServiceUrl = "http://docitt.dev.lendfoundry.com:5039"; // For local development
        }

        /// <summary>
        /// Base64AppendString
        /// </summary>
        private const string Base64AppendString = "data:image/png;base64,";

        private ITokenReader TokenReader;
        private ITokenHandler TokenParser;
        private IDocittCapacityConnectConfiguration Configuration { get; }

        private IRestClient client { get; set; }

        private void SetRestClient()
        {
            var baseUri = new Uri(Configuration.ServiceUrl);            
            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
            }
            else
            {
                client = new RestClient(baseUri);
            }
            var token = $"Bearer {TokenReader.Read()}";
            client.AddDefaultHeader("Authorization", token);
        }

        public async Task<object> GetConfigurations()
        {
            var restRequest = new RestRequest("publickey", Method.GET);
            var response = await Task.Run(() => client.Execute(restRequest));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<object>(response.Content);
            }
            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        #region Institutions

        public async Task<IGetInstitutionResponse> GetAllInstitutionsResponse(InstitutionType type, string keyword = "")
        {
            var restRequest = new RestRequest();
            if (string.IsNullOrEmpty(keyword))
            {
                restRequest = new RestRequest("institutions/feature", Method.POST) { };
            }
            else
            {
                restRequest = new RestRequest("institutions/search/{keyword}", Method.POST) { };
                restRequest.AddUrlSegment("keyword", keyword);
            }

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInstitutionResponse() { Result = JsonConvert.DeserializeObject<List<Institution>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInstitutionResponse> GetSearchListOfInstitutionsResponse(InstitutionType type)
        {
            var restRequest = new RestRequest();
            restRequest = new RestRequest("institutions/search/list", Method.POST) { };
            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInstitutionResponse() { Result = JsonConvert.DeserializeObject<List<Institution>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInstitutionByIdResponse> GetInstitutionByIdResponse(string institutionId, InstitutionType type)
        {
            var restRequest = new RestRequest("institutions/detail/{institutionId}", Method.POST) { };
            restRequest.AddUrlSegment("institutionId", institutionId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInstitutionByIdResponse() { Result = JsonConvert.DeserializeObject<Institution>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }
        public async Task<IInstitutionDetail> GetInstitutionDetailByIdResponse(string institutionId, InstitutionType type)
        {
            var restRequest = new RestRequest("institutions/detail/{institutionId}", Method.POST) { };
            restRequest.AddUrlSegment("institutionId", institutionId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = JsonConvert.DeserializeObject<InstitutionDetail>(response.Content);
                if (!string.IsNullOrEmpty(result.logo))
                    result.logo = $"{Base64AppendString}{result.logo}";

                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }
        public async Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomerResponse(string customerId, InstitutionType type)
        {
            var restRequest = new RestRequest("customer/{customerId}/institution", Method.GET) { };
            restRequest.AddUrlSegment("customerId", customerId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCustomerInstitutionResponse() { Result = JsonConvert.DeserializeObject<List<CustomerInstitution>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public Task<object> GetConnectionStateResponse(string customerId, InstitutionType type, IConnectionStateRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<IGetCompanyByIdResponse> CreateCompanyResponse(ICompanyRequest request)
        {
            var restRequest = new RestRequest("company", Method.POST) { };
            restRequest.AddJsonBody(request);
            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCompanyByIdResponse() { Result = JsonConvert.DeserializeObject<Company>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetLoginResponse> GetLoginResponse(ILoginRequest request)
        {
            var restRequest = new RestRequest("login", Method.POST) { };
            restRequest.AddJsonBody(request);
            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetLoginResponse() { Result = JsonConvert.DeserializeObject<LoginResponse>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInvitationResponse> GetAllInvitationsResponse(string companyId)
        {
            var restRequest = new RestRequest("company/{companyId}/invitation", Method.GET) { };
            restRequest.AddUrlSegment("companyId", companyId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationResponse() { Result = JsonConvert.DeserializeObject<List<Invitation>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetCustomerByIdResponse> CreateCustomerResponse(ICustomerRequest request)
        {
            var restRequest = new RestRequest("customer", Method.POST) { };
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCustomerByIdResponse() { Result = JsonConvert.DeserializeObject<Customer>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInvitationByIdResponse> CreateCustomerInvitationResponse(string customerId, IInvitationRequest request)
        {
            var restRequest = new RestRequest("customer/{customerId}/invitation", Method.POST) { };
            restRequest.AddUrlSegment("customerId", customerId);
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationByIdResponse() { Result = JsonConvert.DeserializeObject<Invitation>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfigurationResponse(string companyId, ICompanyConfigurationRequest request)
        {
            var restRequest = new RestRequest("company/{companyId}/invitation/configuration", Method.POST) { };
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddUrlSegment("companyId", companyId);
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCompanyConfigurationByIdResponse() { Result = JsonConvert.DeserializeObject<CompanyConfiguration>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInvitationByIdResponse> AcceptInvitationResponse(string customerId, IInvitationAcceptRequest request)
        {
            var restRequest = new RestRequest("customer/{customerId}/invitation/accept", Method.POST) { };
            restRequest.AddUrlSegment("customerId", customerId);
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationByIdResponse() { Result = JsonConvert.DeserializeObject<Invitation>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> AddInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request, InstitutionType typeOfInstitution)
        {
            var restRequest = new RestRequest("item/create", Method.POST) { };
            restRequest.AddJsonBody(new { applicant_id = customerId, institution_id = institutionId, username = request.UserName, password = request.Secret, pin = request.Pin });

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = JsonConvert.DeserializeObject<InstitutionAccount>(response.Content);
                result.Logo = $"{Base64AppendString}{result.Logo}";
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> AddInstitutionResponseLink(string customerId, string institutionId, object metaData, string entityId = null)
        {
            string restUrl = "item/create/plaidlink";
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                restUrl = restUrl + "/{entityId}";
            }

            var restRequest = new RestRequest(restUrl, Method.POST) { };
            restRequest.AddParameter("application/json", metaData, ParameterType.RequestBody);
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                restRequest.AddUrlSegment("entityId", entityId);
            }
            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = JsonConvert.DeserializeObject<InstitutionAccount>(response.Content);
                result.Logo = $"{Base64AppendString}{result.Logo}";
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> UpdateInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request)
        {
            var restRequest = new RestRequest("item/update", Method.POST) { };
            restRequest.AddJsonBody(new { applicant_id = customerId, plaid_item_id = request.PlaidItemId, institution_id = institutionId, username = request.UserName, password = request.Secret, pin = request.Pin });

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<InstitutionAccount>(response.Content);
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<string> ApproveInvitationResponse(string customerId, string invitationId)
        {
            var restRequest = new RestRequest("customer/{customerId}/invitation/{invitationId}/approve", Method.POST) { };
            restRequest.AddUrlSegment("customerId", customerId);
            restRequest.AddUrlSegment("invitationId", invitationId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> GetChallengesResponse(string applicationNumber, string customerId, string institutionId = default(string), IConnectionStateRequest request = default(ConnectionStateRequest))
        {
            var restRequest = new RestRequest("customer/{customerId}/institution/challenge/GetChallenges", Method.GET) { };
            restRequest.AddUrlSegment("customerId", customerId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetChallengeResponse() { Result = JsonConvert.DeserializeObject<List<Challenge>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetInvitationByIdResponse> UpdateCompanyInvitationResponse(string companyId, IInvitation request)
        {
            var restRequest = new RestRequest("company/{companyId}/invitation", Method.POST) { };
            restRequest.AddUrlSegment("companyId", companyId);
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationByIdResponse() { Result = JsonConvert.DeserializeObject<Invitation>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> ReplyChallengeResonse(string applicationNumber, string customerId, object request, InstitutionType type)
        {
            IMfaReplyRequest mfaRequest = (IMfaReplyRequest)request;

            var restRequest = new RestRequest("item/mfa", Method.POST) { };
            restRequest.AddJsonBody(new { public_token = mfaRequest.PublicToken, mfa_type = mfaRequest.MfaType, responses = mfaRequest.Answers, applicant_id = mfaRequest.CustomerId, institution_id = mfaRequest.InstitutionId });

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = JsonConvert.DeserializeObject<InstitutionAccount>(response.Content);
                result.Logo = $"{Base64AppendString}{result.Logo}";
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetCustomerAccountResponse> GetCustomerAccountsResponse(string customerId, InstitutionType type)
        {
            var restRequest = new RestRequest("item/accounts/all/{applicant_id}", Method.POST) { };
            restRequest.AddUrlSegment("applicant_id", customerId);

            var response = await Task.Run(() => client.Execute(restRequest));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCustomerAccountResponse() { Result = JsonConvert.DeserializeObject<List<object>>(response.Content) };
            }


            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<bool> DeleteCustomerAccountDocumentResponse(string customerId, InstitutionType type, string entityId)
        {
            var restRequest = new RestRequest("item/account/delete", Method.POST) { };
            dynamic objRequest = new System.Dynamic.ExpandoObject();
            objRequest.applicant_id = customerId;
            objRequest.plaid_account_id = entityId;

            var json = restRequest.JsonSerializer.Serialize(objRequest);

            restRequest.AddParameter("text/json", json, ParameterType.RequestBody);

            var response = await Task.Run(() => client.Execute(restRequest));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<List<IAccountTransaction>> GetCustomerAccountTransactionsResponse(string customerId, IBankAccountTransactionRequest request, string entityId = null)
        {
            string restUrl = "transaction/get";
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                restUrl = restUrl + "/{entityId}";
            }

            var restRequest = new RestRequest(restUrl, Method.POST) { };
            var json = JsonConvert.SerializeObject(request);
            restRequest.AddParameter("text/json", json, ParameterType.RequestBody);
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                restRequest.AddUrlSegment("entityId", entityId);
            }
            var response = await Task.Run(() => client.Execute(restRequest));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<AccountTransaction>>(response.Content).Cast<IAccountTransaction>().ToList();
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> SyncCustomerAccountDocumentResponse(string customerId, string institutionId, string entityId)
        {
            var restRequest = new RestRequest("item/accounts/sync", Method.PUT) { };
            restRequest.AddJsonBody(new { applicantId = customerId, institutionId = institutionId, accountId = entityId });

            var response = await Task.Run(() => client.Execute(restRequest));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<SyncAccount>(response.Content);
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public Task<object> GetCustomerDocumentInfoResponse(string customerId, InstitutionType type, string connectionId = default(string))
        {
            throw new NotImplementedException();
        }

        public Task<Stream> DownloadCustomerDocumentResponse(string customerId, string documentId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteCustomerConnectionResponse(string accountId, string connectionId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetFileThisIdFromPreApplication
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>string</returns>
        public Task<string> GetFileThisIdFromPreApplication(string plaidInstitutionId)
        {
            throw new NotImplementedException();
        }
        #endregion

        public async Task<IGetCustomerByIdResponse> GetCustomerResponse(string userName, string companyId)
        {
            var invitations = await Task.Run(() => this.GetAllInvitationsResponse(companyId));

            if (invitations != null && invitations.Result != null)
            {
                if (invitations.Result.Any(x => x.Email == userName && x.IsAccepted))
                {
                    ICustomer matchedCustomer = new Customer();

                    var invitation = invitations.Result.Where(x => x.Email == userName).FirstOrDefault();
                    matchedCustomer.Email = invitation.Email;
                    matchedCustomer.CustomerId = invitation.CustomerId;
                    matchedCustomer.FirstName = invitation.FirstName;
                    matchedCustomer.LastName = invitation.LastName;
                    matchedCustomer.InvitationId = invitation.Id;

                    return await Task.Run(() => new GetCustomerByIdResponse() { Result = matchedCustomer });
                }
            }

            return await Task.Run(() => new GetCustomerByIdResponse() { Result = new Customer() });
        }
    }
}

﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DocittCapacityConnectSimulatorProxy : IDocittCapacityConnectProxy
    {
        public DocittCapacityConnectSimulatorProxy(IDocittCapacityConnectConfiguration configuration, IDocumentManagerService documentManagerClient, ITenantTime tenantTime)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (documentManagerClient == null)
                throw new ArgumentNullException(nameof(documentManagerClient));
            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));

            Configuration = configuration;
            _sample = new DccSimulator(documentManagerClient, tenantTime, configuration);
        }

        private IDocumentManagerService DocumentManagerClient { get; set; }


        private IDocittCapacityConnectConfiguration Configuration { get; }

        private DccSimulator _sample;

        #region Institutions

        public async Task<IGetInstitutionResponse> GetAllInstitutionsResponse( InstitutionType type, string keyword = "")
        {
            var result = await _sample.GetInstitutions(type, keyword);

            return new GetInstitutionResponse() { Result = result };

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting list of institutions." };
        }

        public Task<object> GetConfigurations()
        {
            throw new NotImplementedException();
        }

        public async Task<IGetInstitutionResponse> GetSearchListOfInstitutionsResponse( InstitutionType type)
        {
            var result = await _sample.GetInstitutions(type);

            return new GetInstitutionResponse() { Result = result };

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting list of institutions." };
        }

        public async Task<IGetInstitutionByIdResponse> GetInstitutionByIdResponse(string institutionId,  InstitutionType type)
        {
            var _result = await _sample.GetInstitutions(type);

            if (_result != null)
            {
                return new GetInstitutionByIdResponse() { Result = _result.FirstOrDefault(l => l.InstitutionId == institutionId) };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("Institution with given institutionId: {0} does not exist.", institutionId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting institution." };
        }

        public async Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomerResponse(string customerId,  InstitutionType type)
        {
            var _result = await _sample.GetAllInstitutionsOfCustomer(customerId, type);

            if (_result != null)
            {
                return new GetCustomerInstitutionResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No institutions found for given customerId: {0} ", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting institution." };
        }

        public Task<object> GetConnectionStateResponse(string customerId, InstitutionType type, IConnectionStateRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<IInstitutionDetail> GetInstitutionDetailByIdResponse(string institutionId,  InstitutionType type)
        {
            var _result = await _sample.GetInstitutions(type);

            if (_result != null)
            {
                if (type == InstitutionType.Banking)
                {
                    var institutionDetail = _result.FirstOrDefault(a => a.InstitutionId == institutionId);
                    var sampleInstitutionDetail = JsonConvert.DeserializeObject<InstitutionDetail>(Configuration.SampleBankDetailResponse);
                    sampleInstitutionDetail.institutionId = institutionDetail.InstitutionId;
                    sampleInstitutionDetail.logo = institutionDetail.Logo;
                    sampleInstitutionDetail.name = institutionDetail.Name;

                    return sampleInstitutionDetail;
                }

                return null;
                ////Rahul to check-- return new GetInstitutionByIdResponse() { Result = _result.Where(l => (l.Type == type || type == InstitutionType.All) && l.InstitutionId == institutionId).FirstOrDefault() };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("Institution with given institutionId: {0} does not exist.", institutionId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting institution." };

        }

        public async Task<IGetCompanyByIdResponse> CreateCompanyResponse(ICompanyRequest request)
        {
            var baseUri = new Uri(Configuration.ServiceUrl);
            RestClient client;
            var restRequest = new RestRequest("company", Method.POST) { };

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
            }
            else
            {
                client = new RestClient(baseUri);
            }

            restRequest.AddJsonBody(request);
            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetCompanyByIdResponse() { Result = JsonConvert.DeserializeObject<Company>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetLoginResponse> GetLoginResponse(ILoginRequest request)
        {
            var _result = await _sample.Login(request);

            if (_result != null)
            {
                return new GetLoginResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("Username or Passowrd is not valid.") };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in login." };
        }

        public async Task<IGetInvitationResponse> GetAllInvitationsResponse(string companyId)
        {
            var baseUri = new Uri(Configuration.ServiceUrl);
            RestClient client;
            var restRequest = new RestRequest("company/{companyId}/invitation", Method.GET) { };

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
            }
            else
            {
                client = new RestClient(baseUri);
            }
            
            restRequest.AddUrlSegment("companyId", companyId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationResponse() { Result = JsonConvert.DeserializeObject<List<Invitation>>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<IGetCustomerByIdResponse> CreateCustomerResponse(ICustomerRequest request)
        {
            var _result = await _sample.CreateCustomer(request);

            if (_result != null)
            {
                return new GetCustomerByIdResponse() { Result = _result };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in creating customer." };
        }

        public async Task<IGetInvitationByIdResponse> CreateCustomerInvitationResponse(string customerId, IInvitationRequest request)
        {
            var _result = await _sample.CreateCustomerInvitation(customerId, request);

            if (_result != null)
            {
                return new GetInvitationByIdResponse() { Result = _result };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in creating invitation." };

        }

        public async Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfigurationResponse(string companyId, ICompanyConfigurationRequest request)
        {
            var _result = await _sample.CreateCompanyConfiguration();

            if (_result != null)
            {
                return new GetCompanyConfigurationByIdResponse() { Result = _result };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in creating company configuration." };
        }

        public async Task<IGetInvitationByIdResponse> AcceptInvitationResponse(string customerId, IInvitationAcceptRequest request)
        {
            var _result = await _sample.AcceptInvitation(customerId);

            if (_result != null)
            {
                return new GetInvitationByIdResponse() { Result = _result };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in accepting invitation." };

        }

        public async Task<object> AddInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request,  InstitutionType typeOfInstitution)
        {

            var _result = await _sample.AddInstitution(customerId, institutionId, request, typeOfInstitution);

            if (_result != null)
            {
                return _result;
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("Institution: {0} does not exist.", institutionId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in adding institution." };
        }

        public Task<object> AddInstitutionResponseLink(string customerId, string institutionId, object metaData, string entityId = null)
        {
            throw new NotImplementedException();
        }

        public Task<object> UpdateInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<string> ApproveInvitationResponse(string customerId, string invitationId)
        {
            var baseUri = new Uri(Configuration.ServiceUrl);
            RestClient client;
            var restRequest = new RestRequest("customer/{customerId}/invitation/{invitationId}/approve", Method.POST) { };

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
            }
            else
            {
                client = new RestClient(baseUri);
            }

            
            restRequest.AddUrlSegment("customerId", customerId);
            restRequest.AddUrlSegment("invitationId", invitationId);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> GetChallengesResponse(string applicationNumber, string customerId,  string institutionId = default(string), IConnectionStateRequest request = default(ConnectionStateRequest))
        {
            var _result = await _sample.GetChallenge(customerId);

            if (_result != null)
            {
                return new GetChallengeResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No Challenges found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting challenges." };
        }

        public async Task<IGetInvitationByIdResponse> UpdateCompanyInvitationResponse(string companyId, IInvitation request)
        {
            var baseUri = new Uri(Configuration.ServiceUrl);
            RestClient client;
            var restRequest = new RestRequest("company/{companyId}/invitation", Method.POST) { };

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
            }
            else
            {
                client = new RestClient(baseUri);
            }

            
            restRequest.AddUrlSegment("companyId", companyId);
            restRequest.AddJsonBody(request);

            var response = await Task.Run(() => client.Execute(restRequest));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return new GetInvitationByIdResponse() { Result = JsonConvert.DeserializeObject<Invitation>(response.Content) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = response.StatusCode, Response = response.Content };
        }

        public async Task<object> ReplyChallengeResonse(string applicationNumber, string customerId, object request,  InstitutionType type)
        {
            var _result = await _sample.PostChallengeReply(customerId, request, type);

            if (_result != null)
            {
                return _result;
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No Challenges found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in posting challenge." };
        }

        public async Task<IGetCustomerAccountResponse> GetCustomerAccountsResponse(string customerId,  InstitutionType type)
        {
            var _result = await _sample.GetCustomerAccounts(customerId, type);

            if (_result != null)
            {
                return new GetCustomerAccountResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No Accounts found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting accounts." };

        }

        public async Task<bool> DeleteCustomerAccountDocumentResponse(string customerId,  InstitutionType type, string entityId)
        {
            try
            {
                await _sample.DeleteCustomerAccountDocument(customerId, type, entityId);
                return true;
            }
            catch (DocittCapacityConnectNetworkException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DocittCapacityConnectNetworkException(ex) { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in deleting account / document." };
            }
        }

        public Task<List<IAccountTransaction>> GetCustomerAccountTransactionsResponse(string customerId,  IBankAccountTransactionRequest request, string entityId = null)
        {
            throw new NotImplementedException();
        }

        public async Task<object> SyncCustomerAccountDocumentResponse(string customerId,  string institutionId, string entityId)
        {
            var result = await _sample.SyncCustomerAccountDocumentResponse(customerId, institutionId, entityId);
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No Accounts found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting accounts." };
        }

        public async Task<object> GetCustomerDocumentInfoResponse(string customerId, InstitutionType type, string connectionId = default(string))
        {

            var _result = await _sample.GetCustomerTaxW2Info(customerId, type);

            if (_result != null)
            {
                return new GetCustomerDocumentInfoResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No Accounts found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting accounts." };

        }

        public async Task<Stream> DownloadCustomerDocumentResponse(string customerId, string documentId)
        {
            var _result = await _sample.DownloadDocument(customerId, documentId);

            if (_result != null)
            {
                return _result;
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No document found for customer: {0}.", customerId) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting document." };

        }

        public Task<bool> DeleteCustomerConnectionResponse(string accountId, string connectionId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetFileThisIdFromPreApplication
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>string</returns>
        public Task<string> GetFileThisIdFromPreApplication(string plaidInstitutionId)
        {
            throw new NotImplementedException();
        }

        #endregion

        public async Task<IGetCustomerByIdResponse> GetCustomerResponse(string userName,  string companyId = "")
        {
            var _result = await _sample.GetCustomer(userName);

            if (_result != null)
            {
                return new GetCustomerByIdResponse() { Result = _result };
            }
            else
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("Customer with given username: {0} does not exist.", userName) };
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.InternalServerError, Response = "Error in getting customer." };
        }
    }
}

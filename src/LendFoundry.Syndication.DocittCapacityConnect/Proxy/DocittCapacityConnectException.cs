﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DocittCapacityConnectException : Exception
    {
        public Dictionary<string, string> Errors { get; set; }
    }
}
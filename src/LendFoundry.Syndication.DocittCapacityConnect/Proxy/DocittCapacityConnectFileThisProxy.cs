﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Docitt.FileThis.Abstractions;
using Docitt.FileThis;
using Newtonsoft.Json;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DocittCapacityConnectFileThisProxy : IDocittCapacityConnectProxy
    {

        public DocittCapacityConnectFileThisProxy(
            IDocittCapacityConnectConfiguration configuration, 
            IDccFileThisService fileThisClient,
            ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (fileThisClient == null)
                throw new ArgumentNullException(nameof(fileThisClient));

            Configuration = configuration;
            this.FileThisClient = fileThisClient;
            this.Logger = logger;
            //Configuration.ServiceUrl = "http://docitt.dev.lendfoundry.com:5039"; // For local development
        }

        private IDocittCapacityConnectConfiguration Configuration { get; }
        private IDccFileThisService FileThisClient { get; }
        private ILogger Logger {get;}

        #region Institutions
        //// Institution list by search keyword with category type - Filethis
        /// <summary>
        /// GetAllInstitutionsResponse
        /// </summary>
        /// <param name="type">type</param>
        /// <param name="keyword">keyword</param>
        /// <returns></returns>
        public async Task<IGetInstitutionResponse> GetAllInstitutionsResponse( InstitutionType type, string keyword = "")
        {
            var typeString = this.InstitutionTypeString(type);
            var result = await this.FileThisClient.GetCategorySearch(typeString, keyword);

            if (result != null)
            {
                return this.ConvertInstitutions(result);
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public Task<object> GetConfigurations()
        {
            throw new NotImplementedException();
        }

        //// Institution feature list by category type - Filethis -NO NEED RAHUL WILL MANAGE BY DATA attribute
        /// <summary>
        /// GetSearchListOfInstitutionsResponse
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<IGetInstitutionResponse> GetSearchListOfInstitutionsResponse( InstitutionType type)
        {
            var result = await this.FileThisClient.GetCategorySources(type.ToString());

            if (result != null)
            {
                return this.ConvertInstitutions(result);
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }


        public Task<IGetInstitutionByIdResponse> GetInstitutionByIdResponse(string institutionId,  InstitutionType type)
        {
            throw new NotImplementedException();
        }

        public Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomerResponse(string customerId,  InstitutionType type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetConnectionStateResponse
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="type">type</param>
        /// <param name="request">request</param>
        /// <returns>return state</returns>
        public async Task<object> GetConnectionStateResponse(string customerId, InstitutionType type, IConnectionStateRequest request)
        {
            Logger.Debug("FileThisClient: GetConnectionStateResponse");
            var response = await FileThisClient.GetLatestChange(request.AccountId, request.ConnectionId);

            if (response != null)
            {
                return response.Resource;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NoContent, Response = string.Empty };
        }

        ////  Institution detail by id - Filethis
        /// <summary>
        /// GetInstitutionByIdResponse
        /// </summary>
        /// <param name="institutionId">institutionId</param>
        /// <param name="type">type</param>
        /// <returns></returns>
        public async Task<IInstitutionDetail> GetInstitutionDetailByIdResponse(string institutionId,  InstitutionType type)
        {
            var result = await this.FileThisClient.GetSourceById(institutionId);

            if (result != null)
            {
                IInstitutionDetail institutionDetail = this.ConvertInstitutionDetail(result);
                return institutionDetail;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public Task<IGetCompanyByIdResponse> CreateCompanyResponse(ICompanyRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IGetLoginResponse> GetLoginResponse(ILoginRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IGetInvitationResponse> GetAllInvitationsResponse(string companyId)
        {
            throw new NotImplementedException();
        }

        public Task<IGetCustomerByIdResponse> CreateCustomerResponse(ICustomerRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IGetInvitationByIdResponse> CreateCustomerInvitationResponse(string customerId, IInvitationRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfigurationResponse(string companyId, ICompanyConfigurationRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IGetInvitationByIdResponse> AcceptInvitationResponse(string customerId, IInvitationAcceptRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<string> ApproveInvitationResponse(string customerId, string invitationId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AddInstitutionResponse
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="institutionId">institutionId</param>
        /// <param name="request">request</param>
        /// <param name="token">token</param>
        /// <param name="typeOfInstitution">typeOfInstitution</param>
        /// <returns></returns>
        public async Task<object> AddInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request,  InstitutionType typeOfInstitution)
        {
            Logger.Debug("FileThis Client: AddInstitutionResponse");
            var requestConnection = this.CreatingRequestConnection(request);
            var result = await this.FileThisClient.CreateConnection(requestConnection);

            if (result != null)
            {
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public Task<object> AddInstitutionResponseLink(string customerId, string institutionId, object metaData, string entityId = null)
        {
            throw new NotImplementedException();
        }

        public Task<object> UpdateInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetChallengesResponse
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="token">token</param>
        /// <returns></returns>
        public async Task<object> GetChallengesResponse(
                string applicationNumber, 
                string customerId,  
                string institutionId = default(string), 
                IConnectionStateRequest request = default(ConnectionStateRequest))
        {
            try
            {
                Logger.Debug("FileThisClient: GetChanllengesResponse...");
                var interactionRequest = CreateInteractionRequest(applicationNumber, customerId, institutionId, request);
                var result = await FileThisClient.InteractionRequest(interactionRequest);

                if (result != null)
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetChallengesResponse",ex);
                throw ex;
            }
            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public Task<IGetInvitationByIdResponse> UpdateCompanyInvitationResponse(string companyId, IInvitation request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ReplyChallengeResonse
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="request">request</param>
        /// <param name="token">token</param>
        /// <param name="type">type</param>
        /// <returns></returns>
        public async Task<object> ReplyChallengeResonse(string applicationNumber, string customerId, object request,  InstitutionType type)
        {
            var interactionRequest = CreateInteractionReply(applicationNumber, customerId, (FileThisMfaReplyRequest)request);
            bool result = false;
            try
            {
                result = await FileThisClient.InteractionResponse(interactionRequest);
            }
            catch(Exception ex)
            {
                Logger.Error($"Error in ReplyChallengeResponse for application {applicationNumber}",ex);
            }
            
            return result;
        }

        public Task<IGetCustomerAccountResponse> GetCustomerAccountsResponse(string customerId,  InstitutionType type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteCustomerAccountDocumentResponse
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="token">token</param>
        /// <param name="type">type</param>
        /// <param name="entityId">entityId</param>
        /// <returns>True or false</returns>
        public async Task<bool> DeleteCustomerAccountDocumentResponse(string customerId,  InstitutionType type, string entityId)
        {
            return await FileThisClient.DeleteDocument(entityId);
        }

        public Task<List<IAccountTransaction>> GetCustomerAccountTransactionsResponse(string customerId,  IBankAccountTransactionRequest request, string entityId = null)
        {
            throw new NotImplementedException();
        }

        public async Task<object> SyncCustomerAccountDocumentResponse(string customerId,  string institutionId, string entityId)
        {
            var result = await FileThisClient.ConnectionSyncDocuments(entityId); // entityId => connectionId
            if (result != null)
            {
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public async Task<object> GetCustomerDocumentInfoResponse(string customerId, InstitutionType type, string connectionId = default(string))
        {
            var result = await FileThisClient.GetDocuments(connectionId);
            if (result != null)
            {
                return result;
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = System.Net.HttpStatusCode.NotFound, Response = string.Empty };
        }

        public Task<Stream> DownloadCustomerDocumentResponse(string customerId, string documentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteCustomerConnectionResponse
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>True or false</returns>
        public async Task<bool> DeleteCustomerConnectionResponse(string accountId, string connectionId)
        {
            return await this.FileThisClient.DeleteConnection(accountId, connectionId);
        }

        #endregion

        public Task<IGetCustomerByIdResponse> GetCustomerResponse(string userName,  string companyId = "")
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetFileThisIdFromPreApplication
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>string</returns>
        public async Task<string> GetFileThisIdFromPreApplication(string plaidInstitutionId)
        {
            return await this.FileThisClient.GetFileThisSourceId(plaidInstitutionId);
        }

        #region Private methods

        /// <summary>
        /// ConvertInstitutions
        /// </summary>
        /// <param name="sourceList">sourceList</param>
        /// <returns>Institution List</returns>
        private IGetInstitutionResponse ConvertInstitutions(IList<ResponseSource> sourceList)
        {
            IGetInstitutionResponse result = new GetInstitutionResponse();

            var institutions = new List<Institution>();
            foreach (var sourceItem in sourceList)
            {
                var institution = new Institution();

                institution.InstitutionId = sourceItem.SourceId;
                institution.Logo = sourceItem.Logo;
                institution.Name = sourceItem.SourceName;

                institutions.Add(institution);
            }

            result.Result = institutions;

            return result;
        }

        /// <summary>
        /// ConvertInstitutionDetail
        /// </summary>
        /// <param name="sourceDetail">sourceDetail</param>
        /// <returns></returns>
        private IInstitutionDetail ConvertInstitutionDetail(IResponseSourceDetail sourceDetail)
        {
            var credentialList = new List<Credential>();
            foreach (var item in sourceDetail.Credential)
            {
                var credential = new Credential()
                {
                    label = item.Label,
                    name = item.Name,
                    type = item.Type
                };
                credentialList.Add(credential);
            }

            return new InstitutionDetail
            {
                institutionId = sourceDetail.SourceId,
                name = sourceDetail.SourceName,
                logo = sourceDetail.Logo,
                credentials = credentialList
            };
        }

        /// <summary>
        /// CreatingRequestConnection
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        private RequestConnection CreatingRequestConnection(IInstitutionRequest request)
        {
            return new RequestConnection
            {
                ApplicantId = request.CustomerId,
                Username = EncodeTo64(request.UserName),
                Password = EncodeTo64(request.Secret),
                SourceId = request.InstitutionId,
                ApplicationNumber = request.ApplicationNumber
            };
        }

        /// <summary>
        /// CreatingRequestInteraction
        /// </summary>
        /// <returns></returns>
        private RequestInteraction CreateInteractionRequest(
                    string applicationNumber, 
                    string customerId, 
                    string institutionId, 
                    IConnectionStateRequest request)
        {
            return new RequestInteraction
            {
                ApplicantId = customerId,
                AccountId = request.AccountId,
                ConnectionId = request.ConnectionId,
                SourceId = institutionId,
                InteractionId = null,
                Inputs = null,
                ApplicationNumber = applicationNumber
            };
        }

        private RequestInteraction CreateInteractionReply(string applicationNumber, string customerId, IFileThisMfaReplyRequest request)
        {
            return new RequestInteraction
            {
                ApplicantId = customerId,
                AccountId = request.AccountId,
                ConnectionId = request.ConnectionId,
                Inputs = request.Inputs,
                InteractionId = request.InteractionId,
                ApplicationNumber = applicationNumber
            };
        }


        /// <summary>
        /// InstitutionTypeString
        /// </summary>
        /// <param name="type">type</param>
        /// <returns>string</returns>
        private string InstitutionTypeString(InstitutionType type)
        {
            switch (type)
            {
                case InstitutionType.TaxReturn:
                    return "TaxReturns";
                case InstitutionType.W2PayStub:
                    return "PaystubAndW2s";
                case InstitutionType.FileThisBanking:
                    return "Banking";
            }
            return string.Empty;
        }

        /// <summary>
        /// Encode string to base64
        /// </summary>
        /// <param name="toEncode"></param>
        /// <returns></returns>
        private string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.ASCII.GetBytes(toEncode);
            string returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        /// <summary>
        /// Decode base64 string
        /// </summary>
        /// <param name="encodedData"></param>
        /// <returns></returns>
        private string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        #endregion
    }
}

﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Configuration;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DccProxyFactory : IDccProxyFactory
    {
        private IDocumentManagerService DocumentManagerClient { get; set; }
        private ITenantTime TenantTime { get; }
        private IDccFileThisService FileThisClient { get; }
        private ITokenReader TokenReader{get;}
        private ITokenHandler TokenParser{get;}
        private IConfigurationService<DocittCapacityConnectConfiguration> Configuration { get; }
        private IDictionary<bool, Func<object, InstitutionType, IDocittCapacityConnectProxy>> Proxies { get; }
        private ILogger Logger {get;}
        public DccProxyFactory(
            IConfigurationService<DocittCapacityConnectConfiguration> configuration, 
            IDocumentManagerService documentManagerClient,
            ITenantTime tenantTime, 
            IDccFileThisService fileThisClient,
            ITokenReader tokenReader, 
            ITokenHandler tokenParser,
            ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (documentManagerClient == null)
                throw new ArgumentNullException(nameof(documentManagerClient));
            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));
            if (fileThisClient == null)
                throw new ArgumentNullException(nameof(fileThisClient));
            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));
            if (tokenParser == null)
                throw new ArgumentNullException(nameof(tokenParser));

            Configuration = configuration;
            Proxies = BuildProxies();
            DocumentManagerClient = documentManagerClient;
            TenantTime = tenantTime;
            FileThisClient = fileThisClient;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            Logger = logger;
        }

        private IDictionary<bool, Func<object, InstitutionType, IDocittCapacityConnectProxy>> BuildProxies()
        {
            return new Dictionary<bool, Func<object, InstitutionType, IDocittCapacityConnectProxy>>
            {
                {true, BuildSimulatorProxy},
                {false, BuildDccProxy }
            };
        }



        private IDocittCapacityConnectProxy BuildDccProxy(object settings, InstitutionType typeOfInstitution)
        {
            var proxySettings = Newtonsoft.Json.Linq.JObject.FromObject(settings).ToObject<DocittCapacityConnectConfiguration>();

            switch (typeOfInstitution)
            {
                case InstitutionType.All:
                case InstitutionType.Banking:
                    return new DocittCapacityConnectProxy(proxySettings,TokenReader, TokenParser);
                case InstitutionType.TaxReturn:
                case InstitutionType.W2PayStub:
                case InstitutionType.FileThisBanking:
                    return new DocittCapacityConnectFileThisProxy(proxySettings, FileThisClient, Logger);

                default:
                    return new DocittCapacityConnectProxy(proxySettings,TokenReader, TokenParser);
            }

        }

        private IDocittCapacityConnectProxy BuildSimulatorProxy(object settings, InstitutionType typeOfInstitution)
        {
            var simulatorSettings = Newtonsoft.Json.Linq.JObject.FromObject(settings).ToObject<DocittCapacityConnectConfiguration>();
            return new DocittCapacityConnectSimulatorProxy(simulatorSettings, DocumentManagerClient, TenantTime);
        }

        public IDocittCapacityConnectProxy GetProxy(InstitutionType type)
        {
            var dccConfiguration = Configuration.Get();
            //dccConfiguration.UseSimulatorFor[InstitutionType.TaxReturn] = false;
            //dccConfiguration.UseSimulatorFor[InstitutionType.W2PayStub] = false;

            var proxy = Proxies[type == InstitutionType.All ? true : dccConfiguration.UseSimulatorFor[type]];

            if (proxy == null)
                throw new ArgumentException("No proxy exists as defined in configuration");

            return proxy(dccConfiguration, type);
        }
    }
}

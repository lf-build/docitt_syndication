﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public interface IDocittCapacityConnectSimulatorProxy
    {
        #region Institutions

        Task<IGetInstitutionResponse> GetAllInstitutionsResponse(string token, InstitutionType type);
        Task<IGetInstitutionByIdResponse> GetInstitutionByIdResponse(string institutionId, string token, InstitutionType type);

        Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomerResponse(string customerId, string token, InstitutionType type);


        Task<IGetCompanyByIdResponse> CreateCompanyResponse(ICompanyRequest request);

        Task<IGetLoginResponse> GetLoginResponse(ILoginRequest request);

        Task<IGetInvitationResponse> GetAllInvitationsResponse(string companyId, string token);

        Task<IGetCustomerByIdResponse> CreateCustomerResponse(ICustomerRequest request, string token);

        Task<IGetInvitationByIdResponse> CreateCustomerInvitationResponse(string customerId, IInvitationRequest request, string token);

        Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfigurationResponse(string companyId, ICompanyConfigurationRequest request, string token);

        Task<IGetInvitationByIdResponse> AcceptInvitationResponse(string customerId, IInvitationAcceptRequest request, string token);

        Task<string> ApproveInvitationResponse(string customerId, string invitationId, string token);

        Task<string> AddInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request, string token);

        Task<IGetChallengeResponse> GetChallengesResponse(string customerId, string token);

        Task<IGetInvitationByIdResponse> UpdateCompanyInvitationResponse(string companyId, IInvitation request, string token);

        Task<string> ReplyChallengeResonse(string customerId, IChallengeReplyRequest request, string token);

        Task<IGetCustomerAccountResponse> GetCustomerAccountsResponse(string customerId, string token, InstitutionType type);

        Task<IGetCustomerDocumentInfoResponse> GetCustomerDocumentInfoResponse(string customerId, string token, InstitutionType type);

        #endregion

        Task<IGetCustomerByIdResponse> GetCustomerResponse(string userName, string token);

    }
}

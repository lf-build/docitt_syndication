﻿using System;
using System.Net;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public class DocittCapacityConnectNetworkException : Exception
    {
        public DocittCapacityConnectNetworkException()
        {
            
        }

        public DocittCapacityConnectNetworkException(Exception innerException):base(innerException.Message,innerException)
        {
            
        }
        
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Response { get; set; }
    }
}
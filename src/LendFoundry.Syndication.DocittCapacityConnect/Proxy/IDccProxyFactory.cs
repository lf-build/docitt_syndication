﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public interface IDccProxyFactory
    {
        IDocittCapacityConnectProxy GetProxy(InstitutionType type);
    }
}

﻿
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.Proxy
{
    public interface IDocittCapacityConnectProxy
    {
        #region Institutions

        Task<IGetInstitutionResponse> GetAllInstitutionsResponse( InstitutionType type, string keyword = "");

        Task<object> GetConfigurations();

        Task<IGetInstitutionResponse> GetSearchListOfInstitutionsResponse( InstitutionType type);

        Task<IGetInstitutionByIdResponse> GetInstitutionByIdResponse(string institutionId,  InstitutionType type);

        Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomerResponse(string customerId,  InstitutionType type);

        Task<object> GetConnectionStateResponse(string customerId, InstitutionType type, IConnectionStateRequest request);

        Task<IInstitutionDetail> GetInstitutionDetailByIdResponse(string institutionId,  InstitutionType type);

        Task<IGetCompanyByIdResponse> CreateCompanyResponse(ICompanyRequest request);

        Task<IGetLoginResponse> GetLoginResponse(ILoginRequest request);

        Task<IGetInvitationResponse> GetAllInvitationsResponse(string companyId);

        Task<IGetCustomerByIdResponse> CreateCustomerResponse(ICustomerRequest request);

        Task<IGetInvitationByIdResponse> CreateCustomerInvitationResponse(string customerId, IInvitationRequest request);

        Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfigurationResponse(string companyId, ICompanyConfigurationRequest request);

        Task<IGetInvitationByIdResponse> AcceptInvitationResponse(string customerId, IInvitationAcceptRequest request);

        Task<string> ApproveInvitationResponse(string customerId, string invitationId);

        Task<object> AddInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request,  InstitutionType typeOfInstitution);

        Task<object> AddInstitutionResponseLink(string customerId, string institutionId, object metaData, string entityId = null);

        Task<object> UpdateInstitutionResponse(string customerId, string institutionId, IInstitutionRequest request);

        Task<object> GetChallengesResponse(string applicationNumber, string customerId,  string institutionId = default(string), IConnectionStateRequest request = default(ConnectionStateRequest));

        Task<IGetInvitationByIdResponse> UpdateCompanyInvitationResponse(string companyId, IInvitation request);

        Task<object> ReplyChallengeResonse(string applicationNumber, string customerId, object request,  InstitutionType type);

        Task<IGetCustomerAccountResponse> GetCustomerAccountsResponse(string customerId,  InstitutionType type);

        Task<bool> DeleteCustomerAccountDocumentResponse(string customerId,  InstitutionType type, string entityId);

        Task<List<IAccountTransaction>> GetCustomerAccountTransactionsResponse(string customerId,  IBankAccountTransactionRequest request, string entityId = null);

        Task<object> SyncCustomerAccountDocumentResponse(string customerId,  string institutionId, string entityId);

        Task<object> GetCustomerDocumentInfoResponse(string customerId, InstitutionType type, string connectionId = default(string));

        Task<Stream> DownloadCustomerDocumentResponse(string customerId, string documentId);

        Task<bool> DeleteCustomerConnectionResponse(string accountId, string connectionId);

        Task<string> GetFileThisIdFromPreApplication(string plaidInstitutionId);

        #endregion

        Task<IGetCustomerByIdResponse> GetCustomerResponse(string userName,  string companyId = "");
    }
}

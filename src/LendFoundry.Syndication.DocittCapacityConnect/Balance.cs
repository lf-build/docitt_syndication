﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IBalance
    {
        string Available { get; set; }
        string Current { get; set; }
    }

    public class Balance : IBalance
    {
        public string Available { get; set; }
        public string Current { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class InvitationRequest : IInvitationRequest
    {
        public string CompanyDisplayName { get; set; }
        public string CompanyId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NotificationType { get; set; }
        public string PhoneNumber { get; set; }
        public string Template { get; set; }
    }
}

﻿using LendFoundry.DocumentManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IAccountDocument
    {
        IDocument DocumentInfo { get; set; }
        bool IsSubmitted { get; set; }

        [JsonProperty("Name")]
        string AccountNumber { get; set; } // Account No
        string Date { get; set; }
        string FileId { get; set; }
        string DocumentId { get; set; }
    }
}

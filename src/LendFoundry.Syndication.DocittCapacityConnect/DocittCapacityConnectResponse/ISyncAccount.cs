﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ISyncAccount
    {
        string Plaid_Item_Id { get; set; }
        Account Account { get; set; }
        bool Has_Error { get; set; }
        PlaidError Error { get; set; }
        bool IsCredentialsChanged { get; set; }
        bool IsSuccess { get; set; }
    }
}

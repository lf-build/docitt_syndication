﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IPlaidLink
    {
        string Plaid_Item_Id { get; set; }
        Account[] Accounts { get; set; }
        string Public_Token { get; set; }
        string Request_Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IBankSummaryResponse : IPlaidFileThisBankMapDetail
    {
        List<Account> Accounts { get; set; }
        string InstitutionId { get; set; }
        string Name { get; set; }
        string Logo { get; set; }
        string Plaid_Item_Id { get; set; }
        string ApplicantId { get; set; }
        string State { get; set; }
        string ConnectionId { get; set; }
        string FileThisAccountId { get; set; }
    }
}

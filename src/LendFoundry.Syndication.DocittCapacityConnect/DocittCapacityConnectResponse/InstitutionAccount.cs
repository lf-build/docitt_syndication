﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;


namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class InstitutionAccount : IInstitutionAccount
    {
        public bool Is_Linked { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPlaidLink, PlaidLink>))]
        public IPlaidLink Plaid_Link { get; set; }
        public bool Has_Mfa { get; set; }
        public object Mfa { get; set; }
        public bool Has_Error { get; set; }
        public PlaidError Error { get; set; }
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class StateResponse : IStateResponse
    {
        public string AccountId { get; set; }
        public string ConnectionId { get; set; }
        public string InteractionId { get; set; }
        public string State { get; set; }
        public bool IsSuccess { get; set; }
        public string SuccessMessage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class PlaidLink : IPlaidLink
    {
        public string Plaid_Item_Id { get; set; }
        public Account[] Accounts { get; set; }
        public string Public_Token { get; set; }
        public string Request_Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class SyncAccount : ISyncAccount
    {
        public string Plaid_Item_Id { get; set; }
        public Account Account { get; set; }
        public bool Has_Error { get; set; }
        public PlaidError Error { get; set; }
        public bool IsCredentialsChanged { get; set; }
        public bool IsSuccess { get; set; } = true;
    }
}

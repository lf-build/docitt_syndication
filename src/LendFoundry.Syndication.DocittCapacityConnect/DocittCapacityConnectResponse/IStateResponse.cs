﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IStateResponse
    {
        string AccountId { get; set; }
        string ConnectionId { get; set; }
        string InteractionId { get; set; }
        string State { get; set; }
        bool IsSuccess { get; set; }
        string SuccessMessage { get; set; }
    }
}

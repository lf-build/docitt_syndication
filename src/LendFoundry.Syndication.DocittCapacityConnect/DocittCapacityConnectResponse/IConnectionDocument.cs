﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IConnectionDocument
    {
        string DocittDocumentId { get; set; }
        string Name { get; set; }
        object MinimumAmountDue { get; set; }
        object LastPaymentAmount { get; set; }
        string InstitutionId { get; set; }
        string Institution { get; set; }
        string Filename { get; set; }
        object EndDate { get; set; }
        object DueDate { get; set; }
        string DocumentType { get; set; }
        object DocumentSubType { get; set; }
        string DocumentId { get; set; }
        object StartDate { get; set; }
        string Date { get; set; }
        object CurrentBalance { get; set; }
        DateTime CreatedOn { get; set; }
        string ConnectionId { get; set; }
        object ActionDate { get; set; }
        object AccountType { get; set; }
        object AccountSubtype { get; set; }
        object AccountNumber { get; set; }
        object AccountName { get; set; }
        string CustomerId { get; set; }
        object TotalAmountDue { get; set; }
        bool IsDeleted { get; set; }
        DateTime FileThisCreatedDate { get; set; }
        object FileThisModifiedDate { get; set; }
        string Id { get; set; }
    }
}
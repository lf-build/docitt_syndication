﻿using System;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class Customer : ICustomer
    {
        public DateTime CreatedDate { get; set; }
        public string CustomerId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Ssn { get; set; }
        public decimal MonthlyGrossIncome { get; set; }
        public decimal SalesPrice { get; set; }

        public string InvitationId { get; set; }
    }
}

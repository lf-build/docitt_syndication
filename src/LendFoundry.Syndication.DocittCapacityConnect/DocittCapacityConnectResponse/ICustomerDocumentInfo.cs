﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICustomerDocumentInfo
    {
        string Id { get; set; }
        string DocumentType { get; set; }
        string InvitationId { get; set; }
        string Type { get; set; }
        int Size { get; set; }
        string Signature { get; set; }
        string Path { get; set; }
        bool IsEncrypted { get; set; }
        string CustomerId { get; set; }
        string DeleteDate { get; set; }
        bool IsDeleted { get; set; }
        int VisibilityWeight { get; set; }
        string InstitutionId { get; set; }
        string FileId { get; set; }
        string YearDate { get; set; }
        bool IsLinked { get; set; }
        bool IsManualUpload { get; set; }
        string InstitutionName { get; set; }
        string Name { get; set; }
        string Total { get; set; }
        string Company { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        List<IAccountDocument> Documents { get; set; }
        bool IsSubmitted { get; set; }

    }
}

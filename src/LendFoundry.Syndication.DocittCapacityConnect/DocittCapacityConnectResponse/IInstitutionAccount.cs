﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInstitutionAccount
    {
        bool Is_Linked { get; set; }
        IPlaidLink Plaid_Link { get; set; }
        bool Has_Mfa { get; set; }
        object Mfa { get; set; }
        bool Has_Error { get; set; }
        PlaidError Error { get; set; }
        string InstitutionId { get; set; }
        string Name { get; set; }
        string Logo { get; set; }
    }
}

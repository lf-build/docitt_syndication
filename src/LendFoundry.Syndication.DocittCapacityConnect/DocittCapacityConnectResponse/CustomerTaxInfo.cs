﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class CustomerTaxInfo : ICustomerTaxInfo
    {
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Year { get; set; }
    }
}

﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class InteractionMfa : IInteractionMfa
    {
        public string Code { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInteractionMfaPart, InteractionMfaPart>))]
        public List<IInteractionMfaPart> Parts { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string LegacyCode { get; set; }
        public string Version { get; set; }
    }
}

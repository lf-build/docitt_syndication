﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class AccountDocument : IAccountDocument
    {
        [JsonConverter(typeof(InterfaceConverter<IDocument, Document>))]
        public IDocument DocumentInfo { get; set; }

        public bool IsSubmitted { get; set; }

        [JsonProperty("Name")]
        public string AccountNumber { get; set; } // Account No
        public string Date { get; set; }
        public string FileId { get; set; }
        public string DocumentId { get; set; }
    }
}

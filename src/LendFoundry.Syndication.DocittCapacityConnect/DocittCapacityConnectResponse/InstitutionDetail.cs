﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class InstitutionDetail : IInstitutionDetail
    {
        public List<Credential> credentials { get; set; }
        public bool has_mfa { get; set; }
        public string institutionId { get; set; }
        public object mfa { get; set; }
        public string logo { get; set; }
        public string name { get; set; }
        public string[] products { get; set; }
        public string urlAccountLocked { get; set; }
        public string urlAccountSetup { get; set; }
        public string urlForgottenPassword { get; set; }
        public Colors colors { get; set; }

    }

    public class Colors
    {
        public string dark { get; set; }
        public string darker { get; set; }
        public string[] gradient { get; set; }
        public string primary { get; set; }
    }

    public class Credential
    {
        public string label { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }

}

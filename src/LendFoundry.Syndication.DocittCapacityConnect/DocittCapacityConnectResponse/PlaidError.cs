﻿
namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class PlaidError : IPlaidError
    {
        public string Display_Message { get; set; }
        public string Error_Code { get; set; }
        public string Error_Message { get; set; }
        public string Error_Type { get; set; }
        public string Request_Id { get; set; }
    }    
}

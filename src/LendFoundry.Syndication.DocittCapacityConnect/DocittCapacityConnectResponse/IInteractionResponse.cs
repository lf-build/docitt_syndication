﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInteractionResponse
    {
        string ApplicantId { get; set; }
        string AccountId { get; set; }
        string ConnectionId { get; set; }
        string InteractionId { get; set; }
        IInteractionMfa Mfa { get; set; }
        string InstitutionId { get; set; }
        string Name { get; set; }
        object Logo { get; set; }
        string LogoUrl { get; set; }
        bool IsMfa { get; set; }
        bool IsError { get; set; }
        object Error { get; set; }
    }
}

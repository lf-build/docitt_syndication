﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IPlaidFileThisBankMapDetail
    {
        bool IsBankAvailableInFileThis { get; set; }
        bool IsLinkedInPlaid { get; set; }
        string FileThisInstitutionId { get; set; }
        bool IsFileThisBankLinked { get; set; }        
        bool IsManualUpload { get; set; }
    }
}

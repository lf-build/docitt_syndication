﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class BankSummaryResponse : IBankSummaryResponse
    {
        public string Plaid_Item_Id { get; set; }
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public List<Account> Accounts { get; set; }
        public string ApplicantId { get; set; }
        public string State { get; set; }
        public string ConnectionId { get; set; }
        public string FileThisAccountId { get; set; }

        public bool IsBankAvailableInFileThis { get; set; }
        public bool IsLinkedInPlaid { get; set; }
        public string FileThisInstitutionId { get; set; }
        public bool IsFileThisBankLinked { get; set; }
        public bool IsManualUpload { get; set; }
    }
}

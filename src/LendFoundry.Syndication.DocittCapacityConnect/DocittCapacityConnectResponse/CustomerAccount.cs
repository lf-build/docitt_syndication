﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class CustomerAccount : ICustomerAccount
    {
        public CustomerAccount(AccountHolder[] accountHolders)
        {            
            AccountHolders = accountHolders;

        }

        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public float AvailableBalance { get; set; }
        public float CurrentBalance { get; set; }
        public string CustomerId { get; set; }
        public string InstitutionId { get; set; }
        public DateTime DateOpened { get; set; }
        public string ProvidersAccountId { get; set; }
        public string ProviderId { get; set; }
        public DateTime Updated { get; set; }
        public bool IsRetirement { get; set; }
      //  public IAccountTransaction[] Transactions { get; set; }
        public IAccountHolder[] AccountHolders { get; set; }

        public bool IsManualUpload { get; set; }
        public bool IsDeleted { get; set; }
    }

 

    public class AccountHolder : IAccountHolder
    {
        public AccountHolder(AccountHolderAddress[] accountHolderAddresses)
        {
            AccountHolderAddresses = accountHolderAddresses;
        }

        public string AccountHolderId { get; set; }
        public string AccountId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Ssn { get; set; }
        public IAccountHolderAddress[] AccountHolderAddresses { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class AccountHolderAddress : IAccountHolderAddress
    {
        public string AccountHolderId { get; set; }
        public string AddressId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string StreetAddress3 { get; set; }
        public string Zipcode { get; set; }
        public string ZipcodeExtension { get; set; }
    }
}

﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class ApplicantBankAccountDocument : IApplicantBankAccountDocument
    {
        [JsonConverter(typeof(InterfaceConverter<IApplicantDocument, ApplicantDocument>))]
        public IApplicantDocument ApplicantDocument { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IApplicantAccount, ApplicantAccount>))]
        public IApplicantAccount ApplicantAccount { get; set; }
        public bool IsBankAvailableInFileThis { get; set; }
        public bool IsLinkedInPlaid { get; set; }
        public string FileThisInstitutionId { get; set; }
        public bool IsFileThisBankLinked { get; set; }
        public bool IsManualUpload { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IApplicantBankAccountDocument : IPlaidFileThisBankMapDetail
    {
        IApplicantDocument ApplicantDocument { get; set; }
        IApplicantAccount ApplicantAccount { get; set; }
    }
}

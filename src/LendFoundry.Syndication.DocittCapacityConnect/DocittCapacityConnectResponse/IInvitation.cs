﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInvitation
    {
        string Id { get; set; }
        string DocumentType { get; set; }
        string CompanyId { get; set; }
        DateTime? CompleteDate { get; set; }
        DateTime? AcceptedDate { get; set; }
        DateTime CreatedDate { get; set; }
        string CustomerId { get; set; }
        DateTime? DeleteDate { get; set; }
        bool IsDeleted { get; set; }
        string Email { get; set; }
        string InvitationCode { get; set; }
        bool IsAccepted { get; set; }
        bool IsComplete { get; set; }
        bool IsApproved { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        DateTime? ApprovedDate { get; set; }
        string CompanyInviteConfigId { get; set; }
        string[] InvitationFlags { get; set; }
        IWorkflowState WorkflowState { get; set; }
        IInvitationData InvitationData { get; set; }
    }

    public interface IWorkflowState
    {
        DateTime ActionDate { get; set; }
        DateTime? ResponseDate { get; set; }
        string StepId { get; set; }
        string StepOutputRequest { get; set; }
        string WorkflowId { get; set; }
        string StateName { get; set; }
    }

    public interface IInvitationData
    {
        string InvitationDataId { get; set; }
        decimal AverageDailyBalance { get; set; }
        decimal Credits { get; set; }
        decimal Debits { get; set; }
        int MonthlyAccountHighDay { get; set; }
        int MonthlyAccountLowDay { get; set; }
        decimal NinetyDayAvgBalance { get; set; }
        int NinetyDaysofAccountDataCount { get; set; }
        int NumberOfAccounts { get; set; }
        int NumberOfRetirementAccounts { get; set; }
        decimal SixtyDayAvgBalance { get; set; }
        decimal ThirtyDayAvgBalance { get; set; }
        decimal VerifiedBalance { get; set; }
    }

}

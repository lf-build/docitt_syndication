﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class DocumentInfo : IDocumentInfo
    {
        public string DocumentId { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public bool IsLinked { get; set; }
        public bool IsManualUpload { get; set; }

        public string Type { get; set; }
        public string YearDate { get; set; }
        public string Name { get; set; }
        public string Total { get; set; }
        public string Company { get; set; }

        public string DocumentType { get; set; }
        public string DocumentSubType { get; set; }
        public string AccountType { get; set; }
        public string AccountSubtype { get; set; }
        public string AccountNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        public List<IAccountDocument> Documents { get; set; }
        public bool IsSubmitted { get; set; }
    }
}

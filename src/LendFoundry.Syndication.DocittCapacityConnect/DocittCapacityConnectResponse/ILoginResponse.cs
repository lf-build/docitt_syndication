﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ILoginResponse
    {
        string ClientId { get; set; }
        string UserName { get; set; }
        string UserType { get; set; }
        string BearerToken { get; set; }
        string TokenType { get; set; }
        string Message { get; set; }
    }
}

﻿
namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInstitution
    {
        string Description { get; set; }
        string InstitutionId { get; set; }
        string Name { get; set; }

        bool IsDefault { get; set; }

        string Logo { get; set; }

        InstitutionType Type { get; set; }
        //object Url { get; set; }
        //object DeleteDate { get; set; }
        //bool IsDeleted { get; set; }
        // IProviderinstitution[] ProviderInstitutions { get; set; }
    }



    public interface IProviderinstitution
    {
        string ProviderId { get; set; }
        object InstitutionId { get; set; }
        string ProvidersUniqueId { get; set; }
        int ProviderWeight { get; set; }
        string ProviderInstitutionCode { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInteractionMfaPart
    {
        string Kind { get; set; }
        string Id { get; set; }
        string Text { get; set; }
        string Label { get; set; }
        bool Sensitive { get; set; }
        object Value { get; set; }
        object Choices { get; set; }
    }
}

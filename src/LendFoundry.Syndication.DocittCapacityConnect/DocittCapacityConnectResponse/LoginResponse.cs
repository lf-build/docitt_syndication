﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class LoginResponse : ILoginResponse
    {
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string BearerToken { get; set; }
        public string TokenType { get; set; }
        public string Message { get; set; }
    }
}

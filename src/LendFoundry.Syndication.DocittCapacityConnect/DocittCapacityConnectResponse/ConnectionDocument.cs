﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class ConnectionDocument : IConnectionDocument
    {
        public string DocittDocumentId { get; set; }
        public string Name { get; set; }
        public object MinimumAmountDue { get; set; }
        public object LastPaymentAmount { get; set; }
        public string InstitutionId { get; set; }
        public string Institution { get; set; }
        public string Filename { get; set; }
        public object EndDate { get; set; }
        public object DueDate { get; set; }
        public string DocumentType { get; set; }
        public object DocumentSubType { get; set; }
        public string DocumentId { get; set; }
        public object StartDate { get; set; }
        public string Date { get; set; }
        public object CurrentBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ConnectionId { get; set; }
        public object ActionDate { get; set; }
        public object AccountType { get; set; }
        public object AccountSubtype { get; set; }
        public object AccountNumber { get; set; }
        public object AccountName { get; set; }
        public string CustomerId { get; set; }
        public object TotalAmountDue { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime FileThisCreatedDate { get; set; }
        public object FileThisModifiedDate { get; set; }
        public string Id { get; set; }
    }
}

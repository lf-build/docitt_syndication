﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class ApplicantDocument : IApplicantDocument
    {
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDocumentInfo, DocumentInfo>))]
        public List<IDocumentInfo> ListOfDocuments { get; set; }
        public string ConnectionId { get; set; }
        public string FileThisAccountId { get; set; }
        public string ApplicantId { get; set; }
        public bool IsManualUpload { get; set; }
        public bool IsLinked { get; set; } = true;
        public string State { get; set; } = "not-connected";
        public bool IsSuccess { get; set; }
        public string SuccessMessage { get; set; }
    }
}

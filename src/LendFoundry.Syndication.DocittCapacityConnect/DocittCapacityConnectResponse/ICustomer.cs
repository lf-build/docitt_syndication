﻿using System;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICustomer
    {
        DateTime CreatedDate { get; set; }
        string CustomerId { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        string Ssn { get; set; }
        decimal MonthlyGrossIncome { get; set; }
        decimal SalesPrice { get; set; }

        string InvitationId { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInstitutionDetail
    {
        List<Credential> credentials { get; set; }
        bool has_mfa { get; set; }
        string institutionId { get; set; }
        object mfa { get; set; }
        string logo { get; set; }
        string name { get; set; }
        string[] products { get; set; }
        string urlAccountLocked { get; set; }
        string urlAccountSetup { get; set; }
        string urlForgottenPassword { get; set; }
        Colors colors { get; set; }
    }
}

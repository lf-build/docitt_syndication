﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class InteractionResponse : IInteractionResponse
    {
        public string ApplicantId { get; set; }
        public string AccountId { get; set; }
        public string ConnectionId { get; set; }
        public string InteractionId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IInteractionMfa, InteractionMfa>))]
        public IInteractionMfa Mfa { get; set; }
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public object Logo { get; set; }
        public string LogoUrl { get; set; }
        public bool IsMfa { get; set; }
        public bool IsError { get; set; }
        public object Error { get; set; }
    }
}

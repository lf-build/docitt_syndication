﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICustomerTaxInfo
    {
        string InstitutionId { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        int Year { get; set; }
        
    }
}

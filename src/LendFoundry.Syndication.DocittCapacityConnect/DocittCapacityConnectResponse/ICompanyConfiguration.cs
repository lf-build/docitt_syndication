﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICompanyConfiguration
    {
        string InvitationId { get; set; }
        string CompanyId { get; set; }
        string CompanyInviteConfigId { get; set; }
        DateTime Created { get; set; }
        int DaysOfData { get; set; }
        bool Default { get; set; }
        string Name { get; set; }
        bool RealDocs { get; set; }
        decimal VerifiedAssetsRequirement { get; set; }
        string[] RulesList { get; set; }
    }

}

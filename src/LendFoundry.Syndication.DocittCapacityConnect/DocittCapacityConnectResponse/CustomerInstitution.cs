﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class CustomerInstitution : ICustomerInstitution
    {
        public string ConnectionId { get; set; }
        public string CustomerId { get; set; }
        public string InstitutionId { get; set; }
        public string InvitationId { get; set; }
        public string ProviderAccountId { get; set; }
        public string ProviderFiConnectionId { get; set; }
        public string ProviderId { get; set; }
        public InstitutionType Type { get; set; }
        public bool IsChallengeCompleted { get; set; }
        public string InstitutionName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class Invitation : IInvitation
    {
        public Invitation(WorkflowState workflowState, InvitationData invitationData)
        {
            WorkflowState = workflowState;
            InvitationData = invitationData;
        }

        public string Id { get; set; }
        public string DocumentType { get; set; }
        public string CompanyId { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerId { get; set; }
        public DateTime? DeleteDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Email { get; set; }
        public string InvitationCode { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsComplete { get; set; }
        public bool IsApproved { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string CompanyInviteConfigId { get; set; }
        public string[] InvitationFlags { get; set; }
        public IWorkflowState WorkflowState { get; set; }
        public IInvitationData InvitationData { get; set; }
    }

    public class WorkflowState: IWorkflowState
    {
        public DateTime ActionDate { get; set; }
        public DateTime? ResponseDate { get; set; }
        public string StepId { get; set; }
        public string StepOutputRequest { get; set; }
        public string WorkflowId { get; set; }
        public string StateName { get; set; }
    }

    public class InvitationData: IInvitationData
    {
        public string InvitationDataId { get; set; }
        public decimal AverageDailyBalance { get; set; }
        public decimal Credits { get; set; }
        public decimal Debits { get; set; }
        public int MonthlyAccountHighDay { get; set; }
        public int MonthlyAccountLowDay { get; set; }
        public decimal NinetyDayAvgBalance { get; set; }
        public int NinetyDaysofAccountDataCount { get; set; }
        public int NumberOfAccounts { get; set; }
        public int NumberOfRetirementAccounts { get; set; }
        public decimal SixtyDayAvgBalance { get; set; }
        public decimal ThirtyDayAvgBalance { get; set; }
        public decimal VerifiedBalance { get; set; }
    }
}

﻿using System;


namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICompany
    {

        string ApiKey { get; set; }
        string ApiSecret { get; set; }
        string CompanyId { get; set; }
        DateTime CreatedDate { get; set; }
        string Name { get; set; }
        string ClientId { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IInteractionMfa
    {
        string Code { get; set; }
        List<IInteractionMfaPart> Parts { get; set; }
        string Id { get; set; }
        string Title { get; set; }
        string LegacyCode { get; set; }
        string Version { get; set; }
    }
}

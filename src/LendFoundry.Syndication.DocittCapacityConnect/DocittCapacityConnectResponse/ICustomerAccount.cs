﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICustomerAccount
    {
        string AccountId { get; set; }
        string AccountName { get; set; }
        string AccountNumber { get; set; }
        string AccountType { get; set; }
        float AvailableBalance { get; set; }
        float CurrentBalance { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
        DateTime DateOpened { get; set; }
        string ProvidersAccountId { get; set; }
        string ProviderId { get; set; }
        DateTime Updated { get; set; }
        bool IsRetirement { get; set; }
     //   IAccountTransaction[] Transactions { get; set; }
        IAccountHolder[] AccountHolders { get; set; }

        bool IsManualUpload { get; set; }
        bool IsDeleted { get; set; }
    }
   

    public interface IAccountHolder
    {
        string AccountHolderId { get; set; }
        string AccountId { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        string Ssn { get; set; }
        IAccountHolderAddress[] AccountHolderAddresses { get; set; }
        bool IsPrimary { get; set; }
    }

    public interface IAccountHolderAddress
    {
        string AccountHolderId { get; set; }
        string AddressId { get; set; }
        string City { get; set; }
        string State { get; set; }
        string StreetAddress1 { get; set; }
        string StreetAddress2 { get; set; }
        string StreetAddress3 { get; set; }
        string Zipcode { get; set; }
        string ZipcodeExtension { get; set; }
    }

}

﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class Institution : IInstitution
    {
        public string Description { get; set; }
        public string InstitutionId { get; set; }
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public string Logo { get; set; }

        [DefaultValue(InstitutionType.Banking)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public InstitutionType Type { get; set; } // = InstitutionType.Banking;

        //public object Url { get; set; }
        //public object DeleteDate { get; set; }
        //public bool IsDeleted { get; set; }
        // public IProviderinstitution[] ProviderInstitutions { get; set; }
    }

    public class Providerinstitution
    {
        public string ProviderId { get; set; }
        public object InstitutionId { get; set; }
        public string ProvidersUniqueId { get; set; }
        public int ProviderWeight { get; set; }
        public string ProviderInstitutionCode { get; set; }
    }

    public class Section
    {
        [JsonProperty(ItemConverterType = typeof(InterfaceTypeConverter<Template>))]
        public Dictionary<string, ITemplate> Templates { get; set; }
    }

    public interface ITemplate
    {
        string TemplateId { get; set; }
    }

    public class Template : ITemplate
    {
        public string TemplateId { get; set; }
    }

    public class InterfaceTypeConverter<T> : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            var msg = string.Format("This converter should be applied directly with [JsonProperty(ItemConverterType = typeof(TypeConverter<{0}>))] or [JsonProperty(typeof(TypeConverter<{0}>))]",
                                    typeof(T));
            throw new NotImplementedException(msg);
        }

        public override bool CanWrite { get { return false; } }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(T));
            //throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<T>(reader);
        }
    }
}

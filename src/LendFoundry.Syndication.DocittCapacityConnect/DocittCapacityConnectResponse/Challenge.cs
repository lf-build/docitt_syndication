﻿using System;


namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class Challenge: IChallenge
    {
        public int Attempts { get; set; }
        public string ChallengeId { get; set; }
        public string ChallengePayload { get; set; }
        public bool IsComplete { get; set; }
        public string ProviderId { get; set; }
        public string Type { get; set; }
        public string CustomerId { get; set; }
        public string InvitationId { get; set; }
        public DateTime Created { get; set; }
        public string ProviderChallengeId { get; set; }
    }
}

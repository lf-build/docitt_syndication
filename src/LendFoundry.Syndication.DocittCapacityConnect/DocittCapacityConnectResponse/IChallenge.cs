﻿using System;


namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IChallenge
    {
        int Attempts { get; set; }
        string ChallengeId { get; set; }
        string ChallengePayload { get; set; }
        bool IsComplete { get; set; }
        string ProviderId { get; set; }
        string Type { get; set; }
        string CustomerId { get; set; }
        string InvitationId { get; set; }
        DateTime Created { get; set; }
        string ProviderChallengeId { get; set; }
    }

}

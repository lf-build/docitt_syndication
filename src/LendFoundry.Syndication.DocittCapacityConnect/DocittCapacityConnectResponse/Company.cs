﻿using System;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class Company : ICompany
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
        public string CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Name { get; set; }
        public string ClientId { get; set; }
    }
}

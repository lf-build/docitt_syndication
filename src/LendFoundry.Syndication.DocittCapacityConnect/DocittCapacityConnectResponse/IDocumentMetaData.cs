﻿using System;


namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IDocumentMetaData
    {
        string DocittDocumentId { get; set; }
        string Name { get; set; }
        string MinimumAmountDue { get; set; }
        string LastPaymentAmount { get; set; }
        string SourceId { get; set; }
        string Institution { get; set; }
        string FileName { get; set; }
        string EndDate { get; set; }
        string DueDate { get; set; }
        string DocumentType { get; set; }
        string DocumentSubType { get; set; }
        string DocumentId { get; set; }
        string StartDate { get; set; }
        string Date { get; set; }
        string CurrentBalance { get; set; }
        DateTime CreatedOn { get; set; }
        string ConnectionId { get; set; }
        string ActionDate { get; set; }
        string AccountType { get; set; }
        string AccountSubtype { get; set; }
        string AccountNumber { get; set; }
        string AccountName { get; set; }
        string CustomerId { get; set; }
        string TotalAmountDue { get; set; }
        string Id { get; set; }

        string InstitutionType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class CompanyConfiguration : ICompanyConfiguration
    {
        public string InvitationId { get; set; }
        public string CompanyId { get; set; }
        public string CompanyInviteConfigId { get; set; }
        public DateTime Created { get; set; }
        public int DaysOfData { get; set; }
        public bool Default { get; set; }
        public string Name { get; set; }
        public bool RealDocs { get; set; }
        public decimal VerifiedAssetsRequirement { get; set; }
        public string[] RulesList { get; set; }
    }
}

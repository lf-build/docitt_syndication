﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IAccountTransaction
    {
        string Amount { get; set; }
        DateTime Date { get; set; }
        string Name { get; set; }
        string ShortDate { get; set; }
    }
}

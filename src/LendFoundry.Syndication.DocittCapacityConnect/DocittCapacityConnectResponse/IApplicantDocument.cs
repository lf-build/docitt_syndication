﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IApplicantDocument
    {
        string InstitutionId { get; set; }
        string Name { get; set; }
        string Logo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDocumentInfo, DocumentInfo>))]
        List<IDocumentInfo> ListOfDocuments { get; set; }
        string ConnectionId { get; set; }
        string FileThisAccountId { get; set; }
        string ApplicantId { get; set; }
        bool IsManualUpload { get; set; }
        bool IsLinked { get; set; }
        string State { get; set; }
        bool IsSuccess { get; set; }
        string SuccessMessage { get; set; }
    }
}

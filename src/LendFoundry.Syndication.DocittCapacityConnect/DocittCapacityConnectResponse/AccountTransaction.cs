﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class AccountTransaction : IAccountTransaction
    {
        public string Amount { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string ShortDate { get; set; }
    }
}

﻿
namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IPlaidError
    {
        string Display_Message { get; set; }
        string Error_Code { get; set; }
        string Error_Message { get; set; }
        string Error_Type { get; set; }
        string Request_Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface ICustomerInstitution
    {
        string ConnectionId { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
        string InvitationId { get; set; }
        string ProviderAccountId { get; set; }
        string ProviderFiConnectionId { get; set; }
        string ProviderId { get; set; }

        InstitutionType Type { get; set; }
        bool IsChallengeCompleted { get; set; }
        string InstitutionName { get; set; }
    }

}

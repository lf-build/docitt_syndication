﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public interface IDocumentInfo
    {
        string DocumentId { get; set; }
        string FileId { get; set; }
        string FileName { get; set; }
        bool IsLinked { get; set; }
        bool IsManualUpload { get; set; }

        string Type { get; set; }
        string YearDate { get; set; }
        string Name { get; set; }
        string Total { get; set; }
        string Company { get; set; }

        string DocumentType { get; set; }
        string DocumentSubType { get; set; }
        string AccountType { get; set; }
        string AccountSubtype { get; set; }
        string AccountNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        List<IAccountDocument> Documents { get; set; }
        bool IsSubmitted { get; set; }

    }
}

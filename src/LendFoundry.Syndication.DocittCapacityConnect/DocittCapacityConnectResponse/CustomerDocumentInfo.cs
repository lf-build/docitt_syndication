﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class CustomerDocumentInfo : ICustomerDocumentInfo
    {
        public string Id { get; set; }
        public string DocumentType { get; set; }
        public string InvitationId { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public string Signature { get; set; }
        public string Path { get; set; }
        public bool IsEncrypted { get; set; }
        public string CustomerId { get; set; }
        public string DeleteDate { get; set; }
        public bool IsDeleted { get; set; }
        public int VisibilityWeight { get; set; }
        public string InstitutionId { get; set; }
        public string FileId { get; set; }
        public string YearDate { get; set; }
        public bool IsLinked { get; set; }
        public bool IsManualUpload { get; set; }
        public string InstitutionName { get; set; }
        public string Name { get; set; }
        public string Total { get; set; }
        public string Company { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        public List<IAccountDocument> Documents { get; set; }
        public bool IsSubmitted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse
{
    public class InteractionMfaPart : IInteractionMfaPart
    {
        public string Kind { get; set; }
        public string Id { get; set; }
        public string Text { get; set; }
        public string Label { get; set; }
        public bool Sensitive { get; set; }
        public object Value { get; set; }
        public object Choices { get; set; }
    }
}

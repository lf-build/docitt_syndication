﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class MfaReplyRequest : IMfaReplyRequest
    {
        public string PublicToken { get; set; }
        public string MfaType { get; set; }
        public string[] Answers { get; set; }
        public string CustomerId { get; set; }
        public string InstitutionId { get; set; }
    }
}

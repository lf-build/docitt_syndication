﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class BankAccountInfoRequest : IBankAccountInfoRequest
    {
        public string BankName { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
        //   public string BankInfoId { get; set; }
        public string AccountHolderName { get; set; }
    }
}

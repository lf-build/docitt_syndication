﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class DocittCapacityConnectConfiguration : IDocittCapacityConnectConfiguration
    {
        public string Certificate { get; set; }
        public string CertificatePassword { get; set; }
        public string ProxyUrl { get; set; }
        public string ServiceUrl { get; set; }
        public string Ticket { get; set; }
        public string CompanyId { get; set; }

        public string[] DefaultInstitutions { get; set; }
        public string[] DefaultTaxInstitutions { get; set; }
        public string[] DefaultW2Institutions { get; set; }

        public List<Institution> FavouriteInstitutions { get; set; }

        public string CompanyUserName { get; set; }
        public string CompanyPassword { get; set; }

        public bool IsSimulatorOn { get; set; }
        public Dictionary<InstitutionType, bool> UseSimulatorFor { get; set; }
        public int MaxFilesToRequiredConditionRequest { get; set; }

        public string DefaultProviderId { get; set; }
        public string DaefaultChallengeAnswer { get; set; }
        public string SampleBankDetailResponse { get; set; }
        public string SampleBankAccountResponse { get; set; }
        public string SampleBankAccountSyncResponse { get; set; }
        public string SampleMfaSelectionResponse { get; set; }
        public string SampleInvalidCredentialResponse { get; set; }
        public string SampleInvalidMfaResponse { get; set; }
        public string SampleTaxW2DocumentResponse { get; set; }
        public string[] AcceptedBankPasswords { get; set; }
        public int NoOfYearsW2Docments { get; set; }
        public int NoOfPayStubDocments { get; set; }
        public int MaxFileSizeInMB { get; set; }
        public string PermissibleFileTypes { get; set; }
        public Dictionary<InstitutionType, TemplateDetail> WriteExplanationTemplates { get; set; }
        public int BankTransactionPeriodInDays { get; set; }
        public int BankTransactionCountPerPage { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}

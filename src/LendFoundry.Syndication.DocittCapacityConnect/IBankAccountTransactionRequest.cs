﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IBankAccountTransactionRequest
    {
        string PlaidItemId { get; set; }
        string PlaidAccountId { get; set; }
        string StartDate { get; set; }
        string EndDate { get; set; }
        int Count { get; set; }
        int Offset { get; set; }
    }
}

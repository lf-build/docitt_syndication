﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCompanyByIdResponse: IGetCompanyByIdResponse
    {
        public ICompany Result { get; set; }
    }
}

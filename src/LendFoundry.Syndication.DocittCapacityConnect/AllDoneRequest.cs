﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class AllDoneRequest : IAllDoneRequest
    {
        public string RequestId { get; set; }
        public string CreatedBy { get; set; }
        public string EntityType { get; set; }
        public string ApplicationNumber { get; set; }
    }
}

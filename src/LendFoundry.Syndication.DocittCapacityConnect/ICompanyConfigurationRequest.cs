﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ICompanyConfigurationRequest
    {
        int DaysOfData { get; set; }
        string Name { get; set; }
        bool RealDocs { get; set; }
        string InvitationId { get; set; }
        int VerifiedAssetsRequirement { get; set; }
        string[] RulesList { get; set; }
    }

}

﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetChallengeResponse : IGetChallengeResponse
    {
        public IEnumerable<IChallenge> Result { get; set; }
    }
}

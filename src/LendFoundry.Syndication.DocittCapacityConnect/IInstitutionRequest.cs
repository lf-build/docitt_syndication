﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IInstitutionRequest
    {
        string CustomerId { get; set; }
        string InvitationId { get; set; }
        string InstitutionId { get; set; }
        string Secret { get; set; }
        string UserName { get; set; }
        string Pin { get; set; }
        string PlaidItemId { get; set; }
        string ApplicationNumber {get; set;}
    }

}

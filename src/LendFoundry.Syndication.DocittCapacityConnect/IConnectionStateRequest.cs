﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IConnectionStateRequest
    {
        string AccountId { get; set; }
        string ConnectionId { get; set; }
    }
}

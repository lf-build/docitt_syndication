﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetCustomerByIdResponse
    {
        ICustomer Result { get; set; }
    }
}

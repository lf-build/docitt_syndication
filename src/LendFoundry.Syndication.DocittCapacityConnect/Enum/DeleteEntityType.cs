﻿namespace LendFoundry.Syndication.DocittCapacityConnect
{
    /// <summary>
    /// DeleteEntityType
    /// </summary>
    public enum DeleteEntityType
    {
        none = 0,
        account = 1,
        institution = 2
    }
}

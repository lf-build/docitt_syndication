﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public enum InstitutionType
    {
        All = 0,
        Banking = 1,
        TaxReturn = 2,
        W2PayStub = 3,
        FileThisBanking = 4
    }
}

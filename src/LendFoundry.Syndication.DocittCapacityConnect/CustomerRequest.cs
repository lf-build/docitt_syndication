﻿
namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class CustomerRequest : ICustomerRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Ssn { get; set; }
        public int MonthlyGrossIncome { get; set; }
        public int SalesPrice { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class InstitutionRequest : IInstitutionRequest
    {
        public string CustomerId { get; set; }
        public string InvitationId { get; set; }
        public string InstitutionId { get; set; }
        public string Secret { get; set; }
        public string UserName { get; set; }
        public string Pin { get; set; }
        public string PlaidItemId { get; set; }
        public string ApplicationNumber {get; set;}
    }
}

﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IApplicantAccount
    {
        List<Account> Accounts { get; set; }
        string InstitutionId { get; set; }
        string Name { get; set; }
        string Logo { get; set; }
        string Plaid_Item_Id { get; set; }
        string ApplicantId { get; set; }

    }
}
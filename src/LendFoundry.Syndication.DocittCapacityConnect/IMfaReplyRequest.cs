﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IMfaReplyRequest
    {
        string PublicToken { get; set; }
        string MfaType { get; set; }
        string[] Answers { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
    }
}

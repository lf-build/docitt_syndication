﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class ManualRequest
    {
        public string id { get; set; } //unique id
        public string Institution { get; set; }//(banking/w2paystub/tax)
        public string Owner { get; set; } //(banking/w2paystub/tax)
        public string AccountNo { get; set; }  //(banking)
        public string Type { get; set; }  //(banking/tax)  (tax -> 1040 , 1040A etc..) (banking -> saving, current etc..)

        public string Total { get; set; } //(banking/w2paystub)

        public string MonthYear { get; set; } //(w2paystub)

        public string Company { get; set; } //(w2paystub)
    }
}

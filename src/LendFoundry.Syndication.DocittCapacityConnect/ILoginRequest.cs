﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ILoginRequest
    {
        string UserName { get; set; }
        string Password { get; set; }
    }

}

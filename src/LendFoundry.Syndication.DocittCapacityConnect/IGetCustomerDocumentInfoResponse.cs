﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetCustomerDocumentInfoResponse
    {
        IEnumerable<ICustomerDocumentInfo> Result { get; set; }
    }
}

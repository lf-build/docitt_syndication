﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetInvitationByIdResponse
    {
        IInvitation Result { get; set; }
    }
}

﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IDocittCapacityConnectSimulatorService
    {
        #region Institutions

        Task<IGetInstitutionResponse> GetAllInstitutions(string token, InstitutionType type);

        Task<IGetInstitutionByIdResponse> GetInstitutionById(string institutionId, string token, InstitutionType type);

        Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomer(string customerId, string token, InstitutionType type);

        Task<IGetCompanyByIdResponse> CreateCompany(ICompanyRequest request);

        Task<IGetCustomerByIdResponse> CreateCustomer(ICustomerRequest request, string token);

        Task<IGetCustomerByIdResponse> GetCustomer(string companyId, string userName, string token);

        Task<IGetInvitationByIdResponse> CreateCustomerInvitation(string customerId, IInvitationRequest request, string token);

        Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfiguration(string companyId, ICompanyConfigurationRequest request, string token);

        Task<IGetInvitationByIdResponse> AcceptInvitation(string customerId, IInvitationAcceptRequest request, string token);

        Task<string> ApproveInvitation(string customerId, string invitationId, string token);

        Task<string> AddInstitution(string customerId, string institutionId, IInstitutionRequest request, string token);

        Task<IGetChallengeResponse> GetChallenges(string institutionId, string customerId, string token);

        Task<IGetInvitationByIdResponse> UpdateCompanyInvitation(string companyId, IInvitation request, string token);

        Task<string> ReplyChallenge(string customerId, IChallengeReplyRequest request, string token);

        Task<IGetCustomerAccountResponse> GetCustomerAccounts(string customerId, string token, InstitutionType type);

        // Task<IGetCustomerTaxInfoResponse> GetCustomerTax(string customerId, string token, InstitutionType type);

        #endregion

        #region Assets

        Task<IGetCustomerByIdResponse> CreateCustomerWithInvitationConfig(string companyId, ICustomerRequest request, string token);

        Task<IGetChallengeResponse> AddInstitutionAndGetChallenge(IInstitutionRequest request, string token);

        #endregion

        #region Login
        Task<IGetLoginResponse> GetLogin(ILoginRequest request);

        #endregion
    }
}

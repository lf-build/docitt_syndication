﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerByIdResponse: IGetCustomerByIdResponse
    {
        public ICustomer Result { get; set; }
    }
}

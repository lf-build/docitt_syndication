﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerInstitutionResponse : IGetCustomerInstitutionResponse
    {
       public IEnumerable<ICustomerInstitution> Result { get; set; }
    }
}

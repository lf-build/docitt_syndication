﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class ChallengeReplyRequest : IChallengeReplyRequest
    {
        public ChallengeReplyRequest(Response[] response)
        {
            Response = response;
        }

        public string ChallengeId { get; set; }
        public string CustomerId { get; set; }
        public string InstitutionId { get; set; }
        public string InvitationId { get; set; }
        public string ChallengeType { get; set; }
        public IResponse[] Response { get; set; }
        public string ProviderId { get; set; }
    }

    public class Response : IResponse
    {
        public string[] Mfa { get; set; }
    }
}

﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCompanyConfigurationByIdResponse : IGetCompanyConfigurationByIdResponse
    {
        public ICompanyConfiguration Result { get; set; }
    }
}

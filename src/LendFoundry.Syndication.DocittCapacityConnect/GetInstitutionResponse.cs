﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetInstitutionResponse : IGetInstitutionResponse
    {
        public IEnumerable<IInstitution> Result { get; set; }
    }
}

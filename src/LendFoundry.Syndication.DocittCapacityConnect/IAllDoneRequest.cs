﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IAllDoneRequest
    {
        string RequestId { get; set; }
        string CreatedBy { get; set; }
        string EntityType { get; set; }
        string ApplicationNumber { get; set; }
    }
}

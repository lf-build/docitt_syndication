﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IChallengeReplyRequest
    {

        string ChallengeId { get; set; }
        string CustomerId { get; set; }
        string InstitutionId { get; set; }
        string InvitationId { get; set; }
        string ChallengeType { get; set; }
        IResponse[] Response { get; set; }
        string ProviderId { get; set; }
    }

    public interface IResponse
    {
        string[] Mfa { get; set; }
    }
}

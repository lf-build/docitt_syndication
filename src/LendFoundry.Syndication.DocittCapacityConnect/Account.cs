﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{

    public interface IAccount
    {
        string Account_Id { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        string SubType { get; set; }
        string Mask { get; set; }
        Balance Balances { get; set; }
        bool IsLinked { get; set; }
        bool IsManualUpload { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        List<IAccountDocument> Documents { get; set; }
        bool IsSubmitted { get; set; }
        string OwnerName { get; set; }
        string TotalBalance { get; set; }
        bool IsDeleted { get; set; }
        string FileId { get; set; }
    }

    public class Account : IAccount
    {
        public string Account_Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Mask { get; set; }
        public Balance Balances { get; set; }
        public bool IsLinked { get; set; }
        public bool IsManualUpload { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAccountDocument, AccountDocument>))]
        public List<IAccountDocument> Documents { get; set; }
        public bool IsSubmitted { get; set; }
        public string OwnerName { get; set; } = "N/A";
        public string TotalBalance { get; set; }
        public bool IsDeleted { get; set; }
        public string FileId { get; set; }
    }

}

using System.IO;
using System.Text;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ILargeDepositTransactionRequest
    {
        string Income { get; set; }
        string PurchasePrice { get; set; }
    }
}

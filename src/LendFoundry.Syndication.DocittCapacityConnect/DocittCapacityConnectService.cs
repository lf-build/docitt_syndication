﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using LendFoundry.Syndication.DocittCapacityConnect.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.DataAttributes;
using LendFoundry.DocumentManager;
using Docitt.RequiredCondition;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Docitt.Questionnaire;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using LendFoundry.Foundation.Date;
using Docitt.BusinessRules;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class DocittCapacityConnectService : IDocittCapacityConnectService
    {
        public DocittCapacityConnectService(
            IDccProxyFactory proxyfactory, 
            IDocittCapacityConnectConfiguration configuration, 
            IDataAttributesEngine dataAttributeClient, 
            IDocumentManagerService documentManagerClient, 
            IRequiredConditionService requiredConditionClient, 
            IQuestionnaireService questionnaireClient, 
            ITokenReader tokenReader, 
            ITokenHandler tokenParser, 
            ITemplateManagerService templateManager, 
            ITenantTime tenantTime, 
            IHttpContextAccessor httpAccessor, 
            IBusinessRuleClientService businessRuleClient,
            ILogger logger)
        {
            //if (docittCapacityConnectClient == null)
            //    throw new ArgumentNullException(nameof(docittCapacityConnectClient));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxyfactory == null)
                throw new ArgumentNullException(nameof(proxyfactory));
            if (dataAttributeClient == null)
                throw new ArgumentNullException(nameof(dataAttributeClient));
            if (documentManagerClient == null)
                throw new ArgumentNullException(nameof(documentManagerClient));
            if (requiredConditionClient == null)
                throw new ArgumentNullException(nameof(requiredConditionClient));
            if (questionnaireClient == null)
                throw new ArgumentNullException(nameof(questionnaireClient));
            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));
            if (tokenParser == null)
                throw new ArgumentNullException(nameof(tokenParser));
            if (templateManager == null)
                throw new ArgumentNullException(nameof(templateManager));
            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));
            if (httpAccessor == null)
                throw new ArgumentNullException(nameof(httpAccessor));
            if (businessRuleClient == null)
                throw new ArgumentNullException(nameof(businessRuleClient));

            ProxyFactory = proxyfactory;
            Client = proxyfactory.GetProxy(InstitutionType.Banking);
            //Client = docittCapacityConnectClient;
            Configuration = configuration;
            DataAttributeClient = dataAttributeClient;
            DocumentManagerClient = documentManagerClient;
            RequiredConditionClient = requiredConditionClient;
            QuestionnaireClient = questionnaireClient;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            TemplateManager = templateManager;
            TenantTime = tenantTime;
            HttpAccessor = httpAccessor;
            BusinessRuleClient = businessRuleClient;

            Logger = logger;

            CustomerEntityType = "customer";
            FileThisEntityType = "filethis";
            RequestStatusPending = "pending";
            CustomerAccountDocumentEntityType = "customer_account_document";
            CustomerInfoName = "customerInfo";
            ManualBankingName = "ManualBanking";
            LoginRequiredErrorCode = "ITEM_LOGIN_REQUIRED";
        }

        private ILogger Logger;

        private IDocittCapacityConnectConfiguration Configuration { get; }
        private IDocittCapacityConnectProxy Client { get; set; }
        private string DefaultFileExtension = ".pdf";
        public IDccProxyFactory ProxyFactory { get; set; }
        private IDataAttributesEngine DataAttributeClient { get; set; }
        private IQuestionnaireService QuestionnaireClient { get; set; }
        private IDocumentManagerService DocumentManagerClient { get; set; }
        private IRequiredConditionService RequiredConditionClient { get; set; }
        private ITemplateManagerService TemplateManager { get; }
        private ITenantTime TenantTime { get; }
        private IHttpContextAccessor HttpAccessor { get; }
        private IBusinessRuleClientService BusinessRuleClient { get; }

        private ITokenReader TokenReader;
        private ITokenHandler TokenParser;

        #region Data Attributes

        private string CustomerEntityType { get; set; }
        private string FileThisEntityType { get; set; }
        private string CustomerAccountDocumentEntityType { get; set; }
        private string CustomerInfoName { get; set; }
        private string ManualBankingName { get; set; }

        #endregion

        //        private string DocumentTypeW2 = "W2";
        private string DocumentTypePayStub = "PayStub";
        private string DocumentTypeTaxReturn = "Form1040";

        private const string STATE_WAITING = "waiting";
        private const string STATE_COMPLETED = "completed";
        private const string STATE_UPLOADING = "uploading";

        private const string STATE_CONNECTED = "connected";
        private const string STATE_NOT_CONNECTED = "not-connected";
        private const string DATE_FORMATE_MMDDYYYY = "MM/dd/yyyy";
        private const string SAMPLE_BANK_MASK = "1111";
        public string _ProviderId = "58eca4410e7fab6b4ad2f719";
        private string LoginRequiredErrorCode;
        private string RequestStatusPending;

        #region Institutions

        public async Task<IGetInstitutionResponse> GetAllInstitutions(string type, string keyword = "")
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            IGetInstitutionResponse allInstitutions = new GetInstitutionResponse();

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                Client = ProxyFactory.GetProxy(typeOfInstitution);
                allInstitutions = await Task.Run(() => Client.GetAllInstitutionsResponse(typeOfInstitution, keyword));
            }
            else if (Configuration.FavouriteInstitutions != null)
                allInstitutions.Result = Configuration.FavouriteInstitutions.Where(x => x.Type == typeOfInstitution);

            return allInstitutions;
        }

        public async Task<IGetInstitutionResponse> GetSearchListOfInstitutions(string type)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            IGetInstitutionResponse allInstitutions = new GetInstitutionResponse();

            if (typeOfInstitution == InstitutionType.Banking)
            {
                Client = ProxyFactory.GetProxy(typeOfInstitution);
                allInstitutions = await Task.Run(() => Client.GetSearchListOfInstitutionsResponse(typeOfInstitution));
            }
            else if (Configuration.FavouriteInstitutions != null)
            {
                allInstitutions.Result = Configuration.FavouriteInstitutions.Where(x => x.Type == typeOfInstitution);
            }

            return allInstitutions;
        }

        public async Task<IGetInstitutionByIdResponse> GetInstitutionById(string institutionId, string type)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));
            Client = ProxyFactory.GetProxy(typeOfInstitution);
            return await Task.Run(() => Client.GetInstitutionByIdResponse(institutionId, typeOfInstitution));
        }

        public async Task<object> GetInstitutionDetailById(string institutionId, string type)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            return await Task.Run(() => Client.GetInstitutionDetailByIdResponse(institutionId, typeOfInstitution));
        }

        public async Task<IGetCustomerInstitutionResponse> GetAllInstitutionsOfCustomer(string customerId, string type)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));

            var institutionList = await DataAttributeClient.GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution));
            if (institutionList == null)
            {
                Client = ProxyFactory.GetProxy(typeOfInstitution);
                var result = await Task.Run(() => Client.GetAllInstitutionsOfCustomerResponse(customerId, typeOfInstitution));
                if (result.Result != null && result.Result.Count() > 0)
                {
                    await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), result.Result);
                }

                return result;
            }

            return await Task.Run(() => new GetCustomerInstitutionResponse() { Result = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString()) });
        }

        public async Task<object> GetConnectionState(string customerId, string type, IConnectionStateRequest request, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.AccountId))
                throw new ArgumentNullException(nameof(request.AccountId));
            if (string.IsNullOrEmpty(request.ConnectionId))
                throw new ArgumentNullException(nameof(request.ConnectionId));

            Client = ProxyFactory.GetProxy(typeOfInstitution);

            try
            {
                var response = await Client.GetConnectionStateResponse(customerId, typeOfInstitution, request);

                var responseContent = JsonConvert.SerializeObject(response);
                var stateResponse = JsonConvert.DeserializeObject<StateResponse>(responseContent);

                // If state is completed or waiting then fetch data and save it
                if (stateResponse != null && stateResponse.State.Equals(STATE_COMPLETED, StringComparison.InvariantCultureIgnoreCase) || stateResponse.State.Equals(STATE_WAITING, StringComparison.InvariantCultureIgnoreCase) || stateResponse.State.Equals(STATE_UPLOADING, StringComparison.InvariantCultureIgnoreCase))
                {
                    stateResponse.IsSuccess = await GetDocumentsByConnectionId(customerId, typeOfInstitution, request.ConnectionId, entityId);
                }

                return stateResponse;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<IGetCompanyByIdResponse> CreateCompany(ICompanyRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentNullException(nameof(request.Name));

            return await Task.Run(() => Client.CreateCompanyResponse(request));
        }

        public async Task<IGetCustomerByIdResponse> CreateCustomer(ICustomerRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.FirstName))
                throw new ArgumentNullException(nameof(request.FirstName));
            if (string.IsNullOrEmpty(request.Email))
                throw new ArgumentNullException(nameof(request.Email));

            #region Check if Customer exists 

            var _customerId = await DataAttributeClient.GetFirstAttributeValue(CustomerEntityType, request.Email, "CustomerId");
            if (_customerId != null)
            {
                var _InvitationId = await DataAttributeClient.GetFirstAttributeValue(CustomerEntityType, request.Email, "InvitationId");

                return await Task.Run(() => new GetCustomerByIdResponse() { Result = new Customer() { CustomerId = Convert.ToString(_customerId), InvitationId = Convert.ToString(_InvitationId) } });
            }
            #endregion

            var _result = await Task.Run(() => Client.CreateCustomerResponse(request));
            Dictionary<string, string> customerAttributes = new Dictionary<string, string>();
            customerAttributes.Add(nameof(_result.Result.CustomerId), _result.Result.CustomerId);
            customerAttributes.Add(nameof(_result.Result.InvitationId), _result.Result.InvitationId);

            return _result;
        }

        public async Task<IGetCustomerByIdResponse> GetCustomer(string companyId, string userName)
        {
            // temporarily returning static value. Actually need to fetch from DB.
            if (string.IsNullOrEmpty(companyId))
                throw new ArgumentNullException(nameof(companyId));

            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            var _customerInfo = await DataAttributeClient.GetFirstAttributeValue(CustomerEntityType, userName, CustomerInfoName);
            if (_customerInfo == null)
            {
                var result = await Task.Run(() => Client.GetCustomerResponse(userName));
                await DataAttributeClient.SetAttribute(CustomerEntityType, userName, CustomerInfoName, result.Result);

                return await Task.Run(() => result);
            }
            else
            {
                return await Task.Run(() => new GetCustomerByIdResponse() { Result = JsonConvert.DeserializeObject<Customer>(_customerInfo.ToString()) });
            }

            //var invitations = await Task.Run(() => Client.GetAllInvitationsResponse(companyId));

            //if (invitations != null && invitations.Result != null)
            //{
            //    if (invitations.Result.Any(x => x.Email == userName && x.IsAccepted))
            //    {
            //        ICustomer matchedCustomer = new Customer();

            //        var invitation = invitations.Result.Where(x => x.Email == userName).FirstOrDefault();
            //        matchedCustomer.Email = invitation.Email;
            //        matchedCustomer.CustomerId = invitation.CustomerId;
            //        matchedCustomer.FirstName = invitation.FirstName;
            //        matchedCustomer.LastName = invitation.LastName;
            //        matchedCustomer.InvitationId = invitation.Id;

            //        return await Task.Run(() => new GetCustomerByIdResponse() { Result = matchedCustomer });
            //    }
            //}

            //return await Task.Run(() => new GetCustomerByIdResponse() { Result = new Customer() });

            // return await Task.Run(() => new GetCustomerByIdResponse() { Result = new Customer() { CustomerId = "592bbfedc3310c0d9ca0ac6b", Email = userName, InvitationId = "592bc020c3310c123cc88aa8" } });
            //return await Task.Run(() => Client.CreateCustomerResponse(request));
        }

        public async Task<IGetInvitationByIdResponse> CreateCustomerInvitation(string customerId, IInvitationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(request.CompanyId))
                throw new ArgumentNullException(nameof(request.CompanyId));

            return await Task.Run(() => Client.CreateCustomerInvitationResponse(customerId, request));
        }

        public async Task<IGetCompanyConfigurationByIdResponse> CreateCompanyConfiguration(string companyId, ICompanyConfigurationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(companyId))
                throw new ArgumentNullException(nameof(companyId));
            if (string.IsNullOrEmpty(request.InvitationId))
                throw new ArgumentNullException(nameof(request.InvitationId));
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentNullException(nameof(request.Name));

            return await Task.Run(() => Client.CreateCompanyConfigurationResponse(companyId, request));
        }

        public async Task<IGetInvitationByIdResponse> AcceptInvitation(string customerId, IInvitationAcceptRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(request.Code))
                throw new ArgumentNullException(nameof(request.Code));

            return await Task.Run(() => Client.AcceptInvitationResponse(customerId, request));
        }

        public async Task<string> ApproveInvitation(string customerId, string invitationId)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(invitationId))
                throw new ArgumentNullException(nameof(invitationId));

            return await Task.Run(() => Client.ApproveInvitationResponse(customerId, invitationId));
        }

        public async Task<object> AddInstitution(string customerId, string type, string institutionId, IInstitutionRequest request, string entityId = null)
        {
            Logger.Debug("AddInstitution");
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));
            if (string.IsNullOrEmpty(request.CustomerId))
                throw new ArgumentNullException(nameof(request.CustomerId));

            if (typeOfInstitution != InstitutionType.Banking && Configuration.UseSimulatorFor[typeOfInstitution] && string.IsNullOrEmpty(request.InvitationId))
                throw new ArgumentNullException(nameof(request.InvitationId));
            if (string.IsNullOrEmpty(request.InstitutionId))
                throw new ArgumentNullException(nameof(request.InstitutionId));
            if (string.IsNullOrEmpty(request.UserName))
                throw new ArgumentNullException(nameof(request.UserName));
            if (string.IsNullOrEmpty(request.Secret))
                throw new ArgumentNullException(nameof(request.Secret));

            request.ApplicationNumber = entityId;

            if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
            {
                var institutionAccounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
                var customerAccounts = new List<ApplicantBankAccountDocument>();
                if (institutionAccounts != null)
                {
                    customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(institutionAccounts.ToString());

                    if (typeOfInstitution == InstitutionType.Banking && customerAccounts.Where(b => b.IsLinkedInPlaid).Select(c => c.ApplicantAccount).FirstOrDefault(a => a.InstitutionId == request.InstitutionId) != null)
                    {
                        return new { IsSuccess = true, SuccessMessage = "Institution already linked.", InstitutionId = request.InstitutionId };
                    }

                    if (typeOfInstitution == InstitutionType.FileThisBanking && customerAccounts.FirstOrDefault(a => (!a.IsLinkedInPlaid && (a.ApplicantDocument?.InstitutionId == request.InstitutionId)) || (a.FileThisInstitutionId == request.InstitutionId)) != null)
                    {
                        return new { IsSuccess = true, SuccessMessage = "Institution already linked.", InstitutionId = request.InstitutionId };
                    }
                }
            }

            #region FileThis Integration
            else if (!Configuration.UseSimulatorFor[typeOfInstitution])
            {
                var institutionAccounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                var customerDocuments = new List<ApplicantDocument>();
                if (institutionAccounts != null)
                {
                    customerDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(institutionAccounts.ToString());
                    if (customerDocuments.FirstOrDefault(a => a.InstitutionId == request.InstitutionId && a.State.Equals(STATE_CONNECTED)) != null)
                    {
                        return new { IsSuccess = true, SuccessMessage = "Institution already linked.", InstitutionId = request.InstitutionId };
                    }
                }

            }

            #endregion

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();

            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());
                var _institution = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
                if (_institution != null && !string.IsNullOrEmpty(_institution.InvitationId))
                {
                    return "";
                }
            }

            Client = ProxyFactory.GetProxy(typeOfInstitution);

            Logger.Debug("...AddInstitutionResponse");
            var response = await Task.Run(() => Client.AddInstitutionResponse(customerId, institutionId, request, typeOfInstitution));
            if (typeOfInstitution == InstitutionType.Banking)
            {
                return await Task.Run(() => ProcessMfaResponse(response, customerId, entityId));
            }

            if (!Configuration.UseSimulatorFor[typeOfInstitution])
            {
                return await Task.Run(() => ProcessConnectionResponse(response, customerId, typeOfInstitution, entityId));
            }

            var _institutionToCompare = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
            if (_institutionToCompare == null) // this means institution is not present in data attributes
            {

                var _newInstitution = new CustomerInstitution();
                _newInstitution.CustomerId = customerId;
                _newInstitution.InstitutionId = request.InstitutionId;
                _newInstitution.InvitationId = request.InvitationId;
                _newInstitution.Type = typeOfInstitution;
                _newInstitution.ProviderId = _ProviderId;

                var allInstitutionResponse = await Task.Run(() => Client.GetAllInstitutionsResponse(typeOfInstitution));
                var newInstitutionDetail = allInstitutionResponse.Result.FirstOrDefault(a => a.InstitutionId == request.InstitutionId);
                if (newInstitutionDetail != null)
                {
                    _newInstitution.InstitutionName = newInstitutionDetail.Name;
                }
                listOfInstitutions.Add(_newInstitution);

                await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);
            }

            return response;

        }

        public async Task<object> GetConfigurations(string type)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            Client = ProxyFactory.GetProxy(typeOfInstitution);
            return await Task.Run(() => Client.GetConfigurations());
        }

        public async Task<object> AddInstitutionLink(string customerId, string type, string institutionId, object metaData, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (typeOfInstitution == InstitutionType.Banking)
            {
                var institutionAccounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
                var customerAccounts = new List<ApplicantBankAccountDocument>();
                if (institutionAccounts != null)
                {
                    customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(institutionAccounts.ToString());

                    if (typeOfInstitution == InstitutionType.Banking && customerAccounts.Where(b => b.IsLinkedInPlaid).Select(c => c.ApplicantAccount).FirstOrDefault(a => a.InstitutionId == institutionId) != null)
                    {
                        return new { IsSuccess = true, SuccessMessage = "Institution already linked.", InstitutionId = institutionId };
                    }

                    if (typeOfInstitution == InstitutionType.FileThisBanking && customerAccounts.FirstOrDefault(a => (!a.IsLinkedInPlaid && a.ApplicantDocument.InstitutionId == institutionId) || (a.FileThisInstitutionId == institutionId && a.ApplicantDocument != null)) != null)
                    {
                        return new { IsSuccess = true, SuccessMessage = "Institution already linked.", InstitutionId = institutionId };
                    }
                }
            }

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();

            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());
                var _institution = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
                if (_institution != null && !string.IsNullOrEmpty(_institution.InvitationId))
                {
                    return "";
                }
            }

            Client = ProxyFactory.GetProxy(typeOfInstitution);


            var response = await Task.Run(() => Client.AddInstitutionResponseLink(customerId, institutionId, metaData, entityId));
            if (typeOfInstitution == InstitutionType.Banking)
            {
                return await Task.Run(() => ProcessMfaResponse(response, customerId, entityId));
            }

            if (!Configuration.UseSimulatorFor[typeOfInstitution])
            {
                return await Task.Run(() => ProcessConnectionResponse(response, customerId, typeOfInstitution, entityId));
            }

            var _institutionToCompare = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
            if (_institutionToCompare == null) // this means institution is not present in data attributes
            {

                var _newInstitution = new CustomerInstitution();
                _newInstitution.CustomerId = customerId;
                _newInstitution.InstitutionId = institutionId;
                _newInstitution.InvitationId = institutionId;
                _newInstitution.Type = typeOfInstitution;
                _newInstitution.ProviderId = _ProviderId;

                var allInstitutionResponse = await Task.Run(() => Client.GetAllInstitutionsResponse(typeOfInstitution));
                var newInstitutionDetail = allInstitutionResponse.Result.FirstOrDefault(a => a.InstitutionId == institutionId);
                if (newInstitutionDetail != null)
                {
                    _newInstitution.InstitutionName = newInstitutionDetail.Name;
                }
                listOfInstitutions.Add(_newInstitution);

                await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);
            }

            return response;

        }

        public async Task<object> UpdateInstitution(string customerId, string type, string institutionId, IInstitutionRequest request, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));
            if (string.IsNullOrEmpty(request.CustomerId))
                throw new ArgumentNullException(nameof(request.CustomerId));
            if (typeOfInstitution != InstitutionType.Banking && string.IsNullOrEmpty(request.InvitationId))
                throw new ArgumentNullException(nameof(request.InvitationId));
            if (string.IsNullOrEmpty(request.InstitutionId))
                throw new ArgumentNullException(nameof(request.InstitutionId));
            if (string.IsNullOrEmpty(request.UserName))
                throw new ArgumentNullException(nameof(request.UserName));
            if (string.IsNullOrEmpty(request.Secret))
                throw new ArgumentNullException(nameof(request.Secret));
            if (string.IsNullOrEmpty(request.PlaidItemId))
                throw new ArgumentNullException(nameof(request.PlaidItemId));

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();

            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());
                var _institution = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
                if (_institution != null && !string.IsNullOrEmpty(_institution.InvitationId))
                {
                    return "";
                }
            }

            Client = ProxyFactory.GetProxy(typeOfInstitution);


            var response = await Task.Run(() => Client.UpdateInstitutionResponse(customerId, institutionId, request));
            if (typeOfInstitution == InstitutionType.Banking)
            {
                return await Task.Run(() => ProcessMfaResponse(response, customerId, entityId));
            }


            var _institutionToCompare = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();
            if (_institutionToCompare == null) // this means institution is not present in data attributes
            {

                var _newInstitution = new CustomerInstitution();
                _newInstitution.CustomerId = customerId;
                _newInstitution.InstitutionId = request.InstitutionId;
                _newInstitution.InvitationId = request.InvitationId;
                _newInstitution.Type = typeOfInstitution;
                _newInstitution.ProviderId = _ProviderId;

                var allInstitutionResponse = await Task.Run(() => Client.GetAllInstitutionsResponse(typeOfInstitution));
                var newInstitutionDetail = allInstitutionResponse.Result.FirstOrDefault(a => a.InstitutionId == request.InstitutionId);
                if (newInstitutionDetail != null)
                {
                    _newInstitution.InstitutionName = newInstitutionDetail.Name;
                }
                listOfInstitutions.Add(_newInstitution);

                await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);
            }

            return response;

        }

        private async Task<bool> RenameEntityIdInDataAttribute(string entityType, string inviteId, string userName)
        {

            var dataResult = await DataAttributeClient.GetAllAttributes(entityType, inviteId);
            foreach (var data in dataResult)
            {
                string keyVal = data.Key;
                keyVal = keyVal.Substring(0, 1).ToUpper() + keyVal.Substring(1, keyVal.Length - 1); //First Character Upper
                var value = (Newtonsoft.Json.Linq.JArray)data.Value;
                await DataAttributeClient.SetAttribute(entityType, userName, keyVal, value[0].ToObject<dynamic>());
            }
            return true;
        }

        public async Task<bool> MapUsername(string inviteId, string userName)
        {
            await RenameEntityIdInDataAttribute(CustomerEntityType, inviteId, userName);

            await RenameEntityIdInDataAttribute(FileThisEntityType, inviteId, userName);

            await RenameEntityIdInDataAttribute(CustomerAccountDocumentEntityType, inviteId, userName);

            return true;
        }

        public async Task<string> AddManualBank(string customerId, IBankAccountInfoRequest request, string entityId = null)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(request.BankName))
                throw new ArgumentNullException(nameof(request.BankName));

            string bankName = request.BankName.Trim().ToLower();
            string _institutionId = string.Empty;

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, ManualBankingName, entityId);
            var listOfInstitutions = new List<Institution>();
            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<Institution>>(institutionList.ToString());
            }

            #region Search manual bank name matches with any already added manual bank institution

            // check with already added manual institutions
            var matchedInstitution = listOfInstitutions.Where(b => b.Name.ToLower().Contains(bankName)).FirstOrDefault();
            if (matchedInstitution != null)
            {
                _institutionId = matchedInstitution.InstitutionId;
            }

            #endregion

            if (string.IsNullOrEmpty(_institutionId))
            {
                // if no matches for new manual bank information 
                // then create new institution add into customer institution list

                Institution newManualBank = new Institution();
                newManualBank.Description = newManualBank.Name = request.BankName;
                _institutionId = newManualBank.InstitutionId = Guid.NewGuid().ToString();
                newManualBank.Type = InstitutionType.Banking;

                listOfInstitutions.Add(newManualBank);                
                await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, ManualBankingName, listOfInstitutions, entityId);
            }

            #region Add account detail of new manual bank


            // Add account information of manual bank

            var accounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
            var customerAccounts = new List<IApplicantBankAccountDocument>();
            if (accounts != null)
            {
                customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accounts.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
            }

            var matchedCustomerInstitutionAccount = customerAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount).FirstOrDefault(c => c.InstitutionId == _institutionId);
            IApplicantAccount newApplicantAccount = new ApplicantAccount();
            if (matchedCustomerInstitutionAccount != null)
            {
                newApplicantAccount = matchedCustomerInstitutionAccount;
            }
            else
            {
                newApplicantAccount.Plaid_Item_Id = Guid.NewGuid().ToString();
                newApplicantAccount.InstitutionId = _institutionId;
                newApplicantAccount.Name = request.BankName;
                newApplicantAccount.Accounts = new List<Account>();
            }
            Account newAccount = new Account();


            Balance balances = new Balance();
            newAccount.TotalBalance = balances.Available = Convert.ToString(request.CurrentBalance);

            newAccount.Account_Id = Guid.NewGuid().ToString();
            newAccount.Mask = request.AccountNumber;
            newAccount.IsManualUpload = true;
            newAccount.IsLinked = true;
            newAccount.SubType = request.AccountType;
            newAccount.OwnerName = request.AccountHolderName;
            newAccount.Balances = balances;

            newApplicantAccount.Accounts.Add(newAccount);

            if (matchedCustomerInstitutionAccount == null)
            {
                IApplicantBankAccountDocument applicationBankAccount = new ApplicantBankAccountDocument();
                applicationBankAccount.ApplicantAccount = newApplicantAccount;
                applicationBankAccount.IsManualUpload = true;
                customerAccounts.Add(applicationBankAccount);
            }

            await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), customerAccounts, entityId);
            
            #endregion
            return await Task.Run(() => string.Format("Institution: {0} added successfully.", request.BankName));
        }


        public async Task<object> GetChallenges(string institutionId, string customerId, string type, IConnectionStateRequest request, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));

            Client = ProxyFactory.GetProxy(typeOfInstitution);

            if (typeOfInstitution != InstitutionType.Banking && !Configuration.UseSimulatorFor[typeOfInstitution])
            {
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.AccountId == null)
                    throw new ArgumentNullException(nameof(request.AccountId));
                if (request.ConnectionId == null)
                    throw new ArgumentNullException(nameof(request.ConnectionId));

                Logger.Debug("FileThis interaction");
                return await Client.GetChallengesResponse(
                   applicationNumber: entityId, 
                   customerId: customerId, 
                   institutionId: institutionId, 
                   request: request);
            }

            IGetChallengeResponse challengeResult = new GetChallengeResponse();

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();
            if (institutionList != null)
            {

                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());

                try
                {
                    challengeResult = (IGetChallengeResponse)await Task.Run(() => 
                        Client.GetChallengesResponse(applicationNumber: entityId, customerId: customerId));
                }
                catch (Exception ex)
                {
                    if (ex is DocittCapacityConnectNetworkException)
                    {
                        var networkEx = ex as DocittCapacityConnectNetworkException;
                        if (networkEx.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            var _institution = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();

                            if (_institution != null && !_institution.IsChallengeCompleted)
                            {
                                _institution.IsChallengeCompleted = true;
                                await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);
                            }

                            switch (typeOfInstitution)
                            {
                                case InstitutionType.Banking:
                                    await GetCustomerAccounts(customerId, Convert.ToString(typeOfInstitution), entityId);
                                    break;
                                case InstitutionType.TaxReturn:
                                    await GetCustomerDocumentInfo(customerId, Convert.ToString(typeOfInstitution), entityId:entityId);
                                    break;
                                case InstitutionType.W2PayStub:
                                    break;
                                default:
                                    break;
                            }

                            return challengeResult;
                        }
                    }

                    throw ex;
                }

                // Get the institution Name from Institution Id
                string institutionName = string.Empty;

                IGetInstitutionByIdResponse singleInstitutionResult = await Task.Run(() => Client.GetInstitutionByIdResponse(institutionId, typeOfInstitution));

                if (singleInstitutionResult != null && singleInstitutionResult.Result != null)
                {
                    institutionName = singleInstitutionResult.Result.Name;
                }

                challengeResult.Result = challengeResult.Result.Where(c => c.IsComplete == false && c.Type == institutionName);
                if (challengeResult.Result.Count() == 0) // i.e. challenge is completed or no challenge for this institutionId
                {
                    var _institution = listOfInstitutions.Where(l => l.InstitutionId == institutionId).FirstOrDefault();

                    if (!_institution.IsChallengeCompleted)
                    {
                        _institution.IsChallengeCompleted = true;
                        await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);
                    }

                    switch (typeOfInstitution)
                    {
                        case InstitutionType.Banking:
                            await GetCustomerAccounts(customerId, Convert.ToString(typeOfInstitution), entityId);
                            break;
                        case InstitutionType.TaxReturn:
                            await GetCustomerDocumentInfo(customerId, Convert.ToString(typeOfInstitution), entityId:entityId);
                            break;
                        case InstitutionType.W2PayStub:
                            break;
                        default:
                            break;
                    }
                }

                return challengeResult;

            }
            else
            {
                throw new DocittCapacityConnectNetworkException();
            }



        }

        public async Task<IGetInvitationByIdResponse> UpdateCompanyInvitation(string companyId, IInvitation request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(companyId))
                throw new ArgumentNullException(nameof(companyId));
            if (string.IsNullOrEmpty(request.Id))
                throw new ArgumentNullException(nameof(request.Id));

            return await Task.Run(() => Client.UpdateCompanyInvitationResponse(companyId, request));
        }

        public async Task<object> ReplyChallenge(string customerId, object request, string type, string entityId = null)
        {
            Logger.Debug("ReplyChallenge..");
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));

            IChallengeReplyRequest challengeRequest = new ChallengeReplyRequest(new Response[] { });
            IMfaReplyRequest mfaRequest = new MfaReplyRequest();
            IFileThisMfaReplyRequest interactionRequest = new FileThisMfaReplyRequest();

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            object response;

            if (typeOfInstitution == InstitutionType.Banking)
            {
                Logger.Debug("ReplyChallenge.. Banking");
                mfaRequest = JsonConvert.DeserializeObject<MfaReplyRequest>(Convert.ToString(request));
                if (string.IsNullOrEmpty(mfaRequest.CustomerId))
                    throw new ArgumentNullException(nameof(mfaRequest.CustomerId));
                if (string.IsNullOrEmpty(mfaRequest.InstitutionId))
                    throw new ArgumentNullException(nameof(mfaRequest.InstitutionId));
                if (string.IsNullOrEmpty(mfaRequest.MfaType))
                    throw new ArgumentNullException(nameof(mfaRequest.MfaType));
                if (string.IsNullOrEmpty(mfaRequest.PublicToken))
                    throw new ArgumentNullException(nameof(mfaRequest.PublicToken));
                if (mfaRequest.Answers == null)
                    throw new ArgumentNullException(nameof(mfaRequest.Answers));

                response = await Task.Run(() => Client.ReplyChallengeResonse(
                    applicationNumber: entityId,
                    customerId: customerId, 
                    request: mfaRequest, 
                    type: typeOfInstitution));

                return await Task.Run(() => ProcessMfaResponse(response, customerId, entityId));
            }

            if (!Configuration.UseSimulatorFor[typeOfInstitution])
            {
                Logger.Debug("ReplyChallenge.. Banking");

                interactionRequest = JsonConvert.DeserializeObject<FileThisMfaReplyRequest>(Convert.ToString(request));
                if (string.IsNullOrEmpty(interactionRequest.AccountId))
                    throw new ArgumentNullException(nameof(interactionRequest.AccountId));
                if (string.IsNullOrEmpty(interactionRequest.ConnectionId))
                    throw new ArgumentNullException(nameof(interactionRequest.ConnectionId));
                if (string.IsNullOrEmpty(interactionRequest.InteractionId))
                    throw new ArgumentNullException(nameof(interactionRequest.InteractionId));
                if (interactionRequest.Inputs == null)
                    throw new ArgumentNullException(nameof(interactionRequest.Inputs));

                response = await Task.Run(() => Client.ReplyChallengeResonse(
                    applicationNumber: entityId,
                    customerId: customerId, 
                    request: interactionRequest, 
                    type: typeOfInstitution));
                // Return empty response always
                return string.Empty;
            }

            challengeRequest = JsonConvert.DeserializeObject<ChallengeReplyRequest>(Convert.ToString(request));
            if (string.IsNullOrEmpty(challengeRequest.CustomerId))
                throw new ArgumentNullException(nameof(challengeRequest.CustomerId));
            if (string.IsNullOrEmpty(challengeRequest.InstitutionId))
                throw new ArgumentNullException(nameof(challengeRequest.InstitutionId));
            if (string.IsNullOrEmpty(challengeRequest.InvitationId))
                throw new ArgumentNullException(nameof(challengeRequest.InvitationId));
            if (string.IsNullOrEmpty(challengeRequest.ProviderId))
                throw new ArgumentNullException(nameof(challengeRequest.ProviderId));
            if (string.IsNullOrEmpty(challengeRequest.ChallengeId))
                throw new ArgumentNullException(nameof(challengeRequest.ChallengeId));

             Logger.Debug($"ReplyChallenge.. for {typeOfInstitution}");

            response = await Task.Run(() => Client.ReplyChallengeResonse(
                applicationNumber: entityId,
                customerId: customerId, 
                request: challengeRequest, 
                type: typeOfInstitution));

            #region Set challenge completed to true for given Institution and fetch account / document list

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();
            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());
                var _institution = listOfInstitutions.Where(l => l.InstitutionId == challengeRequest.InstitutionId).FirstOrDefault();

                if (!_institution.IsChallengeCompleted)
                {
                    _institution.IsChallengeCompleted = true;
                    await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfInstitutions, entityId);

                    switch (typeOfInstitution)
                    {
                        case InstitutionType.Banking:
                            await GetCustomerAccounts(customerId, Convert.ToString(typeOfInstitution), entityId);
                            break;
                        case InstitutionType.TaxReturn:
                        case InstitutionType.W2PayStub:
                            await GetCustomerDocumentInfo(customerId, Convert.ToString(typeOfInstitution), entityId:entityId);
                            break;
                        default:
                            break;
                    }
                }
            }

            #endregion

            return response;
        }

        public async Task<IGetCustomerAccountResponse> GetCustomerAccounts(string customerId, string type, string applicationNumber = "")
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            var bankInstitutionType = InstitutionType.Banking;

            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));

            if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
            {
                Client = ProxyFactory.GetProxy(typeOfInstitution);
                var listOfCustomerIds = new List<string>();

                if(!string.IsNullOrEmpty(customerId))
                    listOfCustomerIds.Add(customerId);
                else
                    listOfCustomerIds = await GetCustomerIdsBasedOnRole(await GetCurrentUser(), applicationNumber);

                var accountResult = new List<object>();
                
                foreach (var borrowerCustomerId in listOfCustomerIds)
                {
                    customerId = borrowerCustomerId;

                    //allBorrowersResponse.Result
                    // Get stored customer accounts
                    var accounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(bankInstitutionType), applicationNumber);
                    if (accounts == null)
                    {
                        continue;
                    }

                    var customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accounts.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                    if (typeOfInstitution == InstitutionType.Banking) // If it is banking then do not return filethis linked institutions
                    {
                        customerAccounts = customerAccounts.Where(x => x.ApplicantAccount != null).ToList(); // this will fix issue of bank alert info
                    }

                    accountResult.AddRange(GetBankSummary(customerAccounts, customerId));
                }
                // TODO: temparory fix to match previous response.
                var jsonResult = JsonConvert.SerializeObject(accountResult);
                return await Task.Run(() => new GetCustomerAccountResponse() { Result = JsonConvert.DeserializeObject<List<object>>(jsonResult) });
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.BadRequest, Response = "Institution type is not supported for get accounts." };
        }

        public async Task<bool> DeleteCustomerAccountDocument(string customerId, string type, string entiyType, string accountId, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            var ack = false;
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(accountId))
                throw new ArgumentNullException(nameof(accountId));
            var institutionTypeBanking = InstitutionType.Banking;

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            var accountDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution == InstitutionType.FileThisBanking ? institutionTypeBanking : typeOfInstitution), entityId);
            object updatedAccountDocuments = new List<ApplicantAccount>();
            if (accountDocuments != null)
            {
                var customerInstitutionIds = new List<string>();
                var manualInstitutionIds = new List<string>();

                switch (typeOfInstitution)
                {
                    case InstitutionType.Banking:
                        var customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accountDocuments.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                        var account = customerAccounts.Where(x=>x.ApplicantAccount != null).Select(x => x.ApplicantAccount).SelectMany(x => x.Accounts).Where(z => z.Account_Id == accountId).FirstOrDefault();
                        if (account == null)
                            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("AccountId: {0} does not exist.", accountId) };

                        if (!account.IsManualUpload) // call only if account found is not manually uploaded
                        {
                            ack = await Client.DeleteCustomerAccountDocumentResponse(customerId, typeOfInstitution, accountId);
                        }
                        else
                        { // If document is manual upload
                            ack = true;
                        }
                        
                        customerAccounts.Where(x => x.ApplicantAccount != null).ToList().ForEach(x => x.ApplicantAccount.Accounts.Remove(account));
                        // TODO: add logic to remove plaid bank based on is available in FileThis

                        foreach (var appBankAccountDoc in customerAccounts)
                        {
                            if (appBankAccountDoc.ApplicantAccount != null && !appBankAccountDoc.ApplicantAccount.Accounts.Any())
                            {
                                manualInstitutionIds.Add(appBankAccountDoc.ApplicantAccount.InstitutionId);
                                appBankAccountDoc.ApplicantAccount = null;
                            }
                        }

                        var emptyAccountCustomer = customerAccounts
                            //.Where(x => x.IsLinkedInPlaid && x.ApplicantDocument == null && x.ApplicantAccount == null)
                            .Where(x => x.ApplicantDocument == null && x.ApplicantAccount == null)
                            .ToList();

                        var finalCustomerAccounts = customerAccounts.Except(emptyAccountCustomer);

                        updatedAccountDocuments = finalCustomerAccounts;
                        break;
                    case InstitutionType.FileThisBanking:
                        DeleteEntityType deleteTypeEntity = EnsureDeleteEntityType(entiyType);
                        var customerAccountsDocs = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accountDocuments.ToString()).Cast<IApplicantBankAccountDocument>().ToList();

                        if (deleteTypeEntity == DeleteEntityType.institution)
                        {
                            var applicantDocument = customerAccountsDocs.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument)
                                                    .FirstOrDefault(x => x.InstitutionId == accountId);
                            applicantDocument.ListOfDocuments.Clear();
                            ack = true;
                        }
                        else
                        {
                            var applicantdocumentFileThis = customerAccountsDocs.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument).SelectMany(x => x.ListOfDocuments).FirstOrDefault(z => z.DocumentId == accountId);
                            if (applicantdocumentFileThis == null)
                            {
                                var manualDocument = customerAccountsDocs.Where(x => x.ApplicantDocument != null && x.IsManualUpload).Select(x => x.ApplicantDocument).SelectMany(x => x.ListOfDocuments).SelectMany(x => x.Documents).FirstOrDefault(z => z.DocumentId == accountId);
                                if (manualDocument == null)
                                {
                                    throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("DocumentId: {0} does not exist.", accountId) };
                                }
                                else
                                {
                                    ack = true; // remove documents uploaded manually
                                    customerAccountsDocs.Where(x => x.ApplicantDocument != null).ToList().ForEach(x => x.ApplicantDocument.ListOfDocuments.ForEach(y => y.Documents.Remove(manualDocument)));
                                }
                            }
                            else
                            {
                                if (!applicantdocumentFileThis.IsManualUpload) // call only if account found is not manually uploaded
                                {
                                    ack = await Client.DeleteCustomerAccountDocumentResponse(customerId, typeOfInstitution, accountId);
                                }
                                else
                                { // If document is manual upload
                                    ack = true;
                                }

                                customerAccountsDocs.Where(x => x.ApplicantDocument != null).ToList().ForEach(x => x.ApplicantDocument.ListOfDocuments.Remove(applicantdocumentFileThis));
                            }
                        }

                        // If there is all documents are deleted then remove that institution
                        foreach (var appBankAccountDoc in customerAccountsDocs)
                        {
                            // Considering delete for not connected and connected filethisInstitution both.
                            if (appBankAccountDoc.ApplicantDocument != null
                                && (appBankAccountDoc.ApplicantDocument.State.Equals(STATE_CONNECTED)
                                        || (appBankAccountDoc.ApplicantDocument.InstitutionId == accountId && deleteTypeEntity == DeleteEntityType.institution)
                                    ) && !appBankAccountDoc.ApplicantDocument.ListOfDocuments.Any())
                            {
                                if (!appBankAccountDoc.IsManualUpload)
                                    await Client.DeleteCustomerConnectionResponse(appBankAccountDoc.ApplicantDocument.FileThisAccountId, appBankAccountDoc.ApplicantDocument.ConnectionId);
                                appBankAccountDoc.ApplicantDocument = null;
                            }
                        }

                        var emptyApplicantDocumentFileThis = customerAccountsDocs.Where(x => x.ApplicantDocument == null && x.ApplicantAccount == null).ToList();

                        var finalApplicantDocumentsFileThis = customerAccountsDocs.Except(emptyApplicantDocumentFileThis);
                        updatedAccountDocuments = finalApplicantDocumentsFileThis;
                        break;
                    case InstitutionType.TaxReturn:
                    case InstitutionType.W2PayStub:
                        if (!Configuration.UseSimulatorFor[typeOfInstitution])
                        {
                            var applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(accountDocuments.ToString()).Cast<IApplicantDocument>().ToList();

                            var applicantdocument = applicantDocuments.SelectMany(x => x.ListOfDocuments).FirstOrDefault(z => z.DocumentId == accountId);
                            if (applicantdocument == null)
                                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("DocumentId: {0} does not exist.", accountId) };


                            if (!applicantdocument.IsManualUpload) // call only if account found is not manually uploaded
                            {
                                ack = await Client.DeleteCustomerAccountDocumentResponse(customerId, typeOfInstitution, accountId);
                            }
                            else
                            { // If document is manual upload
                                ack = true;
                            }

                            // If there is all documents are deleted then remove that institution
                            applicantDocuments.ForEach(x => x.ListOfDocuments.Remove(applicantdocument));
                            var emptyApplicantDocument = applicantDocuments.Where(x => x.State.Equals(STATE_CONNECTED) && !x.ListOfDocuments.Any()).ToList();
                            if (emptyApplicantDocument != null)
                            {
                                foreach (var applicantInstitution in emptyApplicantDocument)
                                {
                                    // Delete connection when all document get deleted
                                    await Client.DeleteCustomerConnectionResponse(applicantInstitution.FileThisAccountId, applicantInstitution.ConnectionId);
                                }
                            }
                            var finalApplicantDocuments = applicantDocuments.Except(emptyApplicantDocument);
                            updatedAccountDocuments = finalApplicantDocuments;
                            break;
                        }

                        var customerDocuments = JsonConvert.DeserializeObject<List<CustomerDocumentInfo>>(accountDocuments.ToString()).Cast<ICustomerDocumentInfo>().ToList();
                        var document = customerDocuments.Where(a => a.Id == accountId).FirstOrDefault();
                        if (document == null)
                            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("DocumentId: {0} does not exist.", accountId) };

                        // call only if document found from data attribute and is not manual upload
                        if (!document.IsManualUpload)
                            await Client.DeleteCustomerAccountDocumentResponse(customerId, typeOfInstitution, accountId);

                        customerDocuments.Remove(document);
                        customerInstitutionIds = customerDocuments.Select(a => a.InstitutionId).Distinct().ToList();
                        updatedAccountDocuments = customerDocuments;
                        break;
                }

                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution == InstitutionType.FileThisBanking ? institutionTypeBanking : typeOfInstitution), updatedAccountDocuments, entityId);

                // remove customer institution
                var customerInstitutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                var listOfCustomerInstitutions = new List<CustomerInstitution>();
                if (customerInstitutionList != null)
                {
                    listOfCustomerInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(customerInstitutionList.ToString());
                    listOfCustomerInstitutions = listOfCustomerInstitutions.Where(c => customerInstitutionIds.Contains(c.InstitutionId)).ToList();
                    await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfCustomerInstitutions, entityId);
                }

                // remove manual institution
                if (typeOfInstitution == InstitutionType.Banking)
                {
                    var manualInstitutionList = GetFirstAttributeValue(CustomerEntityType, customerId, ManualBankingName, entityId);
                    var listOfManualInstitutions = new List<Institution>();
                    if (manualInstitutionList != null)
                    {
                        listOfManualInstitutions = JsonConvert.DeserializeObject<List<Institution>>(manualInstitutionList.ToString());
                        listOfManualInstitutions = listOfManualInstitutions.Where(c => manualInstitutionIds.Contains(c.InstitutionId)).ToList();
                        await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, ManualBankingName, listOfManualInstitutions, entityId);
                    }
                }

            }
            if (!ack)
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.BadRequest, Response = string.Format("DocumentId: {0} does not exist.", accountId) };
            }

            return ack;
        }

        public async Task<object> GetCustomerAccountTransactions(string customerId, string accountId, string entityId = null)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(accountId))
                throw new ArgumentNullException(nameof(accountId));

            var typeOfInstitution = InstitutionType.Banking;

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            var accountDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);


            if (accountDocuments != null)
            {

                var customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accountDocuments.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                var matchedPlaidItem = customerAccounts.Where(a => a.ApplicantAccount != null).Select(a => a.ApplicantAccount).FirstOrDefault(b => b.Accounts.Any(a => a.Account_Id == accountId));

                if (matchedPlaidItem == null)
                    throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("AccountId: {0} does not exist.", accountId) };

                var account = matchedPlaidItem.Accounts.FirstOrDefault(a => a.Account_Id == accountId);

                if (!account.IsManualUpload) // call only if account found is not manually uploaded
                {
                    IBankAccountTransactionRequest transactionRequest = new BankAccountTransactionRequest();
                    transactionRequest.Count = Configuration.BankTransactionCountPerPage;
                    transactionRequest.PlaidAccountId = matchedPlaidItem.Plaid_Item_Id;
                    transactionRequest.PlaidAccountId = accountId;
                    transactionRequest.EndDate = TenantTime.Today.Date.ToString(DATE_FORMATE_MMDDYYYY);
                    transactionRequest.StartDate = TenantTime.Today.Date.AddDays(-Convert.ToDouble(Configuration.BankTransactionPeriodInDays)).ToString("MM/dd/yyyy");

                    var transactions = await Client.GetCustomerAccountTransactionsResponse(customerId, transactionRequest, entityId);
                    foreach (var itemTransaction in transactions)
                    {
                        itemTransaction.ShortDate = itemTransaction.Date.ToString(DATE_FORMATE_MMDDYYYY);
                    }
                    IGetCustomerAccountTransactionsResponse response = new GetCustomerAccountTransactionsResponse();
                    response.AccountMask = account.Mask;
                    response.AccountSubType = account.SubType;
                    response.AccountType = account.Type;
                    response.InstitutionName = matchedPlaidItem.Name;
                    response.CurrentBalance = account.Balances.Current;
                    response.TransactionStartDate = transactionRequest.StartDate;
                    response.TransactionEndDate = transactionRequest.EndDate;
                    response.TransactionPeriodInDays = Configuration.BankTransactionPeriodInDays;
                    response.Transactions = transactions.OrderByDescending(o => o.Date).ToList();

                    return response;
                }

            }

            return null;

        }

        public async Task<object> GetCustomerLargeDepositTransactions(string customerId, string applicationNumber, bool returnTrnsactions = false)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            var loanDetail = await QuestionnaireClient.GetApplicationLoanDetail(applicationNumber, customerId);

            IBusinessRulesRequest ruleRequest = new BusinessRulesRequest();
            ruleRequest.Income = loanDetail.AnnualGrossIncome;
            ruleRequest.PurchasePrice = string.IsNullOrEmpty(loanDetail.PurchasePrice.ToString()) ? 0 : Convert.ToDouble(loanDetail.PurchasePrice);
            // "822138"
            var result = await BusinessRuleClient.LargeDepositsGivenApplicantId(customerId, ruleRequest, applicationNumber);

            if (result == null || returnTrnsactions)
            {
                return result;
            }
            else
            {
                return result.Any();
            }
        }

        public async Task<object> SyncCustomerAccountDocument(string customerId, string type, string institutionId, string entityId)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException(nameof(entityId));

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            var accountDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);

            if (typeOfInstitution == InstitutionType.Banking)
            {
                SyncAccount updatedAccount = (SyncAccount)await Client.SyncCustomerAccountDocumentResponse(customerId, institutionId, entityId);
                if (updatedAccount.Has_Error)
                {
                    updatedAccount.IsSuccess = false;
                    updatedAccount.IsCredentialsChanged = updatedAccount.Error.Error_Code.Equals(LoginRequiredErrorCode, StringComparison.InvariantCultureIgnoreCase);
                    return updatedAccount;
                }

                if (updatedAccount.Account == null)
                {
                    throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No account information available for accountId: {0}.", entityId) };
                }

                var customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accountDocuments.ToString());
                var accountToUpdate = customerAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount).FirstOrDefault(a => a.InstitutionId == institutionId).Accounts.FirstOrDefault(a => a.Account_Id == entityId);
                accountToUpdate.Balances.Available = updatedAccount.Account.Balances.Available;
                accountToUpdate.Balances.Current = updatedAccount.Account.Balances.Current;
                updatedAccount.Account.TotalBalance = accountToUpdate.TotalBalance = accountToUpdate.Balances.Available != null ? accountToUpdate.Balances.Available : accountToUpdate.Balances.Current;

                customerAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount).FirstOrDefault(x => x.InstitutionId == institutionId).Accounts.ForEach(y =>
                {
                    if (y.Account_Id == entityId)
                    {
                        y = accountToUpdate;
                    }
                });

                #region If plaid bank has also linked with fileThis banking then update documents

                var customerBank = customerAccounts.FirstOrDefault(x => x.ApplicantAccount != null && x.ApplicantAccount.Accounts.Any(y => y.Account_Id == entityId));
                // if filethis instituion is linked and is not null
                if (customerBank.ApplicantDocument != null && customerBank.IsFileThisBankLinked)
                {
                    // Sync filethis documents
                    customerBank.ApplicantDocument = (IApplicantDocument)await SyncCustomerDocuments(customerId, Convert.ToString(InstitutionType.FileThisBanking), customerBank.FileThisInstitutionId, entityId);
                    await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), customerAccounts, entityId);

                    var accountsWithDocuments = AddFileThisLinkedDocumentsIntoEachBankAccount(customerBank.ApplicantDocument, customerBank.ApplicantAccount.Accounts);
                    updatedAccount.Account = accountsWithDocuments.FirstOrDefault(a => a.Account_Id == entityId);
                    return updatedAccount;
                }

                #endregion

                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), customerAccounts, entityId);

                return updatedAccount;
            }

            return null;
        }

        public async Task<object> SyncCustomerDocuments(string customerId, string type, string institutionId, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(institutionId))
                throw new ArgumentNullException(nameof(institutionId));

            Client = ProxyFactory.GetProxy(typeOfInstitution);
            if (typeOfInstitution == InstitutionType.Banking || Configuration.UseSimulatorFor[typeOfInstitution])
            {
                throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.BadRequest, Response = string.Format("Institution type is not supported.") };
            }

            switch (typeOfInstitution)
            {
                case InstitutionType.TaxReturn:
                case InstitutionType.W2PayStub:
                    // For FileThis sync documents of given institution
                    var documentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                    List<IApplicantDocument> applicantDocuments = new List<IApplicantDocument>();
                    if (documentInfoList != null)
                        applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(documentInfoList.ToString()).Cast<IApplicantDocument>().ToList();

                    var applicantDocument = applicantDocuments.FirstOrDefault(d => d.InstitutionId == institutionId);

                    if (applicantDocument == null)
                    {
                        throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.BadRequest, Response = string.Format("InstitutionId: {0} does not exist.", institutionId) };
                    }

                    var response = await Client.SyncCustomerAccountDocumentResponse(customerId, institutionId, applicantDocument.ConnectionId);
                    applicantDocument = await AddConnectionDocumentsToApplicantDocument(customerId, applicantDocument, response, typeOfInstitution);
                    applicantDocument.State = STATE_CONNECTED;
                    await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), applicantDocuments, entityId);
                    return applicantDocument;
                case InstitutionType.FileThisBanking:
                    // For FileThis sync documents of given institution
                    var accountDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
                    List<IApplicantBankAccountDocument> applicantBankAccountDocuments = new List<IApplicantBankAccountDocument>();
                    if (accountDocuments != null)
                        applicantBankAccountDocuments = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accountDocuments.ToString()).Cast<IApplicantBankAccountDocument>().ToList();

                    var applicantDocumentForFileThis = applicantBankAccountDocuments.Where(x => x.ApplicantDocument != null)
                                        .Select(x => x.ApplicantDocument).FirstOrDefault(d => d.InstitutionId == institutionId);

                    if (applicantDocumentForFileThis == null)
                    {
                        throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.BadRequest, Response = string.Format("InstitutionId: {0} does not exist.", institutionId) };
                    }

                    var documentListResponse = await Client.SyncCustomerAccountDocumentResponse(customerId, institutionId, applicantDocumentForFileThis.ConnectionId);
                    applicantDocumentForFileThis = await AddConnectionDocumentsToApplicantDocument(customerId, applicantDocumentForFileThis, documentListResponse, InstitutionType.Banking);
                    applicantDocumentForFileThis.State = STATE_CONNECTED;
                    await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), applicantBankAccountDocuments, entityId);
                    return applicantDocumentForFileThis;
            }
            return null;
        }

        public async Task<object> GetCustomerDocumentInfo(string customerId, string type, string connectionId = default(string), string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));

            var documentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            if (!Configuration.UseSimulatorFor[typeOfInstitution])
            {
                List<IApplicantDocument> applicantDocuments = new List<IApplicantDocument>();

                if (documentInfoList != null)
                    applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(documentInfoList.ToString()).Cast<IApplicantDocument>().ToList();

                // return all institutions as all are by default isLinked = true when it is added.
                return applicantDocuments; // .Where(a => a.IsLinked).ToList();
            }

            Client = ProxyFactory.GetProxy(typeOfInstitution);

            if (documentInfoList == null)
            {
                var result = (IGetCustomerDocumentInfoResponse)await Task.Run(() => Client.GetCustomerDocumentInfoResponse(customerId, typeOfInstitution, connectionId));

                // Filter deleted records                
                result.Result = result.Result.Where(a => !a.IsDeleted).ToList();

                #region Fetch document from remote and store on document manager

                foreach (var documentInfo in result.Result)
                {
                    var _newDoc = await Client.DownloadCustomerDocumentResponse(customerId, documentInfo.Id);

                    string metaData = "{" + string.Format(" \"typeOfInstitution\":\"{0}\", \"documentId\":\"{1}\", \"institutionId\":\"{2}\", \"institutionType\":\"{3}\" ", documentInfo.Type, documentInfo.Id, documentInfo.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                    var _objMetaDta = JsonConvert.DeserializeObject<object>(metaData);

                    var newDocumentInfo = await DocumentManagerClient.Create(_newDoc, CustomerEntityType, customerId, documentInfo.Type + DefaultFileExtension, _objMetaDta);
                    documentInfo.FileId = newDocumentInfo.Id;
                    IAccountDocument newAccount = new AccountDocument() { DocumentInfo = newDocumentInfo };
                    documentInfo.Documents = new List<IAccountDocument>();
                    documentInfo.Documents.Add(newAccount);
                }

                #endregion

                if (result.Result != null && result.Result.Count() > 0)
                {
                    await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), result.Result, entityId);
                }

                return result;
            }

            var institutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var listOfInstitutions = new List<CustomerInstitution>();
            if (institutionList != null)
            {
                listOfInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(institutionList.ToString());
            }

            List<ICustomerDocumentInfo> customerDocuments = JsonConvert.DeserializeObject<List<CustomerDocumentInfo>>(documentInfoList.ToString()).Cast<ICustomerDocumentInfo>().ToList();
            var customerDocumentInstitutions = customerDocuments.Select(s => s.InstitutionId).ToList();

            if (!listOfInstitutions.Where(l => l.IsChallengeCompleted).All(l => customerDocumentInstitutions.Contains(l.InstitutionId)))
            {
                var result = (IGetCustomerDocumentInfoResponse)await Task.Run(() => Client.GetCustomerDocumentInfoResponse(customerId, typeOfInstitution, connectionId));
                var newDocuments = result.Result.Where(d => !customerDocuments.Any(c => c.Id == d.Id));

                if (newDocuments != null)
                {
                    newDocuments = newDocuments.ToList();
                }

                customerDocuments.AddRange(newDocuments);

                #region Fetch document from remote and store on document manager

                foreach (var documentInfo in newDocuments)
                {
                    var _newDoc = await Client.DownloadCustomerDocumentResponse(customerId, documentInfo.Id);


                    string metaData = "{" + string.Format(" \"type\":\"{0}\", \"documentId\":\"{1}\", \"institutionId\":\"{2}\", \"institutionType\":\"{3}\" ", documentInfo.Type, documentInfo.Id, documentInfo.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                    var _objMetaDta = JsonConvert.DeserializeObject<object>(metaData);

                    var newDocumentInfo = await DocumentManagerClient.Create(_newDoc, CustomerEntityType, customerId, documentInfo.Type + DefaultFileExtension, _objMetaDta);

                    documentInfo.FileId = newDocumentInfo.Id;

                    IAccountDocument newAccount = new AccountDocument() { DocumentInfo = newDocumentInfo };
                    documentInfo.Documents = new List<IAccountDocument>();
                    documentInfo.Documents.Add(newAccount);
                }

                #endregion

                // Filter deleted records
                customerDocuments = customerDocuments.Where(a => !a.IsDeleted).ToList();
                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), customerDocuments, entityId);

                // return await Task.Run(() => new GetCustomerDocumentInfoResponse() { Result = result.Result });
            }

            return await Task.Run(() => new GetCustomerDocumentInfoResponse() { Result = customerDocuments });
        }

        public async Task<Stream> DownloadCustomerDocument(string customerId, string documentId)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(documentId))
                throw new ArgumentNullException(nameof(documentId));

            var allDocuments = await DocumentManagerClient.GetAll(CustomerEntityType, customerId);
            if (allDocuments.Any(d => d.Metadata.ToString().Contains(documentId)))
            {
                var _doc = allDocuments.Where(d => d.Metadata.ToString().Contains(documentId)).FirstOrDefault();

                return await DocumentManagerClient.Download(CustomerEntityType, customerId, _doc.Id);
            }

            throw new DocittCapacityConnectNetworkException() { HttpStatusCode = HttpStatusCode.NotFound, Response = string.Format("No document found with documentId: {0}.", documentId) };
        }

        public async Task AllDoneSendToRequiredConditions(string customerId, string type, IAllDoneRequest request, string entityId = null)
        {
            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));
            if (string.IsNullOrEmpty(request.ApplicationNumber))
                throw new ArgumentNullException(nameof(request.ApplicationNumber));
            if (string.IsNullOrEmpty(request.CreatedBy))
                throw new ArgumentNullException(nameof(request.CreatedBy));
            if (string.IsNullOrEmpty(request.EntityType))
                throw new ArgumentNullException(nameof(request.EntityType));
            if (string.IsNullOrEmpty(request.RequestId))
                throw new ArgumentNullException(nameof(request.RequestId));

            var templateForWriteExplanation = Configuration.WriteExplanationTemplates[typeOfInstitution];
            var explanationHtml = string.Empty;
            string explnationToSubmit = string.Empty;
            string documentEntityType = Configuration.UseSimulatorFor[typeOfInstitution] ? CustomerEntityType : FileThisEntityType;
            var idsToSubmit = new List<string>();
            var customerAccountDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution == InstitutionType.FileThisBanking ? InstitutionType.Banking : typeOfInstitution), entityId);
            var notSubmittedDocuments = new List<string>();

            var bankAccounts = new List<IApplicantBankAccountDocument>();
            var nonBankingDocuments = new List<ICustomerDocumentInfo>();
            var applicantDocuments = new List<IApplicantDocument>();

            bool isExplanationSubmitted = false;
            dynamic templatePayload;

            if (customerAccountDocuments == null)
            {
                await RequiredConditionClient.MarkAsSkipped(request.EntityType, request.ApplicationNumber, request.RequestId);
                return;
            }

            if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
            {
                bankAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(customerAccountDocuments.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                notSubmittedDocuments = bankAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount)
                    .SelectMany(a => a.Accounts).Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => !d.IsSubmitted).Select(d => d.DocumentInfo.Id).ToList();

                notSubmittedDocuments.AddRange(bankAccounts.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument)
                                        .Where(a => a.State.Equals(STATE_CONNECTED)).SelectMany(a => a.ListOfDocuments)
                                        .Where(a => a.Documents != null)
                                        .SelectMany(d => d.Documents).Where(d => !d.IsSubmitted).Select(d => d.DocumentInfo.Id).ToList());

                templatePayload = await GenerateExplanationPayload(bankAccounts.Cast<object>().ToList(), typeOfInstitution);
            }
            else if (Configuration.UseSimulatorFor[typeOfInstitution])
            {
                nonBankingDocuments = JsonConvert.DeserializeObject<List<CustomerDocumentInfo>>(customerAccountDocuments.ToString()).Cast<ICustomerDocumentInfo>().ToList();
                notSubmittedDocuments = nonBankingDocuments.Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => !d.IsSubmitted).Select(d => d.DocumentInfo.Id).ToList();
                templatePayload = await GenerateExplanationPayload(nonBankingDocuments.Cast<object>().ToList(), typeOfInstitution);
            }
            else
            {
                applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(customerAccountDocuments.ToString()).Cast<IApplicantDocument>().ToList();
                notSubmittedDocuments = applicantDocuments.Where(a => a.State.Equals(STATE_CONNECTED)).SelectMany(a => a.ListOfDocuments).Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => !d.IsSubmitted).Select(d => d.DocumentInfo.Id).ToList();
                templatePayload = await GenerateExplanationPayload(applicantDocuments.Cast<object>().ToList(), typeOfInstitution);
            }

            if (templatePayload.items.Count > 0)
            {
                var strPayload = JsonConvert.SerializeObject(templatePayload.items);
                idsToSubmit = ((List<dynamic>)templatePayload.items).Select(d => d.id).Cast<string>().ToList();

                var result = await TemplateManager.ProcessActiveTemplate(templateForWriteExplanation.TemplateName, templateForWriteExplanation.TemplateFormat, templatePayload);
                explanationHtml = result.Data;
            }
            else
            {
                var requiredConditionList = RequiredConditionClient.GetByStatus(request.EntityType, request.ApplicationNumber, RequestStatusPending);
                var requiredCondition = requiredConditionList.FirstOrDefault(r => r.Id == request.RequestId);

                // If given request's required condition's status is not Pending then only mark it as Skipped
                if (requiredCondition == null)
                {
                    await RequiredConditionClient.MarkAsSkipped(request.EntityType, request.ApplicationNumber, request.RequestId);
                }

                isExplanationSubmitted = true;
            }

            explnationToSubmit = isExplanationSubmitted ? string.Empty : explanationHtml;
            IList<DocumentDetails> documentsToSubmit = new List<DocumentDetails>();
            var documentSubmitted = new List<string>();

            var ipAddress = GetClientIp();
            string metaData = string.Format("{{ \"requestId\":\"{0}\", \"createdBy\":\"{1}\", \"ipAddress\": \"{2}\" }}", request.RequestId, request.CreatedBy, ipAddress);
            var _objMetaDta = JsonConvert.DeserializeObject<object>(metaData);

            if (notSubmittedDocuments.Count > 0)
            {
                var allDocuments = await DocumentManagerClient.GetAll(documentEntityType, customerId);
                allDocuments = allDocuments.Where(x => notSubmittedDocuments.Contains(x.Id)).ToList();

                foreach (var documentInfo in allDocuments)
                {
                    DocumentDetails docDetail = new DocumentDetails();
                    docDetail.FileName = documentInfo.FileName;
                    docDetail.Content = await DocumentManagerClient.Download(documentEntityType, customerId, documentInfo.Id);
                    documentsToSubmit.Add(docDetail);
                    documentSubmitted.Add(documentInfo.Id);

                    if (documentsToSubmit.Count == Configuration.MaxFilesToRequiredConditionRequest)
                    {
                        // If explanation already submitted then set it empty.
                        explnationToSubmit = isExplanationSubmitted ? string.Empty : explanationHtml;

                        await RequiredConditionClient.SubmitDocumentWithExplanation(request.EntityType, request.ApplicationNumber, request.RequestId, documentsToSubmit, _objMetaDta, explnationToSubmit);
                        isExplanationSubmitted = true;

                        if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
                        {
                            // Get plaid bank accounts
                            var accountDocuments = bankAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount)
                                                .SelectMany(b => b.Accounts).Where(a => a.Documents != null).SelectMany(d => d.Documents)
                                                .Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();
                            // FileThis bank documents
                            accountDocuments.AddRange(bankAccounts.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument)
                                        .Where(a => a.State.Equals(STATE_CONNECTED)).SelectMany(a => a.ListOfDocuments)
                                        .Where(a => a.Documents != null).SelectMany(d => d.Documents)
                                        .Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)));
                            foreach (var accountDocument in accountDocuments)
                            {
                                accountDocument.IsSubmitted = true;
                            }
                        }
                        else if (Configuration.UseSimulatorFor[typeOfInstitution])
                        {
                            var documents = nonBankingDocuments.Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();
                            foreach (var accountDocument in documents)
                            {
                                accountDocument.IsSubmitted = true;
                            }
                        }
                        else // this means taxreturn or w2paystubs
                        {
                            var applicantAllDocuments = applicantDocuments.SelectMany(b => b.ListOfDocuments).Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();
                            foreach (var accountDocument in applicantAllDocuments)
                            {
                                accountDocument.IsSubmitted = true;
                            }
                        }

                        documentsToSubmit.Clear();
                        documentSubmitted.Clear();
                    }
                }
            }

            if (documentsToSubmit.Count > 0 || !isExplanationSubmitted)
            {
                await RequiredConditionClient.SubmitDocumentWithExplanation(request.EntityType, request.ApplicationNumber, request.RequestId, documentsToSubmit, _objMetaDta, explnationToSubmit);
                isExplanationSubmitted = true;

                // Mark each submitted document as submitted
                if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
                {
                    var accountDocuments = bankAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount)
                                  .SelectMany(b => b.Accounts).Where(a => a.Documents != null).SelectMany(d => d.Documents)
                                  .Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();

                    accountDocuments.AddRange(bankAccounts.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument)
                                .Where(a => a.State.Equals(STATE_CONNECTED)).SelectMany(a => a.ListOfDocuments)
                                .Where(a => a.Documents != null).SelectMany(d => d.Documents)
                                .Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)));
                    foreach (var accountDocument in accountDocuments)
                    {
                        accountDocument.IsSubmitted = true;
                    }
                }
                else if (Configuration.UseSimulatorFor[typeOfInstitution])
                {
                    var documents = nonBankingDocuments.Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();
                    foreach (var accountDocument in documents)
                    {
                        accountDocument.IsSubmitted = true;
                    }
                }
                else
                {
                    var applicantAllDocuments = applicantDocuments.SelectMany(b => b.ListOfDocuments).Where(a => a.Documents != null).SelectMany(d => d.Documents).Where(d => documentSubmitted.Contains(d.DocumentInfo.Id)).ToList();
                    foreach (var accountDocument in applicantAllDocuments)
                    {
                        accountDocument.IsSubmitted = true;
                    }
                }
            }

            if (typeOfInstitution == InstitutionType.Banking || typeOfInstitution == InstitutionType.FileThisBanking)
            {
                // set isSubmitted to true for accounts which are submitted
                var accountsToMarkSubmitted = bankAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount)
                    .SelectMany(b => b.Accounts).Where(a => idsToSubmit.Contains(a.Account_Id)).ToList();

                var bankDocumentsToMarkSubmitted = bankAccounts.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument).SelectMany(b => b.ListOfDocuments).Where(a => idsToSubmit.Contains(a.DocumentId)).ToList();
                foreach (var account in accountsToMarkSubmitted)
                {
                    account.IsSubmitted = true;
                }
                foreach (var document in bankDocumentsToMarkSubmitted)
                {
                    document.IsSubmitted = true;
                }
                // Save updated submitted flags
                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution == InstitutionType.FileThisBanking ? InstitutionType.Banking : typeOfInstitution), bankAccounts, entityId);
            }
            else if (Configuration.UseSimulatorFor[typeOfInstitution])
            {
                // set isSubmitted to true for documents which are submitted
                var customerDocumentsToMarkSubmitted = nonBankingDocuments.Where(a => idsToSubmit.Contains(a.Id)).ToList();
                foreach (var customerDocument in customerDocumentsToMarkSubmitted)
                {
                    customerDocument.IsSubmitted = true;
                }
                // Save updated submitted flags
                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), nonBankingDocuments, entityId);
            }
            else
            {
                // set isSubmitted to true for accounts which are submitted
                var documentsToMarkSubmitted = applicantDocuments.SelectMany(b => b.ListOfDocuments).Where(a => idsToSubmit.Contains(a.DocumentId)).ToList();
                foreach (var document in documentsToMarkSubmitted)
                {
                    document.IsSubmitted = true;
                }
                // Save updated submitted flags
                await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), applicantDocuments, entityId);
            }

        }

        #endregion

        #region Assets

        public async Task<IGetCustomerByIdResponse> CreateCustomerWithInvitationConfig(string companyId, ICustomerRequest request)
        {
            Client = ProxyFactory.GetProxy(InstitutionType.All);

            string customerId = string.Empty;
            string invitationId = string.Empty;
            string invitationCode = string.Empty;
            string companyInviteConfigId = string.Empty;

            // Need to store companyId, customerId, invitationId in DB
            request.MonthlyGrossIncome = 100000;
            request.SalesPrice = 1000000;

            #region Check if Customer exists 

            var _customerInfo = await DataAttributeClient.GetFirstAttributeValue(CustomerEntityType, request.Email, "customerInfo");
            if (_customerInfo != null)
            {
                return await Task.Run(() => new GetCustomerByIdResponse() { Result = JsonConvert.DeserializeObject<Customer>(_customerInfo.ToString()) });
            }

            #endregion

            IGetCustomerByIdResponse customerResult = await Task.Run(() => Client.CreateCustomerResponse(request));

            if (customerResult != null && !string.IsNullOrEmpty(customerResult.Result.CustomerId))
            {
                customerId = customerResult.Result.CustomerId;

                IInvitationRequest invitationRequest = new InvitationRequest();
                invitationRequest.CompanyId = companyId;
                invitationRequest.Email = request.Email;
                invitationRequest.FirstName = request.FirstName;
                invitationRequest.LastName = request.LastName;

                // create invitation
                IGetInvitationByIdResponse invitationResult = await Task.Run(() => Client.CreateCustomerInvitationResponse(customerId, invitationRequest));

                if (invitationResult != null && !string.IsNullOrEmpty(invitationResult.Result.Id))
                {
                    invitationId = invitationResult.Result.Id;
                    invitationCode = invitationResult.Result.InvitationCode;

                    // create invitation config
                    ICompanyConfigurationRequest configRequest = new CompanyConfigurationRequest();
                    configRequest.DaysOfData = 60;
                    configRequest.Name = "config1";
                    configRequest.RealDocs = true;
                    configRequest.InvitationId = invitationId;
                    configRequest.VerifiedAssetsRequirement = 10000;
                    configRequest.RulesList = new string[] { "5902e577d5a4ea09f309e6e2", "5902e577d5a4ea09f309e6e4", "5902e577d5a4ea09f309e6e6", "5902e577d5a4ea09f309e6e8", "5902e577d5a4ea09f309e6ea" };


                    IGetCompanyConfigurationByIdResponse configResult = await Task.Run(() => Client.CreateCompanyConfigurationResponse(companyId, configRequest));

                    if (configResult != null && configResult.Result != null && !string.IsNullOrEmpty(configResult.Result.CompanyInviteConfigId))
                    {
                        companyInviteConfigId = configResult.Result.CompanyInviteConfigId;
                    }
                    // Client.CreateCompanyConfigurationResponse(companyId, ,token)

                    // accept invitation
                    invitationResult = await Task.Run(() => Client.AcceptInvitationResponse(customerId, new InvitationAcceptRequest() { Code = invitationCode }));

                    customerResult.Result.InvitationId = invitationId;
                    //if (invitationResult != null && invitationResult.Result != null && !string.IsNullOrEmpty(invitationResult.Result.CustomerId))
                    //{

                    //}
                }
            }

            //IGetCustomerInstitutionResponse bankingInstution = new GetCustomerInstitutionResponse();
            //ICustomerInstitution _i1 = new CustomerInstitution();
            //_i1.Type = InstitutionType.Banking;
            //_i1.InstitutionId = "1111";
            //ICustomerInstitution _i2 = new CustomerInstitution();
            //_i2.Type = InstitutionType.Banking;
            //_i2.InstitutionId = "2222";

            //var listInstitution = new List<ICustomerInstitution>();
            //listInstitution.Add(_i1);
            //listInstitution.Add(_i2);

            //bankingInstution.Result = listInstitution;

            //await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, "banking", listInstitution);


            await DataAttributeClient.SetAttribute(CustomerEntityType, request.Email, "customerInfo", customerResult.Result);

            return customerResult;
        }

        #endregion

        #region Login

        public async Task<IGetLoginResponse> GetLogin(ILoginRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.UserName))
                throw new ArgumentNullException(nameof(request.UserName));
            if (string.IsNullOrEmpty(request.Password))
                throw new ArgumentNullException(nameof(request.Password));

            Client = ProxyFactory.GetProxy(InstitutionType.All);
            return await Task.Run(() => Client.GetLoginResponse(request));
        }

        #endregion

        private async Task<string> GetCurrentUser()
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                var username = token?.Subject;
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserException("Invalid user.");
                return await Task.Run(() => username);
            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("User not authenticated.", ex);
            }
        }

        private async Task<List<string>> GetCustomerIdsBasedOnRole(string customerUserName, string applicationNumber)
        {
            var customers = new List<string>();
            var allBorrowersResponse = await QuestionnaireClient.GetBorrowersAndCoBorrowers(applicationNumber, false);
            var currentBorrower = allBorrowersResponse.FirstOrDefault(b => b.Email == customerUserName);
            if (currentBorrower == null)
                return customers;

            customers.Add(customerUserName);

            switch (currentBorrower.ActualApplicantType)
            {
                case ApplicantType.Borrower:
                    customers.AddRange(allBorrowersResponse.Where(c => c.ActualApplicantType == ApplicantType.Spouse).Select(c => c.Email));
                    break;
                case ApplicantType.Spouse:
                    customers.AddRange(allBorrowersResponse.Where(c => c.ActualApplicantType == ApplicantType.Borrower).Select(c => c.Email));
                    break;
            }

            return customers;
        }

        private async Task<object> ProcessMfaResponse(object response, string customerId, string entityId = null)
        {
            IInstitutionAccount result = (IInstitutionAccount)response;
            if (result.Has_Error)
            {
                return new { Error = result.Error, IsSuccess = false, InstitutionId = result.InstitutionId };
            }

            // If there is any mfa then return that mfa response 
            if (result.Has_Mfa)
            {
                return new { mfa = result.Mfa, IsSuccess = false, InstitutionId = result.InstitutionId };
            }

            if (result.Is_Linked)
            {
                return new { IsSuccess = await AddNewAccount(result, customerId, entityId), SuccessMessage = "Authentication successful.", InstitutionId = result.InstitutionId };
            }

            return false;
        }

        private async Task<bool> AddNewAccount(IInstitutionAccount request, string customerId, string entityId = null)
        {
            var institutionAccounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
            var customerAccounts = new List<ApplicantBankAccountDocument>();
            if (institutionAccounts != null)
            {
                customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(institutionAccounts.ToString());
            }
            // customerAccounts.Where(b => b.IsLinkedInPlaid)
            var existingApplicantAccount = customerAccounts.FirstOrDefault(a => a.IsLinkedInPlaid && a.ApplicantAccount.InstitutionId == request.InstitutionId);

            ApplicantAccount newAccount = new ApplicantAccount();

            foreach (var account in request.Plaid_Link.Accounts)
            {
                account.TotalBalance = account.Balances.Available != null ? account.Balances.Available : account.Balances.Current;
            }

            newAccount.Plaid_Item_Id = request.Plaid_Link.Plaid_Item_Id;
            newAccount.Accounts = request.Plaid_Link.Accounts.ToList();
            newAccount.InstitutionId = request.InstitutionId;
            newAccount.Name = request.Name;
            newAccount.Logo = request.Logo;

            if (existingApplicantAccount == null)
            {
                var customerBankAccountDocument = new ApplicantBankAccountDocument();
                customerBankAccountDocument.ApplicantAccount = newAccount;
                customerBankAccountDocument.IsLinkedInPlaid = true;
                Client = ProxyFactory.GetProxy(InstitutionType.FileThisBanking);
                var fileThisInstitutionId = await Client.GetFileThisIdFromPreApplication(request.InstitutionId);
                if (!string.IsNullOrEmpty(fileThisInstitutionId))
                {
                    customerBankAccountDocument.FileThisInstitutionId = fileThisInstitutionId;
                    customerBankAccountDocument.IsBankAvailableInFileThis = true;
                }
                customerAccounts.Add(customerBankAccountDocument);
            }

            else // Update if applicant account already exists
                existingApplicantAccount.ApplicantAccount.Accounts = newAccount.Accounts;

            await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), customerAccounts, entityId);

            return true;
        }

        private async Task<object> ProcessConnectionResponse(object response, string customerId, InstitutionType typeOfInstitution, string entityId = null)
        {
            try
            {
                IApplicantDocument result = new ApplicantDocument();
                if (response == null)
                {
                    return result;
                }


                var responseString = JsonConvert.SerializeObject(response);
                dynamic connectionResponse = JsonConvert.DeserializeObject<object>(responseString);

                result.ApplicantId = connectionResponse.applicantId;
                result.FileThisAccountId = connectionResponse.accountId;
                result.ConnectionId = connectionResponse.connectionId;
                result.InstitutionId = connectionResponse.institutionId;
                result.Logo = connectionResponse.logo;
                result.Name = connectionResponse.name;
                result.IsLinked = true;
                result.State = STATE_NOT_CONNECTED;
                if (typeOfInstitution == InstitutionType.FileThisBanking)
                {
                    result = await AddNewApplicantBankAccountDocument(result, customerId, typeOfInstitution, entityId);
                }
                else
                {
                    result = await AddNewApplicantDocument(result, customerId, typeOfInstitution, entityId);
                }


                result.SuccessMessage = "Connection created successfully.";
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private async Task<IApplicantDocument> AddNewApplicantDocument(IApplicantDocument request, string customerId, InstitutionType typeOfInstitution, string entityId = null)
        {
            var institutionDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
            var customerDocuments = new List<ApplicantDocument>();
            if (institutionDocuments != null)
            {
                customerDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(institutionDocuments.ToString());
            }

            var existingApplicantAccount = customerDocuments.FirstOrDefault(a => a.InstitutionId == request.InstitutionId);

            ApplicantDocument newDocument = new ApplicantDocument();
            newDocument.ListOfDocuments = new List<IDocumentInfo>();
            newDocument.ApplicantId = request.ApplicantId;
            newDocument.InstitutionId = request.InstitutionId;
            newDocument.Name = request.Name;
            newDocument.Logo = request.Logo;
            newDocument.FileThisAccountId = request.FileThisAccountId;
            newDocument.ConnectionId = request.ConnectionId;

            if (existingApplicantAccount == null)
                customerDocuments.Add(newDocument);
            else // Update if applicant account already exists
                existingApplicantAccount.ListOfDocuments = newDocument.ListOfDocuments;

            await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), customerDocuments, entityId);

            return newDocument;
        }

        private async Task<IApplicantDocument> AddNewApplicantBankAccountDocument(IApplicantDocument request, string customerId, InstitutionType typeOfInstitution, string entityId = null)
        {
            var bankInstitutionType = InstitutionType.Banking;
            var institutionDocuments = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(bankInstitutionType), entityId);
            var customerDocuments = new List<ApplicantBankAccountDocument>();
            if (institutionDocuments != null)
            {
                customerDocuments = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(institutionDocuments.ToString());
            }
            // TODO: add filethis bank document in plaidlinked applicant account document object
            var existingApplicantAccount = customerDocuments.FirstOrDefault(a => (!a.IsLinkedInPlaid && a.ApplicantDocument?.InstitutionId == request.InstitutionId) || a.FileThisInstitutionId == request.InstitutionId);

            ApplicantDocument newDocument = new ApplicantDocument();
            newDocument.ListOfDocuments = new List<IDocumentInfo>();
            newDocument.ApplicantId = request.ApplicantId;
            newDocument.InstitutionId = request.InstitutionId;
            newDocument.Name = request.Name;
            newDocument.Logo = request.Logo;
            newDocument.FileThisAccountId = request.FileThisAccountId;
            newDocument.ConnectionId = request.ConnectionId;


            if (existingApplicantAccount == null)
            {
                var customerBankAccountDocument = new ApplicantBankAccountDocument();
                customerBankAccountDocument.ApplicantDocument = newDocument;
                customerDocuments.Add(customerBankAccountDocument);
            }
            else
            {
                if (existingApplicantAccount.ApplicantDocument == null)
                    existingApplicantAccount.ApplicantDocument = newDocument;
                else // Update if applicant account already exists
                    existingApplicantAccount.ApplicantDocument.ListOfDocuments = newDocument.ListOfDocuments;
            }
            await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(bankInstitutionType), customerDocuments, entityId);

            return newDocument;
        }

        private void EnsureFileConfiguration()
        {
            if (Configuration.MaxFileSizeInMB == 0)
                throw new NotFoundException("Service seems to be not configured for max file size, please check");

            if (Configuration.MaxFilesToRequiredConditionRequest == 0)
                throw new NotFoundException("Service seems to be not configured for no of file to be uploaded, please check");
        }

        private void EnsureIsValidFiles(IList<DocumentDetails> files)
        {
            if (files.Count > Configuration.MaxFilesToRequiredConditionRequest)
                throw new ArgumentException("max no of file upload allowed is " + Configuration.MaxFilesToRequiredConditionRequest, nameof(files));

            var maxfilesize = 1024 * 1024 * Configuration.MaxFileSizeInMB;  // IN MB

            foreach (var file in files)
            {
                var length = file.Content.Length;
                if (length < 0)
                {
                    throw new ArgumentException("file length must be greater then zero.", nameof(files));
                }
                if (length > maxfilesize)
                {
                    throw new ArgumentException("file length must be less then " + Configuration.MaxFileSizeInMB + " MB.", nameof(files));
                }

                var extension = Path.GetExtension(file.FileName);

                if (!CheckExtension(extension))
                    throw new ArgumentException($"{extension} file type is not allowed.Allowed permissible file types are '{Configuration.PermissibleFileTypes}'");
            }
        }

        private bool CheckExtension(string extension)
        {
            extension = extension.Replace(".", "");
            if (Configuration.PermissibleFileTypes == null)
                throw new NotFoundException("Service seems to be not configured for permissible file types, please check");

            var permissibleFileTypes = Configuration.PermissibleFileTypes;
            var permissible = permissibleFileTypes.Split(',');
            if (permissible.Any())
            {
                var permissibleList = permissible.ToList();
                return permissibleList.Any(x => x.ToLower() == extension.ToLower());
            }

            return false;
        }
        #region "Manual"

        public async Task ManualUploadWithMetaData(string customerId, string type, IList<DocumentDetails> files, string metaData, string entityId = null)
        {
            ManualRequest objRequest = null;
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException(nameof(customerId));

            InstitutionType typeOfInstitution = EnsureInstitutionType(type);

            if (string.IsNullOrEmpty(metaData) && files.Count <= 0)
                throw new ArgumentNullException("metaData or files");

            EnsureFileConfiguration();
            EnsureIsValidFiles(files);

            if (!string.IsNullOrEmpty(metaData))
                objRequest = JsonConvert.DeserializeObject<ManualRequest>(metaData);
            switch (typeOfInstitution)
            {
                case InstitutionType.Banking:
                    #region Add account detail of new manual bank
                    // Add account information of manual bank
                    var tags = new string[] { "Assets" };

                    //upload document if any 
                    var accounts = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
                    var customerAccounts = new List<IApplicantBankAccountDocument>();
                    if (accounts != null)
                    {
                        customerAccounts = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(accounts.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                    }

                    //check if already exist account then update and append the uploaded file.
                    IApplicantAccount applicantAccount = null;
                    if (objRequest != null)
                        applicantAccount = customerAccounts.Where(x => x.ApplicantAccount != null).Select(x => x.ApplicantAccount).FirstOrDefault(x => x.Plaid_Item_Id == objRequest.id);

                    if (applicantAccount != null)
                    {
                        if (objRequest != null)
                        {
                            //update the information
                            applicantAccount.Name = objRequest.Institution;
                            Balance balance = new Balance();
                            balance.Available = objRequest.Total;
                            if (applicantAccount.Accounts != null & applicantAccount.Accounts.Count > 0)
                            {
                                applicantAccount.Accounts[0].Mask = objRequest.AccountNo;
                                applicantAccount.Accounts[0].IsManualUpload = true;
                                applicantAccount.Accounts[0].IsLinked = true;
                                applicantAccount.Accounts[0].SubType = objRequest.Type;
                                applicantAccount.Accounts[0].OwnerName = objRequest.Owner;
                                applicantAccount.Accounts[0].Balances = balance;
                                applicantAccount.Accounts[0].TotalBalance = applicantAccount.Accounts[0].Balances.Available;
                                applicantAccount.Accounts[0].IsSubmitted = false;
                            }
                        }

                        if (applicantAccount.Accounts[0].Documents == null && files.Count > 0)
                            applicantAccount.Accounts[0].Documents = new List<IAccountDocument>();

                        string documentMetaData = "{" + string.Format(" \"type\":\"{0}\",  \"institutionId\":\"{1}\", \"institutionType\":\"{2}\" ", objRequest.Type, applicantAccount.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                        var _objMetaData = JsonConvert.DeserializeObject<object>(documentMetaData);

                        foreach (var file in files)
                        {
                            var uploadedDocument = await DocumentManagerClient.Create
                                   (
                                       file.Content,
                                       CustomerEntityType,
                                       customerId,
                                       file.FileName,
                                       _objMetaData,
                                       tags.ToList()
                                   );
                            var accountDocument = new AccountDocument();
                            accountDocument.DocumentInfo = uploadedDocument;
                            accountDocument.AccountNumber = objRequest.AccountNo;
                            accountDocument.Date = TenantTime.Today.Date.ToString(DATE_FORMATE_MMDDYYYY);
                            accountDocument.DocumentId = Guid.NewGuid().ToString();
                            accountDocument.FileId = accountDocument.DocumentInfo.Id;

                            applicantAccount.Accounts[0].Documents.Add(accountDocument);
                        }

                        applicantAccount.Accounts[0].FileId = (applicantAccount.Accounts[0].Documents != null && applicantAccount.Accounts[0].Documents.Count > 0) ? applicantAccount.Accounts[0].Documents[0].DocumentInfo.Id : "";
                    }
                    else
                    {
                        //else new account information
                        Account newAccount = new Account();
                        IApplicantAccount newApplicantAccount = new ApplicantAccount();
                        newApplicantAccount.Plaid_Item_Id = Guid.NewGuid().ToString();
                        newApplicantAccount.InstitutionId = Guid.NewGuid().ToString();
                        if (objRequest != null)
                        {
                            newApplicantAccount.Name = objRequest.Institution;
                            Balance balances = new Balance();
                            balances.Available = objRequest.Total;
                            newApplicantAccount.Accounts = new List<Account>();
                            newAccount.Account_Id = Guid.NewGuid().ToString();
                            newAccount.Mask = objRequest.AccountNo;
                            newAccount.IsManualUpload = true;
                            newAccount.IsLinked = true;
                            newAccount.SubType = objRequest.Type;
                            newAccount.OwnerName = objRequest.Owner;
                            newAccount.Balances = balances;
                            newAccount.TotalBalance = objRequest.Total;
                        }
                        if (files.Count > 0)
                            newAccount.Documents = new List<IAccountDocument>();

                        string documentMetaData = "{" + string.Format(" \"type\":\"{0}\",  \"institutionId\":\"{1}\", \"institutionType\":\"{2}\" ", objRequest.Type, newApplicantAccount.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                        var _objMetaData = JsonConvert.DeserializeObject<object>(documentMetaData);

                        foreach (var file in files)
                        {
                            var uploadedDocument = await DocumentManagerClient.Create
                                   (
                                       file.Content,
                                       CustomerEntityType,
                                       customerId,
                                       file.FileName,
                                       _objMetaData,
                                       tags.ToList()
                                   );
                            newAccount.Documents.Add(new AccountDocument() { DocumentInfo = uploadedDocument, AccountNumber = objRequest.AccountNo, FileId = uploadedDocument.Id, Date = TenantTime.Today.Date.ToString(DATE_FORMATE_MMDDYYYY), DocumentId = Guid.NewGuid().ToString() });
                        }

                        newAccount.FileId = (newAccount.Documents != null && newAccount.Documents.Count > 0) ? newAccount.Documents[0].DocumentInfo.Id : "";

                        //var newDocumentInfo = await DocumentManagerClient.Create(_newDoc, CustomerEntityType, customerId, documentInfo.Type + DefaultFileExtension, _objMetaDta);
                        newApplicantAccount.Accounts.Add(newAccount);

                        IApplicantBankAccountDocument applicationBankAccount = new ApplicantBankAccountDocument();
                        applicationBankAccount.ApplicantAccount = newApplicantAccount;
                        applicationBankAccount.IsLinkedInPlaid = true;
                        applicationBankAccount.IsManualUpload = true;
                        customerAccounts.Add(applicationBankAccount);

                    }

                    await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), customerAccounts, entityId);

                    #endregion
                    break;
                case InstitutionType.FileThisBanking:
                    tags = new string[] { "Assets" };

                    if (!Configuration.UseSimulatorFor[typeOfInstitution])
                    {
                        #region FileThis Manual upload

                        var applicantDocuments = new List<IApplicantBankAccountDocument>();
                        var applicantDocumentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), entityId);
                        if (applicantDocumentInfoList != null)
                        {
                            applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(applicantDocumentInfoList.ToString()).Cast<IApplicantBankAccountDocument>().ToList();
                        }

                        IApplicantDocument applicantDocument = null;
                        if (objRequest != null && applicantDocuments != null)
                        {
                            applicantDocument = applicantDocuments.Where(x => x.ApplicantDocument != null).Select(x => x.ApplicantDocument).FirstOrDefault(a => a.InstitutionId == objRequest.id);
                        }

                        bool isNewApplicantDocument = applicantDocument == null;
                        if (isNewApplicantDocument)
                        {
                            applicantDocument = new ApplicantDocument();
                        }
                        applicantDocument = await AddOrUpdateManualApplicantDocument(customerId, applicantDocument, objRequest, isNewApplicantDocument);

                        applicantDocument.ListOfDocuments[0] = await AppendManualDocuments(customerId, applicantDocument.InstitutionId, applicantDocument.ListOfDocuments[0], typeOfInstitution, files);

                        IApplicantBankAccountDocument applicantBankAccountDocument = new ApplicantBankAccountDocument();
                        if (isNewApplicantDocument)
                        {
                            applicantBankAccountDocument.ApplicantDocument = applicantDocument;
                            applicantBankAccountDocument.IsManualUpload = true;
                            applicantDocuments.Add(applicantBankAccountDocument);
                        }

                        await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(InstitutionType.Banking), applicantDocuments, entityId);

                        #endregion
                    }
                    break;
                case InstitutionType.W2PayStub:
                case InstitutionType.TaxReturn:
                    tags = new string[] { Convert.ToString(typeOfInstitution) };

                    if (!Configuration.UseSimulatorFor[typeOfInstitution])
                    {
                        #region FileThis Manual upload

                        var applicantDocuments = new List<IApplicantDocument>();
                        var applicantDocumentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                        if (applicantDocumentInfoList != null)
                        {
                            applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(applicantDocumentInfoList.ToString()).Cast<IApplicantDocument>().ToList();
                        }

                        IApplicantDocument applicantDocument = null;
                        if (objRequest != null && applicantDocuments != null)
                        {
                            applicantDocument = applicantDocuments.FirstOrDefault(a => a.InstitutionId == objRequest.id);
                        }

                        bool isNewApplicantDocument = applicantDocument == null;
                        if (isNewApplicantDocument)
                        {
                            applicantDocument = new ApplicantDocument();
                        }
                        applicantDocument = await AddOrUpdateManualApplicantDocument(customerId, applicantDocument, objRequest, isNewApplicantDocument);

                        applicantDocument.ListOfDocuments[0] = await AppendManualDocuments(customerId, applicantDocument.InstitutionId, applicantDocument.ListOfDocuments[0], typeOfInstitution, files);

                        if (isNewApplicantDocument)
                            applicantDocuments.Add(applicantDocument);

                        await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), applicantDocuments, entityId);

                        #endregion
                    }
                    else
                    {

                        var customerDocuments = new List<ICustomerDocumentInfo>();

                        var documentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                        if (documentInfoList != null)
                        {
                            customerDocuments = JsonConvert.DeserializeObject<List<CustomerDocumentInfo>>(documentInfoList.ToString()).Cast<ICustomerDocumentInfo>().ToList();
                        }

                        ICustomerDocumentInfo customerDocument = null;
                        if (objRequest != null && customerDocuments != null)
                            customerDocument = customerDocuments.FirstOrDefault(x => x.Id == objRequest.id);

                        if (customerDocument != null) // Append documents
                        {
                            customerDocument.DocumentType = objRequest.Type;
                            customerDocument.Type = typeOfInstitution == InstitutionType.W2PayStub ? DocumentTypePayStub : DocumentTypeTaxReturn; ;
                            customerDocument.IsLinked = true;
                            customerDocument.CustomerId = customerId;
                            customerDocument.IsManualUpload = true;
                            customerDocument.InstitutionName = objRequest.Institution;
                            customerDocument.Name = objRequest.Owner;
                            customerDocument.YearDate = objRequest.MonthYear;
                            customerDocument.Company = objRequest.Company;
                            customerDocument.Total = objRequest.Total;

                            if (customerDocument.Documents == null && files.Count > 0)
                                customerDocument.Documents = new List<IAccountDocument>();

                            foreach (var documentInfo in files)
                            {
                                string docMetaData = "{" + string.Format(" \"type\":\"{0}\", \"institutionId\":\"{1}\", \"institutionType\":\"{2}\" ", objRequest.Type, customerDocument.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                                var _objMetaDta = JsonConvert.DeserializeObject<object>(docMetaData);
                                var newDocumentInfo = await DocumentManagerClient.Create(documentInfo.Content, CustomerEntityType, customerId, documentInfo.FileName, _objMetaDta, tags.ToList());
                                customerDocument.Documents.Add(new AccountDocument() { DocumentInfo = newDocumentInfo });
                            }

                            customerDocument.FileId = (customerDocument.Documents != null && customerDocument.Documents.Count > 0) ? customerDocument.Documents[0].DocumentInfo.Id : "";
                        }
                        else
                        {
                            ICustomerDocumentInfo objDocumentInfo = new CustomerDocumentInfo();
                            objDocumentInfo.Id = Guid.NewGuid().ToString();
                            objDocumentInfo.InstitutionId = Guid.NewGuid().ToString();

                            #region Add institution to customer's instituion list

                            // Add institution to customer
                            var customerInstitutionList = GetFirstAttributeValue(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                            var listOfCustomerInstitutions = new List<CustomerInstitution>();

                            if (customerInstitutionList != null)
                            {
                                listOfCustomerInstitutions = JsonConvert.DeserializeObject<List<CustomerInstitution>>(customerInstitutionList.ToString());
                            }

                            CustomerInstitution newCustomerInstitution = new CustomerInstitution();
                            newCustomerInstitution.CustomerId = customerId;
                            newCustomerInstitution.InstitutionId = objDocumentInfo.InstitutionId;
                            newCustomerInstitution.InstitutionName = objRequest.Institution;

                            // newCustomerInstitution.IsChallengeCompleted = true;
                            newCustomerInstitution.Type = typeOfInstitution;
                            listOfCustomerInstitutions.Add(newCustomerInstitution);

                            await DataAttributeClient.SetAttribute(CustomerEntityType, customerId, Convert.ToString(typeOfInstitution), listOfCustomerInstitutions, entityId);

                            #endregion

                            if (objRequest != null)
                            {
                                objDocumentInfo.DocumentType = objRequest.Type;
                                objDocumentInfo.Type = typeOfInstitution == InstitutionType.W2PayStub ? DocumentTypePayStub : DocumentTypeTaxReturn;
                                objDocumentInfo.IsLinked = true;
                                objDocumentInfo.CustomerId = customerId;
                                objDocumentInfo.IsManualUpload = true;
                                objDocumentInfo.InstitutionName = objRequest.Institution;
                                objDocumentInfo.Name = objRequest.Owner;
                                objDocumentInfo.YearDate = objRequest.MonthYear;
                                objDocumentInfo.Company = objRequest.Company;
                                objDocumentInfo.Total = objRequest.Total;
                            }

                            if (files.Count > 0)
                                objDocumentInfo.Documents = new List<IAccountDocument>();

                            foreach (var documentInfo in files)
                            {
                                string docMetaData = "{" + string.Format(" \"type\":\"{0}\", \"institutionId\":\"{1}\", \"institutionType\":\"{2}\" ", objRequest.Type, objDocumentInfo.InstitutionId, Convert.ToString(typeOfInstitution)) + "}";
                                var _objMetaDta = JsonConvert.DeserializeObject<object>(docMetaData);
                                var newDocumentInfo = await DocumentManagerClient.Create(documentInfo.Content, CustomerEntityType, customerId, documentInfo.FileName, _objMetaDta, tags.ToList());

                                objDocumentInfo.Documents.Add(new AccountDocument() { DocumentInfo = newDocumentInfo });
                            }

                            objDocumentInfo.FileId = (objDocumentInfo.Documents != null && objDocumentInfo.Documents.Count > 0) ? objDocumentInfo.Documents[0].DocumentInfo.Id : "";
                            if (objRequest != null || files.Count > 0)
                                customerDocuments.Add(objDocumentInfo);
                        }

                        await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), customerDocuments, entityId);
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion

        private async Task<object> GenerateExplanationPayload(List<object> customerAccountDocuments, InstitutionType type)
        {
            return await Task.Run(() =>
            {
                var newAccountDocuments = new List<object>();
                switch (type)
                {
                    case InstitutionType.Banking:
                    case InstitutionType.FileThisBanking:
                        var customerAccounts = customerAccountDocuments.Cast<IApplicantBankAccountDocument>().ToList();
                        foreach (var institutionAccount in customerAccounts)
                        {
                            if (institutionAccount.ApplicantAccount != null)
                            {
                                // plaid bank accounts
                                foreach (var account in institutionAccount.ApplicantAccount.Accounts)
                                {
                                    if (account.IsSubmitted)
                                    {
                                        continue;
                                    }
                                    var customerAccount = new
                                    {
                                        institutionName = institutionAccount.ApplicantAccount.Name,
                                        ownerName = account.OwnerName,
                                        type = account.Type,
                                        subType = account.SubType,
                                        accountNo = account.Mask,
                                        availableBalance = account.Balances.Available,
                                        currentBalance = account.Balances.Current,
                                        id = account.Account_Id
                                    };

                                    newAccountDocuments.Add(customerAccount);
                                }
                            }

                            if (institutionAccount.ApplicantDocument != null)
                            {
                                // FileThis bank documents or manually uploaded documents
                                foreach (var applicantDocument in institutionAccount.ApplicantDocument.ListOfDocuments)
                                {
                                    if (applicantDocument.IsSubmitted)
                                    {
                                        continue;
                                    }
                                    var customerDocument = new
                                    {
                                        institutionName = institutionAccount.ApplicantDocument.Name,
                                        ownerName = applicantDocument.Name,
                                        type = applicantDocument.Type,
                                        subType = applicantDocument.AccountSubtype,
                                        accountNo = applicantDocument.AccountNumber,
                                        availableBalance = applicantDocument.Total,
                                        currentBalance = applicantDocument.Total,
                                        id = applicantDocument.DocumentId
                                    };
                                    newAccountDocuments.Add(customerDocument);
                                }
                            }
                        }
                        //return await Task.Run(() => new { accounts = newAccountDocuments });
                        break;
                    case InstitutionType.TaxReturn:
                        if (Configuration.UseSimulatorFor[type])
                        {
                            var taxDocuments = customerAccountDocuments.Cast<ICustomerDocumentInfo>().ToList();
                            foreach (var document in taxDocuments)
                            {
                                if (document.IsSubmitted)
                                {
                                    continue;
                                }
                                var customerDocument = new
                                {
                                    institutionName = document.InstitutionName,
                                    ownerName = document.Name,
                                    type = document.Type,
                                    id = document.Id
                                };
                                newAccountDocuments.Add(customerDocument);
                            }
                        }
                        else
                        {
                            var applicantTaxDocuments = customerAccountDocuments.Cast<IApplicantDocument>().ToList();
                            foreach (var applicantInstitution in applicantTaxDocuments)
                            {
                                foreach (var applicantDocument in applicantInstitution.ListOfDocuments)
                                {
                                    if (applicantDocument.IsSubmitted)
                                    {
                                        continue;
                                    }
                                    var customerDocument = new
                                    {
                                        institutionName = applicantInstitution.Name,
                                        ownerName = applicantDocument.Name,
                                        type = applicantDocument.DocumentSubType,
                                        id = applicantDocument.DocumentId
                                    };
                                    newAccountDocuments.Add(customerDocument);
                                }
                            }
                        }
                        // return await Task.Run(() => new { documents = newAccountDocuments });
                        break;
                    case InstitutionType.W2PayStub:
                        if (Configuration.UseSimulatorFor[type])
                        {
                            var w2Documents = customerAccountDocuments.Cast<ICustomerDocumentInfo>().ToList();
                            foreach (var document in w2Documents)
                            {
                                if (document.IsSubmitted)
                                {
                                    continue;
                                }
                                var customerDocument = new
                                {
                                    institutionName = document.InstitutionName,
                                    ownerName = document.Name,
                                    monthYear = document.YearDate,
                                    company = document.Company,
                                    currentBalance = document.Total,
                                    id = document.Id
                                };
                                newAccountDocuments.Add(customerDocument);
                            }
                        }
                        else
                        {
                            var applicantTaxDocuments = customerAccountDocuments.Cast<IApplicantDocument>().ToList();
                            foreach (var applicantInstitution in applicantTaxDocuments)
                            {
                                foreach (var applicantDocument in applicantInstitution.ListOfDocuments)
                                {
                                    if (applicantDocument.IsSubmitted)
                                    {
                                        continue;
                                    }
                                    var customerDocument = new
                                    {
                                        institutionName = applicantInstitution.Name,
                                        ownerName = applicantDocument.Name,
                                        monthYear = applicantDocument.YearDate,
                                        company = applicantDocument.Company,
                                        currentBalance = applicantDocument.Total,
                                        id = applicantDocument.DocumentId
                                    };
                                    newAccountDocuments.Add(customerDocument);
                                }
                            }
                        }
                        // return Task.Run(() => new { documents = newAccountDocuments });
                        break;
                }

                return new { items = newAccountDocuments };
                // return JsonConvert.SerializeObject(new { accounts = newAccounts });
            });
        }

        public InstitutionType EnsureInstitutionType(string type)
        {
            InstitutionType typeOfInstitution = InstitutionType.Banking;
            if (!Enum.TryParse<InstitutionType>(type, true, out typeOfInstitution) && !string.IsNullOrEmpty(type))
                throw new InvalidArgumentException("Institution Type is not correct.");

            typeOfInstitution = string.IsNullOrEmpty(type) ? InstitutionType.Banking : typeOfInstitution;

            return typeOfInstitution;
        }

        private async Task<bool> GetDocumentsByConnectionId(string customerId, InstitutionType typeOfInstitution, string connectionId, string entityId = null)
        {
            Client = ProxyFactory.GetProxy(typeOfInstitution);
            var response = await Client.GetCustomerDocumentInfoResponse(customerId, typeOfInstitution, connectionId);
            var connectionDocuments = JsonConvert.DeserializeObject<List<ConnectionDocument>>(JsonConvert.SerializeObject(response));

            if (connectionDocuments != null)
            {
                if (typeOfInstitution == InstitutionType.FileThisBanking)
                {
                    var bankInstitutionType = InstitutionType.Banking;
                    var documentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(bankInstitutionType), entityId);
                    List<IApplicantBankAccountDocument> applicantDocuments = new List<IApplicantBankAccountDocument>();
                    if (documentInfoList != null)
                        applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantBankAccountDocument>>(documentInfoList.ToString()).Cast<IApplicantBankAccountDocument>().ToList();

                    var applicantDocument = applicantDocuments.FirstOrDefault(d => d.ApplicantDocument != null && d.ApplicantDocument.ConnectionId == connectionId);
                    if (applicantDocument != null && !applicantDocument.ApplicantDocument.State.Equals(STATE_CONNECTED))
                    {
                        applicantDocument.ApplicantDocument = await AddConnectionDocumentsToApplicantDocument(customerId, applicantDocument.ApplicantDocument, response, typeOfInstitution);
                        applicantDocument.ApplicantDocument.State = STATE_CONNECTED;

                        applicantDocument.IsFileThisBankLinked = applicantDocument.IsLinkedInPlaid ? true : false;

                        await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(bankInstitutionType), applicantDocuments, entityId);
                    }
                }
                else
                {
                    var documentInfoList = GetFirstAttributeValue(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), entityId);
                    List<IApplicantDocument> applicantDocuments = new List<IApplicantDocument>();
                    if (documentInfoList != null)
                        applicantDocuments = JsonConvert.DeserializeObject<List<ApplicantDocument>>(documentInfoList.ToString()).Cast<IApplicantDocument>().ToList();

                    var applicantDocument = applicantDocuments.FirstOrDefault(d => d.ConnectionId == connectionId);
                    if (applicantDocument != null && !applicantDocument.State.Equals(STATE_CONNECTED))
                    {
                        applicantDocument = await AddConnectionDocumentsToApplicantDocument(customerId, applicantDocument, response, typeOfInstitution);
                        applicantDocument.State = STATE_CONNECTED;
                        await DataAttributeClient.SetAttribute(CustomerAccountDocumentEntityType, customerId, Convert.ToString(typeOfInstitution), applicantDocuments, entityId);
                    }
                }
                return true;
            }
            return false;
        }

        private async Task<IApplicantDocument> AddConnectionDocumentsToApplicantDocument(string customerId, IApplicantDocument applicantDocument, object connectionDocumentsResponse, InstitutionType typeOfInstitution)
        {
            var connectionDocuments = JsonConvert.DeserializeObject<List<ConnectionDocument>>(JsonConvert.SerializeObject(connectionDocumentsResponse));

            applicantDocument.ListOfDocuments = new List<IDocumentInfo>();
            foreach (var item in connectionDocuments)
            {
                var applicantDocumentInfo = new DocumentInfo();
                applicantDocumentInfo.DocumentId = item.DocumentId;
                applicantDocumentInfo.FileId = item.DocittDocumentId;
                applicantDocumentInfo.FileName = item.Filename;

                var docittDocumentInfo = await DocumentManagerClient.Get(FileThisEntityType, customerId, item.DocittDocumentId);
                IDocumentMetaData documentMetaData = JsonConvert.DeserializeObject<DocumentMetaData>(docittDocumentInfo?.Metadata.ToString());
                documentMetaData.InstitutionType = Convert.ToString(typeOfInstitution);

                // Set institutionType in metadata in order to filter whilesubmitting data
                docittDocumentInfo = await DocumentManagerClient.SetMetadata(FileThisEntityType, customerId, docittDocumentInfo.Id, documentMetaData);
                applicantDocumentInfo.Documents = new List<IAccountDocument>();
                applicantDocumentInfo.Documents.Add(new AccountDocument()
                {
                    DocumentInfo = docittDocumentInfo,
                    FileId = docittDocumentInfo.Id,
                    AccountNumber = applicantDocumentInfo.AccountNumber,
                    Date = applicantDocumentInfo.YearDate,
                    DocumentId = item.DocumentId
                });

                applicantDocumentInfo.Name = documentMetaData.AccountName;
                if (!string.IsNullOrWhiteSpace(documentMetaData.Date))
                {
                    applicantDocumentInfo.YearDate = Convert.ToDateTime(documentMetaData.Date).ToShortDateString();
                }
                applicantDocumentInfo.AccountType = documentMetaData.AccountType;
                applicantDocumentInfo.AccountSubtype = documentMetaData.AccountSubtype;
                applicantDocumentInfo.DocumentType = documentMetaData.DocumentType;
                applicantDocumentInfo.DocumentSubType = documentMetaData.DocumentSubType;
                applicantDocumentInfo.AccountNumber = documentMetaData.AccountNumber;
                applicantDocument.ListOfDocuments.Add(applicantDocumentInfo);
            }

            return await Task.Run(() => applicantDocument);
        }

        private async Task<IApplicantDocument> AddOrUpdateManualApplicantDocument(string customerId, IApplicantDocument applicantDocument, ManualRequest objRequest, bool isNew)
        {
            if (isNew)
            {
                applicantDocument = new ApplicantDocument();
                applicantDocument.InstitutionId = Guid.NewGuid().ToString();
                applicantDocument.IsManualUpload = true;
                applicantDocument.IsLinked = true;
                applicantDocument.State = STATE_CONNECTED;
                applicantDocument.ApplicantId = customerId;
                applicantDocument.ListOfDocuments = new List<IDocumentInfo>();
            }

            if (objRequest != null)
            {
                applicantDocument.Name = objRequest.Institution;
                IDocumentInfo applicantDocumentInfo = new DocumentInfo();
                if (isNew)
                {
                    applicantDocumentInfo.DocumentId = Guid.NewGuid().ToString();
                    applicantDocumentInfo = await SetManualDocumentMetaData(applicantDocumentInfo, objRequest);
                    applicantDocument.ListOfDocuments.Add(applicantDocumentInfo);
                }
                else
                {
                    applicantDocumentInfo = applicantDocument.ListOfDocuments.FirstOrDefault();
                    applicantDocumentInfo = await SetManualDocumentMetaData(applicantDocumentInfo, objRequest);
                    applicantDocument.ListOfDocuments[0] = applicantDocumentInfo;
                }
            }

            return await Task.Run(() => applicantDocument);
        }

        private async Task<IDocumentInfo> SetManualDocumentMetaData(IDocumentInfo applicantDocumentInfo, ManualRequest objRequest)
        {
            applicantDocumentInfo = applicantDocumentInfo ?? new DocumentInfo();
            applicantDocumentInfo.DocumentSubType = objRequest.Type;
            applicantDocumentInfo.Name = objRequest.Owner;
            applicantDocumentInfo.YearDate = objRequest.MonthYear;
            applicantDocumentInfo.Company = objRequest.Company;
            applicantDocumentInfo.Total = objRequest.Total;
            applicantDocumentInfo.IsManualUpload = true;
            applicantDocumentInfo.IsLinked = true;
            applicantDocumentInfo.AccountNumber = objRequest.AccountNo;
            applicantDocumentInfo.AccountSubtype = objRequest.Type;
            return await Task.Run(() => applicantDocumentInfo);
        }

        private async Task<IDocumentInfo> AppendManualDocuments(string customerId, string institutionId, IDocumentInfo applicantDocumentInfo, InstitutionType typeOfInstitution, IList<DocumentDetails> files)
        {
            applicantDocumentInfo.Documents = applicantDocumentInfo.Documents ?? new List<IAccountDocument>();
            foreach (var documentInfo in files)
            {
                string docMetaData = "{" + string.Format(" \"type\":\"{0}\", \"institutionId\":\"{1}\", \"institutionType\":\"{2}\" ", applicantDocumentInfo.DocumentSubType, institutionId, Convert.ToString(typeOfInstitution)) + "}";
                var _objMetaDta = JsonConvert.DeserializeObject<object>(docMetaData);
                var newDocumentInfo = await DocumentManagerClient.Create(documentInfo.Content, FileThisEntityType, customerId, documentInfo.FileName, _objMetaDta, new string[] { Convert.ToString(typeOfInstitution) }.ToList());
                applicantDocumentInfo.Documents.Add(new AccountDocument()
                {
                    DocumentInfo = newDocumentInfo,
                    FileId = newDocumentInfo.Id,
                    AccountNumber = applicantDocumentInfo.AccountNumber,
                    Date = applicantDocumentInfo.YearDate,
                    DocumentId = Guid.NewGuid().ToString()
                });
            }

            if (applicantDocumentInfo.Documents.Any())
            {
                applicantDocumentInfo.FileId = applicantDocumentInfo.Documents[0].DocumentInfo.Id;
                applicantDocumentInfo.FileName = applicantDocumentInfo.Documents[0].DocumentInfo.FileName;
                applicantDocumentInfo.IsSubmitted = false;
            }

            return applicantDocumentInfo;
        }

        private string GetClientIp()
        {
            const string message = "Cannot determine client IP";
            try
            {
                if (HttpAccessor == null)
                    throw new ArgumentNullException(message);

                if (HttpAccessor.HttpContext == null)
                    throw new ArgumentNullException(message);

                if (HttpAccessor.HttpContext.Request == null)
                    throw new ArgumentNullException(message);

                if (!HttpAccessor.HttpContext.Request.Headers.Any(h => h.Key.ToLower() == "x-client-ip"))
                    throw new ArgumentNullException(message);

                return HttpAccessor.HttpContext.Request.Headers["X-Client-IP"];
            }
            catch (Exception)
            {
                //Logger.Error($"The method GetClientId('{message}') raised an error\n: {ex.Message}");
            }

            return null;
        }

        private List<IBankSummaryResponse> GetBankSummary(List<IApplicantBankAccountDocument> customerAccounts, string customerId)
        {
            List<IBankSummaryResponse> summaryResult = new List<IBankSummaryResponse>();
            foreach (var accountItem in customerAccounts)
            {
                IBankSummaryResponse bankSummary = new BankSummaryResponse();
                bankSummary.FileThisInstitutionId = accountItem.FileThisInstitutionId;
                bankSummary.IsBankAvailableInFileThis = accountItem.IsBankAvailableInFileThis;
                bankSummary.IsFileThisBankLinked = accountItem.IsFileThisBankLinked;
                bankSummary.IsLinkedInPlaid = accountItem.IsLinkedInPlaid;
                bankSummary.IsManualUpload = accountItem.IsManualUpload;

                if (accountItem.ApplicantAccount != null) // This means it has plaid bank response
                {
                    bankSummary.Accounts = new List<Account>();
                    bankSummary.ApplicantId = customerId;
                    bankSummary.InstitutionId = accountItem.ApplicantAccount.InstitutionId;
                    bankSummary.Logo = accountItem.ApplicantAccount.Logo;
                    bankSummary.Name = accountItem.ApplicantAccount.Name;
                    bankSummary.Plaid_Item_Id = accountItem.ApplicantAccount.Plaid_Item_Id;

                    if (accountItem.ApplicantDocument != null)
                    {
                        bankSummary.State = accountItem.ApplicantDocument.State;
                        bankSummary.ConnectionId = accountItem.ApplicantDocument.ConnectionId;
                        bankSummary.FileThisAccountId = accountItem.ApplicantDocument.FileThisAccountId;
                    }

                    foreach (var bankAccount in accountItem.ApplicantAccount.Accounts)
                    {
                        bankSummary.Accounts.Add(bankAccount);
                    }

                    if (!accountItem.IsManualUpload && accountItem.ApplicantDocument != null) // Only if bank linked in assets is also linked on fileThis
                    {
                        bankSummary.Accounts = AddFileThisLinkedDocumentsIntoEachBankAccount(accountItem.ApplicantDocument, bankSummary.Accounts);
                    }
                }
                else if (accountItem.ApplicantDocument != null) // This means it has filethis linked documents
                {
                    bankSummary.Accounts = AddFileThisLinkedDocumentsIntoEachBankAccount(accountItem.ApplicantDocument);
                    bankSummary.ApplicantId = accountItem.ApplicantDocument.ApplicantId;
                    bankSummary.InstitutionId = accountItem.ApplicantDocument.InstitutionId;

                    bankSummary.Logo = accountItem.ApplicantDocument.Logo;
                    bankSummary.Name = accountItem.ApplicantDocument.Name;
                    bankSummary.State = accountItem.ApplicantDocument.State;
                    bankSummary.ConnectionId = accountItem.ApplicantDocument.ConnectionId;
                    bankSummary.FileThisAccountId = accountItem.ApplicantDocument.FileThisAccountId;
                }
                summaryResult.Add(bankSummary);
            }

            return summaryResult;
        }

        private List<Account> AddFileThisLinkedDocumentsIntoEachBankAccount(IApplicantDocument applicantDocument, List<Account> bankAccounts = null)
        {
            var processedBankAccounts = bankAccounts ?? new List<Account>();
            if (bankAccounts == null)
            {
                // add single account to add all filethis documents
                processedBankAccounts.Add(new Account());
                if (applicantDocument.IsManualUpload && applicantDocument.ListOfDocuments.Any())
                {
                    var accountToUpdate = processedBankAccounts.FirstOrDefault();
                    var documentInfoToCopy = applicantDocument.ListOfDocuments.FirstOrDefault();
                    accountToUpdate.Account_Id = documentInfoToCopy.DocumentId;
                    accountToUpdate.IsManualUpload = true;
                    accountToUpdate.OwnerName = documentInfoToCopy.Name;
                    accountToUpdate.Mask = documentInfoToCopy.AccountNumber;
                    accountToUpdate.SubType = documentInfoToCopy.AccountSubtype;
                    Balance balance = new Balance();
                    accountToUpdate.TotalBalance = balance.Available = documentInfoToCopy.Total;
                    accountToUpdate.Balances = balance;
                }
            }

            // Add filethis documents to each account
            foreach (var bankAccount in processedBankAccounts)
            {
                bankAccount.Documents = bankAccount.Documents ?? new List<IAccountDocument>();
                foreach (var accountDoc in applicantDocument.ListOfDocuments)
                {
                    foreach (var documentFile in accountDoc.Documents)
                    {
                        var accountDocument = new AccountDocument();
                        accountDocument.AccountNumber = documentFile.AccountNumber ?? accountDoc.AccountNumber;
                        accountDocument.Date = documentFile.Date ?? TenantTime.Today.Date.ToString(DATE_FORMATE_MMDDYYYY);
                        accountDocument.DocumentId = documentFile.DocumentId;
                        accountDocument.FileId = documentFile.FileId;

                        bankAccount.Documents.Add(accountDocument);
                    }
                }
            }

            return processedBankAccounts;
        }

        /// <summary>
        /// EnsureDeleteEntityType
        /// </summary>
        /// <param name="entityType">entityType</param>
        /// <returns></returns>
        public DeleteEntityType EnsureDeleteEntityType(string entityType)
        {
            DeleteEntityType typeEntity = DeleteEntityType.account;
            if (!Enum.TryParse<DeleteEntityType>(entityType, true, out typeEntity) && !string.IsNullOrEmpty(entityType))
                throw new InvalidArgumentException("entityType is not correct.");
            typeEntity = string.IsNullOrEmpty(entityType) ? DeleteEntityType.account : typeEntity;
            return typeEntity;
        }

        public object GetFirstAttributeValue(string entityType, string entityId, string name, string secondaryName = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(secondaryName))
                {
                    var result = (JArray)DataAttributeClient.GetAttribute(entityType, entityId, name, secondaryName).Result;
                    if (result != null)
                    {
                        return result.First();
                    }
                    else
                    {
                        return null;
                    }                    
                }
                else
                {
                    return DataAttributeClient.GetFirstAttributeValue(entityType, entityId, name).Result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

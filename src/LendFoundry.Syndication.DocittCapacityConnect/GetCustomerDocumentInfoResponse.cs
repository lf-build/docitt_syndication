﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerDocumentInfoResponse : IGetCustomerDocumentInfoResponse
    {
        public IEnumerable<ICustomerDocumentInfo> Result { get; set; }
    }
}

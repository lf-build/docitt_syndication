﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerAccountTransactionsResponse : IGetCustomerAccountTransactionsResponse
    {
        public string InstitutionName { get; set; }
        public string AccountType { get; set; }
        public string AccountSubType { get; set; }
        public string AccountMask { get; set; }
        public string CurrentBalance { get; set; }
        public int TransactionPeriodInDays { get; set; }
        public string TransactionStartDate { get; set; }
        public string TransactionEndDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAccountTransaction, AccountTransaction>))]
        public List<IAccountTransaction> Transactions { get; set; }
    }
}

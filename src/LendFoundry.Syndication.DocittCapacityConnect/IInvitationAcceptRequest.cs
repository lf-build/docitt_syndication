﻿
namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IInvitationAcceptRequest
    {
        string Code { get; set; }
    }
}

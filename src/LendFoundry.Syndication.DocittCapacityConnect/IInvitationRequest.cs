﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IInvitationRequest
    {

        string CompanyDisplayName { get; set; }
        string CompanyId { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string NotificationType { get; set; }
        string PhoneNumber { get; set; }
        string Template { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ICustomerInstitutionRequest
    {
        string CustomerId { get; set; }
        string InvitationId { get; set; }
        string InstitutionId { get; set; }
        string Secert { get; set; }
        string UserName { get; set; }
    }

}

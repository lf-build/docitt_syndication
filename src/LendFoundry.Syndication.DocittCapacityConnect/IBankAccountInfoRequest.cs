﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IBankAccountInfoRequest
    {
        string BankName { get; set; }
        string AccountType { get; set; }
        string AccountNumber { get; set; }
        double CurrentBalance { get; set; }
      //  string BankInfoId { get; set; }
        string AccountHolderName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class FileThisMfaReplyRequest : IFileThisMfaReplyRequest
    {
        public string AccountId { get; set; }
        public string ConnectionId { get; set; }
        public string InteractionId { get; set; }
        public Dictionary<string, string> Inputs { get; set; }
    }
}

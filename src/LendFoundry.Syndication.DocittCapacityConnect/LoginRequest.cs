﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class LoginRequest : ILoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

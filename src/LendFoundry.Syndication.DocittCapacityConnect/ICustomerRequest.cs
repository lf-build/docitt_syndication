﻿
namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ICustomerRequest
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        string Ssn { get; set; }
        int MonthlyGrossIncome { get; set; }
        int SalesPrice { get; set; }

        string UserName { get; set; }
        string Password { get; set; }
        string UserType { get; set; }
    }

}

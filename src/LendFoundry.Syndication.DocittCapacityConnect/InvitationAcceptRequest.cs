﻿
namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class InvitationAcceptRequest : IInvitationAcceptRequest
    {
        public string Code { get; set; }
    }

}

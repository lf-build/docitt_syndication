﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class ConnectionStateRequest : IConnectionStateRequest
    {
        public string AccountId { get; set; }
        public string ConnectionId { get; set; }
    }
}

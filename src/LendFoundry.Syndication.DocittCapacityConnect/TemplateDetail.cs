﻿using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class TemplateDetail : ITemplateDetail
    {
        public string TemplateName { get; set; }
        public string Version { get; set; }
        public Format TemplateFormat { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ICompanyRequest
    {
        string Name { get; set; }
        string[] RequestedScopes { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string UserType { get; set; }
    }
}

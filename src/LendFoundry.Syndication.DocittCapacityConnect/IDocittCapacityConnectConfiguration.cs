﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using LendFoundry.Foundation.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IDocittCapacityConnectConfiguration : IDependencyConfiguration
    {

        string Certificate { get; set; }
        string CertificatePassword { get; set; }
        string ServiceUrl { get; set; }
        string ProxyUrl { get; set; }
        string Ticket { get; set; }

        string CompanyId { get; set; }
        string[] DefaultInstitutions { get; set; }
        string[] DefaultTaxInstitutions { get; set; }
        string[] DefaultW2Institutions { get; set; }

        List<Institution> FavouriteInstitutions { get; set; }

        string CompanyUserName { get; set; }
        string CompanyPassword { get; set; }

        bool IsSimulatorOn { get; set; }
        Dictionary<InstitutionType, bool> UseSimulatorFor { get; set; }
        int MaxFilesToRequiredConditionRequest { get; set; }

        string DefaultProviderId { get; set; }
        string DaefaultChallengeAnswer { get; set; }
        string SampleBankDetailResponse { get; set; }
        string SampleBankAccountResponse { get; set; }
        string SampleBankAccountSyncResponse { get; set; }
        string SampleMfaSelectionResponse { get; set; }
        string SampleInvalidCredentialResponse { get; set; }
        string SampleInvalidMfaResponse { get; set; }
        string SampleTaxW2DocumentResponse { get; set; }
        string[] AcceptedBankPasswords { get; set; }
        int NoOfYearsW2Docments { get; set; }
        int NoOfPayStubDocments { get; set; }
        int MaxFileSizeInMB { get; set; }
        string PermissibleFileTypes { get; set; }
        Dictionary<InstitutionType, TemplateDetail> WriteExplanationTemplates { get; set; }
        int BankTransactionPeriodInDays { get; set; }
        int BankTransactionCountPerPage { get; set; }
    }
}

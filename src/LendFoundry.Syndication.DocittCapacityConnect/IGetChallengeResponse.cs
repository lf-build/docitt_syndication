﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetChallengeResponse
    {
        IEnumerable<IChallenge> Result { get; set; }
    }
}

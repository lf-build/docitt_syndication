﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetCustomerAccountResponse : IGetCustomerAccountResponse
    {
        public IEnumerable<object> Result { get; set; }
    }
}

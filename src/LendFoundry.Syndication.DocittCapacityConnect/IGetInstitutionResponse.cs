﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetInstitutionResponse
    {
        IEnumerable<IInstitution> Result { get; set; }
    }
}

﻿using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface ITemplateDetail
    {
        string TemplateName { get; set; }
        string Version { get; set; }
        Format TemplateFormat { get; set; }
    }
}

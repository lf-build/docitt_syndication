﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetCustomerAccountTransactionsResponse
    {
        string InstitutionName { get; set; }
        string AccountType { get; set; }
        string AccountSubType { get; set; }
        string AccountMask { get; set; }
        string CurrentBalance { get; set; }
        int TransactionPeriodInDays { get; set; }
        string TransactionStartDate { get; set; }
        string TransactionEndDate { get; set; }
        List<IAccountTransaction> Transactions { get; set; }
    }
}

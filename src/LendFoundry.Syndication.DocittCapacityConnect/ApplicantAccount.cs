﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class ApplicantAccount : IApplicantAccount
    {
        public string Plaid_Item_Id { get; set; }
        public string InstitutionId { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public List<Account> Accounts { get; set; }        

        public string ApplicantId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class LargeDepositTransactionRequest : ILargeDepositTransactionRequest
    {
        [JsonProperty("income")]
        public string Income { get; set; }

        [JsonProperty("purchase_price")]
        public string PurchasePrice { get; set; }
    }
}

﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;


namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public class GetInvitationByIdResponse : IGetInvitationByIdResponse
    {
        public IInvitation Result { get; set; }
    }
}

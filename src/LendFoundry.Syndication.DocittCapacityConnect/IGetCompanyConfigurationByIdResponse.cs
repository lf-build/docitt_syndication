﻿using LendFoundry.Syndication.DocittCapacityConnect.DocittCapacityConnectResponse;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IGetCompanyConfigurationByIdResponse
    {
        ICompanyConfiguration Result { get; set; }
    }
}

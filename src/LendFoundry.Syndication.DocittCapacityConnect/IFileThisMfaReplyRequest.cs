﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.DocittCapacityConnect
{
    public interface IFileThisMfaReplyRequest
    {
        string AccountId { get; set; }
        string ConnectionId { get; set; }
        string InteractionId { get; set; }
        Dictionary<string, string> Inputs { get; set; }
    }
}

﻿// using LendFoundry.Syndication.FileThis.Configuration;
using LendFoundry.Syndication.FileThis.Proxy;
using Newtonsoft.Json;
using System;
using Xunit;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.FileThis.Tests
{
    public class FileThisServiceTests
    {
        private readonly IFileThisConfiguration Configuration;
        private readonly IFileThisConfiguration BrokenConfiguration;
        private readonly IFileThisProxy Client;
        private readonly IFileThisProxy BrokenClient;

        public int _accountId = 112356; //112136;
        public int _connectionId = 320062; // 373432; //373157;
        public string _partnerAccountId;
        public int _partnerId = 28;
        public int _documentId = 13383747; // 13360142;

        public int _subscriberId = 138; // 13360142;
        public int _subscriptionId = 473; // 13360142;
        public int _interactionId = 1888073;
        public string _serviceId = "533607";


        public FileThisServiceTests()
        {
            Configuration = new FileThisConfiguration()
            {
                ServiceUrl = "https://filethis.com:443/api/v1/",
                ProxyUrl = "", // "http://192.168.1.9:5049",                
                Ticket = "4pm0zueKPGqV97eQX30pWe0OaSh"
            };

            Client = new FileThisProxy(Configuration);

            BrokenConfiguration = new FileThisConfiguration()
            {
                ServiceUrl = "https://filethis.com:443/api/v1/",
                Certificate =
                    @"MIIxRAIBAzCCMQQGCSqGSIb3DQEHAaCCMPUEgjDxMIIw7TCCBf4GCSqGSIb3DQEHAaCCBe8EggXrMIIF5zCCBeMGCyqGSIb3DQEMCgECoIIE/jCCBPowHAYKKoZIhvcNAQwBAzAOBAhtJEzjxWHxnAICB9AEggTYsaoJeNDDOaytFBodYFQ+PzQpYFFrzIk9Wi2EmzCqzp+ow0d3VVWvUiYxyd5T7ph7Ff48F34R9B05XGthLQOd5R0loI5yB9TUHHI4Q/1LEP6kzCLrItFf+1NzaakY8v+qhmW/qCz83teI3b1Lp9SKb2XmK3FFH6JltYS8GhzBiQm2YPyPfdvIV3I3Ordi4mROUQibLhJiHKaEd5yUalqGgTl5EdipMA61j3eLNMJBQZEX+lT6PqPfd4SvcNK57Qva2V9wY8wvGqJlGouueZOjLfTL5lFDRGYWg1ALRwi1LflAtfvvyJ12bMbQc9k01iQ+8UVz+MleFrDQ/byTRNNBspjylVYjHghThoVRBLKt1dZtFo6A/Pby26a34of2m3ElPS++eUlJXE2okr4vkAxLPKnJtL5c8Mb2pXDeWlKSNvMav/V2EFkPrE1IuAYZ6EyYOaIl6e3++lO/EAKQAJSrdWm9YuBuEww8nhhmy8n5mc88Q2vPbewlvuKBdMIsR01UC3UTIHz2h0Ji/7zVSAxVon5dZL/VWzAL65CgmgvELogafSDEP56C0DvO336veX7+QqACLo3J/3xCSlUyxbmgNU9D6kGpPGlNIn9A93DuhW03juvCYLdkPmNZ4A31a7vps0i8u1127KgpyRfXpkRamqA4LRH7afVcGmJId0hXK+SrLM7PkNhrFtsw2z+pfZLe4DL9JsmC5C6Kw3IlvzFyJHeyYcMCNaM7gJ51qzxdIKqNZuV4xkitPLBJiTmAReny1rzHIhulmOv6rPu5ASU2wjJVJSg+87DGLl3tfVmpIzFvkGsk6RmV8mvUsYLGmQ5opNvlSc6MNWrbWOGzG+r2KPaDtvvBuMXFqBDfBS4xu8Dd4/HVnJh2HXpHGeR6yb6YSur4rhZZdgM/JwlGGKCqntnVAxSAD+vFndRliSh6dNycJ3DOn20ARTkiXeTzS4+IlkuclcbTxNXNiEiQ3vOrQz1IbICEqG1GuAY1NX1VpfkGwyHg4rPT1gEr+UQ4Xqmr23kw67igkZZgtxLH3g1kWWBSjZVuXWYr+fHeztSyQdG5zh/l52Oq4P5+NGZmYGMszh8Gao14Xsbx5I9q66DzSYaMgbWfcOIbBY/Skmsw+taOefK3Y2LVMHt7f9MuCDXPc0j73Gil3unJRrVQMOpyJo7KOkXlQ0yaWSIIL6HRQCONtXcssqIQZDPQsk6fR6a1BvGyM7Eiz/z3Zz2Zk3CbadDtaO0ZTbq0pysC0O7zRABG4j/N3i4RZdqwAZAr8I+Yh7EGHyZ4zCdHR6pcyQ2ngYQm0hGod7kzFzZQ5iOnPNNgi1NNS4p5CCT5yGfheCJeYWyJF8Fl/4vy6/W5Vx5ZSYhEwZTtahAT5hxcAXvaFxla0qP93XNQZbWOg1N4C7QszR22YwwG0p4Rwg1ogZjB4Ph+fworqwRo3DgKg61ux+w8R17naaRQhvunaRi9savg6G3Z/cNmnSKc6WGltjGziwAGwQc+9QHlRUgwmHJjAw07aHlghNPxUlbkW6tX0zuzXS4t6cwHjY9JgXMFji8ZJGMtfINe3jojKwTo/nMgE51sSsr/Ry4GLMlLnt4kmrhhisyFRS9eEuObuIqxVsMk/ykR5bL0jidolJMjQqsL3h2zQNORtO/46jGB0TATBgkqhkiG9w0BCRUxBgQEAQAAADBbBgkqhkiG9w0BCRQxTh5MAHsANgBCADAAQgBBADQARgBFAC0AMAAxAEIAMAAtADQAQgBDADAALQBBADIARAA3AC0AMAAwADUANgAxAEMANgAyADkANQA3ADUAfTBdBgkrBgEEAYI3EQExUB5OAE0AaQBjAHIAbwBzAG8AZgB0ACAAUwB0AHIAbwBuAGcAIABDAHIAeQBwAHQAbwBnAHIAYQBwAGgAaQBjACAAUAByAG8AdgBpAGQAZQByMIIq5wYJKoZIhvcNAQcGoIIq2DCCKtQCAQAwgirNBgkqhkiG9w0BBwEwHAYKKoZIhvcNAQwBBjAOBAgWvBrcIvJDLwICB9CAgiqgBVzX3XzNqFktyg6ZZATBiM2phqnxR8trzcI+n4adSMR+r+O6jiWW+VLs9piwHs3ko3Bzcf7RIXpzLBawbUV9NE65uumkBxDRryCtFr6Vn81ddupqqy3ZgD7TY655Q9hLXWCoG208oiWrMsxa3TtAc/OFkfP/w+c5uk++70/gKvTUy0zEJ+nt48u6riDjqjY4J0/0Bq980+Ub2MZQ8ZloEebEW1n978NH0QZW2XGomfNuJyE3dqUItQYX/3EQyBrJ2Q2cGP0r3eR7A4DxXx/S7mNEZ+Q1GX1eehMZUbV049H6l9tOBL3xZy7Q92uuhrGOksRlLBofYA3HWdSNjwtxAxdbJEB3Sqw6vP96xpDTSt2X37pA1DN5UzSJcSGVaHp1Rlj2P+xyZ4JqnVkov5DCV1WVa4gkzoujNwfOztbOY1eB4b7ggVYKPd5tDbKwIGCrWdPnPnM1QvIY0lL1M8VVPCVaESzKSlT2LZqyvAvBgCa7AFCvzk/c29Dmm1f8TKGbizT+FkYtvb4Zpt5wYArSlqI9a1ekM5/t6DY6U9jUVrZZuOpVXUBHlk84gTS3zcEIrd8hlb7EGBAB09xIruLv5a2DMIN34Jac7k3PEKBZRMShFN23HAmctwUIkVKjZOgfPjvvqloRjHYjTGKGSXWlOwAepn7Gj08FCYSRoetcelop/1zhK2Szn0WKz1uGUEnQYeA8M3cJDPnRRXojkEmP1KpALkQ33Mmak1cxhDHBrHAF3YHCgyB2Nb2pCxxbcDzAMd/SfDwOSVBRJDsYzXXw77lPlTlCFImgUQ4+IJFy8tFFZKEybAdHGCicHE5lShho9xH+n8rWC2UYekJ1vn+Sfqlixr7oS02ihtLq8fKKVyBw2R58f/ERm+LvlIhCu5FphRAcHeaW5HEaC+5vc8UVYg60Mj/4jjdF54eIzYnPQu45OylU5y/cuODp7ZAsyYXIhK7qXSGljK+lOg8XNyRQZDeJrsbjTXRC0vRNX/x1Du5hPGsick8e7AnScxbPtRvR+Sls3km+0LxtChFIHkXzoeDnmqv0sMR4sII+dt7fzXMvhg15RO0yRQmpsJXl3dWvOpZ1Rjn9Lo1S0kNNlN8hBklBkViYqqjy1fLRjDTbPat6+iH2NPPeacuslRAxuNXgB5f0CoHZH20q7qOmiQHn7G8TN2NWv5vsStPOyB7gE8UTdgAt/NbWyLhj0NijZtf2vUzi5l7iob+143QZiCoKW+c8q/dIMMqT8s19eIpARqQlDMB6oEl4ABJWQv+0PL/+5ZOMluuoNjbPYBlQQe2MCCwnPNDU2jKjQ2F6mhF1NOvySvsQYTqDCzJXw9stS1KIUrADcXzs99+hmPt8or8l5tg1qJivEYVN6yyKcnktL6uBtLS/1l1K4R2ZoN7+Ld5BpuwGuUV+d4pxaevsqIWRRwscQUasE89NIeTTMaFwAI+RdaeP71i4O5AgibjuLxpovYZhZafBaZVPF8y/7O2v5gnhH1fbLmb6bVluvag6SL2iM5t25muQhsVe5Qm8kw5E+5zzasRegL+7zao/XeVFmj7RcfYOUr+9vOIGzfcBaBfxYlzzumPZUbOD06XDMH+oiMy2yR6Gt7k3KxkgNVAHdEkewRfKB3HrsivZuki+0ckixY1pficUTZvYUmuW4hXrkxErR2Mf3oN+7w2COjG8MPMVKn8TZNZsQljGL8rqAvqMLewBY3lVcV3UfEEYnpCvlORADd2jX4N1P+K3s/MVkP1Nw0gEEIdO8mt1KLBR0KwQuV3oOVSBtqyQ+CnyZGyBS9VRxFQEwEB+NkPEoLpJjNyGJ5j93haWKRe9FcWiTQT65kAyzedBDuJHSwAFPidH8efNAbo3MiHXT5ro8rjZEQ6OkpkcjO8mM2zctAF2/naJEK8/Wh4tZCi7bOroIYwere/8NxtBf5FE3xN2kPiBM6tcry3NmSbnYaHsXYePdvBHdd/Uw0C1mbz1uah52Y+EGu4S1F02qjTKFldyyTVxaom7nNlbG1ulDKX8xulioY+j7Fyoigvctauw51AWtI36Zn7SIXETgWTJHt5Pm/V6E6M7jkYjt/cOv9pOCTkNsVcJFabkDZo62JCCUb7KLaRyGcfaQevvq4egz7wcLNUV5qe7CpXsY4mmuV/ibomVoaHgts2TjarvWz7bmMhztCBn3vM07XEWVMFoqyL3Jzv0OFM0GcHghM8feVYYXRiSOzdQq0ok+nQ7aO29fZL0Mv7j3uhbuSruVG5GhPdLUEJugGOQOtS83Efh2Msqduer5rwDewJJr9ywb8oOnqVNS/g0TDwiYyXsOliq3UHnSNi2mP3aL4jlUS68T9CLzW/tFIbSU4cF2cBeUSWjGzMNHIhjml0RQVWsDY/LOjphBEClr4qdJS9EqzJ4rmy09wxmCKINU8bWvikmtX9J8PM3w2w5bEhvJhtUy1mX5dpq+fJi3DwiVLWZlMi6asW7n+lEYHVO/S95cMAA/hl7u3X+yYNQnWLCD+JJgA2jbWz31GNHnlCBb/JcWk8UfaaHK/O5FlteUB+6ievytCi/rYMrwDUhYrtC6RtTGG1WwIBsjL9MUkMc4mXlutmU9usStjk5g9OMgrwKVN+BDgDxfP6esQBFMj4W/WW8SlFztPczCuJPuwCc2K1pm+p2jQ2S++mzkzkRMA6YSJH9BCyQt+6gR3mRhCKjHJY651kZMvrGeH7vMd7TTR5RNNz58SCuxOe8D663BqAvTdHQhRL+3eaxEoQQNrUWOikzqEvZy5w1VhwBB9kCaHFi6XRM3SoLbaD8kVuMPmwm/70thZRXnRAaRquKsO/5i89OIJ7zi2CQdlmzrKsEuA132Nwz6IJOAL+VefmxtzPi2ssRwqupjRIq1Y51a1Tp+k1mLTK/9L1gzdsWFQ4C3rVw8/1pdu3/ckcI6OZLWodaK5G6iLM23R9gRu2TlHQlw3icj4wwGdA8Ph/mVgnMNTBOfgBRj6bFvd5HMa243dsu9Cuxdv/Q1zFT0tQYX0C1ssKqQVrL8d6MdP0ZNE9KxpRfksQ8zV6hOrDF+0YTm4rM3sM97LiJ7WQthh6GPAafvbPyKS//f4aBcPjwhVIaEJYr8XrZ3xkk9ikZ2SVvdOwrE1c+9IfiMmjeQ5FEx+hxxmLyuIyTu9J7e0FCPObH3Zt1TUIlAIgDEIEXAKlCI/wXGIPm2c1a/uNHHmOWnQ5ngLmJFtmHhlhYdEkS4WZdhkcOitRw8dSCEL3GmMQpSH79m79CgG50UXi8L+kdXihaUkV8xbXkZkdjUBfuWJxEAAvjAT0VDW/K2N1WqM5S/L2AKP6Lg+9oN5SgmlIJ1kxojUR1jjnqkfSps0leDeFvo7RfCD0Ii/x4NDO7FkVcWIRYzzIYiyeeAEu4f0ONY/PHTAQJ80gd8jZoRznem9LZUQ1vRHG42q5pa8LqzQEMsCQZapGNGYyfqt1GuWS3dyYUi11Ovq9mC2ISFRetGDmRXnlc6HwR1XQjvrptU3YH6d2KhxefM7NP4Khrarj3+ZsSxp7ZW4Q44Gf0SgnAbCxLmJs9kblnf0EJxI2H7rv8hL4ZhUCNi7+zFYwRt8aOdMHTxsLLfDpscWoUIAgzgNSvWFZo0WRpgyM8fThnsJ9ey3PlA6N+ay0wP/YiaJb3Y1s/1nDHvLLe3ONsy6uruGrorpddXY+iTq25Ha332nItZhPesYAytXnWssOEs7n+LllqJronzeS2I5zMyPPaNmqiTHd3XEis0fQ3P14KKmYiibcx10P7KbOyRikTyrNTgvwFr1C+y3WJoPaYUv10HCe+52sOpCeKhtzfcw+f5nA6On6U++ofQmEDCFCU1/R8Gl3jEpEZkThWu1hq6QwgnDohugkUg+PXuQMwfHNa7TROWGNiJEcHQZu6vMtpgLK1uXIv7JA9yFNhvdXheyd5o1i3ifmzKR52vNPXFH5+b4TIH1gmYYrONP4UDJfCQP5EzqEkLNbiFqdtg0/r1swv6E8COY15C2EyrtVC1ovMP6e/LtnrmBonwwalTd/EM6/ALGeytGwiAnzDcjGYyMqfT1HFDPBjjU8N8RvcyqFJ8wN+fh6LD4RllSStnN2fOdi2GbsSSUgJ4rU+KBE0e+XMSCyrItB9vnGdYA892cVw3ftR4f1VUrTjcb0Hg7lHpXC35h8kGSUc9OlHbwVMwq7pw3vmhtGsZvN/msHdOQKUfUwZGRj7GBKMAJzzKazO4SxC0SjTM1jvWGZTnvA83PBMRTIn17gtdlP2G7hGdpQTxUu7flCu63HkfCCMlJoWRkH9ytksNc0RZ7LHoVuKmBmvKuMR3raiLVZndqTlad0Az09wprwsrfCR+H0NhFlxx9sws1O5uEUmjVA/AawDJaRSuuy+3WITnfMDluGCr4i+bJNVXI0dEU+m5LBv+kazemXKZ4u/fHZnRV5CvNA7nAlwWo1Bt5sa78IcjxqNaHNp9lJD6Nn7KM6sFuyrp7hJC29xoMSYIHZAv/DLUfeklI9psHl+5P7tLMvleT2Ejv4CYEhO5ioFwstoFc3FXyVuzCFqaHWyxRk+oAm7+jfe2HRQ8O05WxzZssf1t87/EXki3BoeVHXX410VfkE6rURSSj1ZEllRkvbb7jtrFOUAw8CWQD05FGz5bjQsBbRn7IRKqczzbKWWrqucX/JOfyEgfdRDdkX/d7RbCEmaxZKfRAVi5D/gQiPlciqEgIwUju7GE7pzRNmvopteaJg29Ga8+zZYQb5o3JzBH9xDgCLKwzBmq19Jhp/qBeFG1Ob2ygYZH7VPm062Zp0yuIk7swyT8YvvIRzRxw6D7szMG+mfQ91puhhfWdU3Dc7hOINmdJt9XPbF0R4bNOlIJ8HhSBZINwijBtbI6mqhavmLLFD0KJ8tKPnT2v0p8p+BQh8XukbA8eWSBKs/UoLD6sl/Toaised8cfNA3tAZVEtce3ueTrD3Oq0leq2qytPjeCvsqozDtQG8j/qhq6PmfenYk2HxImJOlGZMqM4bxduno9BWDXpTwAaQUi3j6b9Hgge3ma5ALePDH2wklTa2vJRpNkV9Ed/N1Q+mcYPLHCjwJqSlOfYrR8MURMM0HWw5HR+Bpp7yuLms2o94JXhKPmt1tHk+C3Atd2wKA5xa9/Q2Q9GytZPUEGznIqBdGbOadvYac+Rf1wjIEgNOba1MSNPMHrS3t4R5/Zj2bvTZeeOl7m3HOaaU0mW2R4lxVRWgKaCBjEDcPlxNBttpzFCHb5pb5XnCMGIGKOLE8Mpw9BYju+kRdTPDslhJIpZ8HoWkYZFCIIeu6vPAR6/u5hMNmcVwSpQVrFecHbztqsguxOpydYy9AgT71FMQofPxtlEQ+Ad8/IO+O4DlzqzGJb7P0UuJjcijUSFvHgzi3ApzIXO0ncyspDZVYLtB1Mtixk8JcULJHiZNREovCb3U633cn+dZi+9fs/P4F97KxaSgrOd+akkQooP4cgWbylD2wcdeE95FKb3XZC+jCYKr+dRhqNH4y1LUhGS+tk9QSkacwRMa7Th2G7a/Lq3z89hui7JJ7eWFTMBpGS9Bfadfy3n5G2UMAIYQOudN2lZK2r9E3j5pRqEPaIMhniM6pAtTk1IDNCdLshsyE9AliIa5N6X917vM5rTzyQT45U9/9MP65rCo6n602UmO0U2O8Dsrf+hPf4lZGUhuvt+OIJb8p0tAAgYD3TAfZ65E+1xXkrMLXHtScUFkbzm+d7Hp9LWmq2i4p1qmZRD0Nkdhp3cWoGXpMgWBbypSrRoRywvP0x9NXgFp61CmW6Gw6Oip2BdMhyrv93sehaTPdAu2F5thB9ogVUxknLJ1UXoq08Ynv8l3cyBK1N9UJM2AuJ2w+xguMiMG4uXMYGis1+VaZhKd0RYvIglk6TO2BG8GUrJCfeKjSEMy1Put9cr76ylyDZx0zXReEpvv/RNWxj9FnKRy+wfQUrHVAJ1U6R9Goq/p2PifMPsWP0h+CX0gU0+wVbY7ckAQEYY9EHhGLKfVYjZGAFuH4e7F5VzLJ6koSk7fNitl+US32uySMhnEsE5CVti6Bbf7eaHSCDPlBAa7JooQBAwVJtNNtUjq7XVzhHDEWDURcsG4kuEy1f8sz/SDDPOVEKoXcGgJVXBLU1A/Ta88wT2nVJyfsrKfijIdUNv9wfGc3f+Z7gw2iWzqOEbq0K1iUogfkdFuFUlNc1fJ4jaXNbl5BwvqoFT5jYrk+sYBSAeR68AC6NxkfOfhFg2tFXVCCeut5ZbEU0rjxZ9guZH2zC/3BamZvsYALiD8YTDm/tdUg2QTONOsi4swOpaych7jjXfM7bKfYAG1IViYOrC0rh3yzQiIYNrjaUKKi5pgfBieJJd/YuGZFOTg/bPT1B6XVT2DIsYWVAvutjfABydzTKnGQVtBVbgmlYbgg6FjYIuFP9oIiHyaSVELpUc+4lkQ4LMxNnV/t4Jbm3TsWtZ3fDx0hhBQJMNJ5HQZ9jRMAgxun2/Xyk+Uwy4RHf362AsPFEGez/uCpMDgUaRcnPMP0D0PNpSgCwE8BijbRnB3G3JaMAosi53cWewWwPxI1K0P7nNT4IlV2auRPgSMaXLL5yIhp/DEJqum+6LKa6+LauFd/TXd2oT5H5KIKMGQNLivjuhgrBfacqZ9Hby1aYeFoDYCNNdPJZKYRTYa9RjF27Hk9tI+vzPLcX6oZwZoLz1VfQeVklrHWI1zdwmOlYAwtT5HZ5I3CCypCqkbTMe+oDu9ks9rf1BZl0NEAJTFrN1fvOhMTq5p3TXVISK1RopaOsDiAvR5zbAfFMIZSGjnDai1xYwJ4FY8zeJNUXXS0KW5DotLYsTKv1RblMvcLe5yI3R54gSiwwtMG9e7N7SUW9Ea952bSMUleRFQReX12ISHkBarxUqtTYisT/XwET7pvoUSwjgnT4bxmywJNuVDA6YHwuf8ng7iRzUO0cZP9NR3JUHD6hE4iNzt954+JXYQsmCAoavbWHGc3BPFIEmZsHasUna5S3zY4g98iJRaah+iJfIYyYPj/PyFGZxlHA9BLrsyys1XEAj/HcnynQzEgGdH24aoV5uXcsQIbZG24V+11qwEVrBrEfR+KOq5hsL5rfc87bMnFR1iRb+J1r+1nVmti0bz9ZaC3UAv/tyIB8o0s/eJ0LMi4jrVDIk2DXOn0ACRIZ74IIMzrj86Z4sjWKpJnTv2Mm212lF4KLG8CZtAk/m4v8TgiSfHBmK//hEHS3FRfCkcV5dc5AYK9YHyTL610LmDIHWubPZ4YMXGyjY0uF9CJ3HIJiCWPSgdkXSXvcILDFjiyIODNDtrMFqMn7fU1J4+stb+COnwNhJ8/BmnpG6oD3nVU4pkZAssf8oavrz/gEO8WXmpl6qFcRY2uM5FAFwLxo812pvUzgZ3iIdMtsgZTjOm3REQZldeo0RFe1VSoTbeJ8q7RC8/4W6ZO1LhY7zuV6XkW+wgEOQmTdrLqA9spYrQtcJj48d3zWb+yT7Hk2z+DupSeQED4Y8Rv6MRq3wBpEGlx5SfuYm9D4QJgZz/OqUYMUNe7SoY+5lf/n0NIVbWs/a0LmLX3DNfP2eBBIYPh2fOPDGdg6Ds6A+f2YE5wun7eFVzPmH+pX6ZGc1I6WJCgHVe0l3Koizc8pBlsBEZtwakLEICZS0y+dLyhmqzGbClOGlv0zFv99z7FcI+F23izrmt/FcIybKKVDlqwCIND4JQ0A7oSqb60UAjgo0G4B+Z7o2MtHiHtVFRYF4gWPHFIVCacOOTUIJ/nXSWaQtfLnsVLcbm2msGOoReuypvOTKbZDTy7D/rgk4jsOtieoEB4/iO6N2SrKQvd4w/cfHCFSt9uTUydDJrOQtJsc/3WxdYOTPfwNQZvoqcHW3/8DTwpUWMhfBvwfeKxcGfbcbmYIA52TmuKK92x5AnGqLsdHApHurV87qsJ4httyN0oYJW/DNMOOlM+ekHLaDVM8ID1DOCG8/Pc21pp31xhS83MGvjvqbdEv7W1EYYejfC5yaX7QnvaaD1HFFFi+a5EmJ/7sb0Rng8Rq5BrYb1CUNiWqHjyEy7N9iGhA6fArd99LbaYzm8JwVczX7cf1qTgaGcw+3jn9B5e2trVOQDm7VrC/BIgAJJcd1e7CMSydf0XsPDjl+72MiM7W2ww9sDo5wTTT/QBpwgZyvi9eMq+LgcMqXAKC4JrPflFt13uXgJ8p2Jv9IM6NnGNrwA3+c5ZMev/l5EDquNpTegSF/1yYreH0MagErl9aluJ1n634LjmwlDSAAJNySZ79AoyuJHdp6LLSTGb28yocHwV6O+4P7OnrcUdwkwMpjlx/AJ43jR7tf2tJyp3GifZBYn4Pvl0vYujE+2tgC+5tO5/4qrPOAqRF2SmQpJ5MRayuwxLci5T0KZazO0bfPFT+4ttng57avID5jYaN0LALmAoc8MU2VMJP8wQSjC4N+szQorvv5+L2JzLWCwgx1TgFNmk3i5O+MpQ4OhapPU7JTwV9nxqlcZ8Okbs9Ue7X3G8WBYkAG27M/3+hS6Ta1JZCJz9SK0qJspPcA5CwmgONTECrkbprSxZYeGFSh8vTk1Z2dTRY2duKBK+CqrZ1Gto/4CZq7d2mypwG+is1FDRm3OywWcukYGFnCxIoEvShClFvRBRWeMTN7aZKhOP9w11LocM0oHdDSQzYPPavNNHUOpkxC0ELwBO/N9uW5OfJbTGBkuhNilWxyJ11JQTCrCxpAAcEJBjq0y0gojIn5OTwr4+13tINzEAILDcGLO77hqSfHbL4hUDSOokYmM4adbDhEMxoymaMnwvOzc5SHWM57UOk/5jZvwK/6TELb3RH2q5Ef0P/gPjZwzuSoy+rSH/FKpg9FipCm92VsDMh8bgeIW7xERdNeDbcM8EBp0Nn/+LbpEtlogHxFF8mrcqaxUOH1Y/WL0ZYW4d8+REoSp659CXxfDNfXiNcPgWQL0Xghhgl5rug2w+YJA0wDtWshe21D/jII0lgpb4SSFNJIW90qUiLR7bNJfXBCMAA322fvVAEGa2/VBFMFyenPAxsrTwlo0C16UHFlTCKZCrr7FL1nagqxtGrHdCMhCm9fy6wsbTcqgGim9kDq3fLrFGRnHcMQWOCJF9YnLv6Au+R4HwMm6kdIE4doHiY46X5kv/PA5BjprVK2CP9cBzZ7pz4Gk9egmd4/w1FT0FuVQmFTmQFcBN0LJwp7YCr4/Tic+LuScmLQvHDCySln9ov1vr+0wOPghMyhr6sz645Zq3/4juI1INMubLhjnDDBiQ2hjpW3sMv3Bqf/Lkj5XnoPjxvKxNIw0CMvn5q1FdDTytqXbWv0URLJE1cfIZSHrhVu6ONm3K/YIg7m9ixmKFMWn8wEJiYf06ilR2kjpgwBxTPCqqoPHDH9BOlMo2ExD/ns6z5fvuvmCAG1ik/+CohSvWawMjj+YaqLsdpULZ7Ef0zLoQ70VxZDEzGLdywEpRp4r9vdfEB31QZZDONBETqzP71L5yVa08p7cqrpNpCwerEuGi+sugPYklg/U1rAfwwBsM9AxhXqhabl9QZcmEvlz/mu9agg/EqeyQg/CPH0smS0AvY6xm0HMg9k0FgetQsypz0FPslXY61sjmkUHC1IsZoM0K/LMNFPpxj0CbL5spS7ITksgdPmdt9GUceZKrdbCbsV1MMCBvPtOfmrgfGVN1KKFZUCzgyRN/sXGPtBhae487oJal1EecOsfKNRFJmEylbKQxKhx6belYB4BW1JhxXekebAPZynSxJSOqZu5D+tilwmpDWzRSfKWG8+4wrcDrQWmBb3c/xiMw+xSAZ4V6IIShf37Go+1zixYTXXkHOsIxtTN8XMMvaA0stqRwiZl+ATpnpciWG0BqIOr2e/m2sHTLD9qIjb8YdQeb3B2rSCeFsbvy3vf0TNwZ2QbOZo910eaDOOm0mLlJ6nZ0BiGcC0sFpZnLHP744nSLYOSio1s7FeJIZzlnOz4b74kSq8tO7Ru7+47MpWrT2RiCG0rcvSn47khb4Hna3WC14fxsY1/Wpes3TJlhijcBNdqcQFaUrtO6tm3CCfeRWlL1K4zNN7Mza10I7zpNRezVXq2GkVW81TRHGlLT5dlWugGXXBQbxJSLsBx/umwgM6I/qEBlwuFfLn9fImolBRM/zq9A2z/Z7C/e9JktsK/ErbqMK0XUt3aCPDDVhpa+OkLfCfU8VZpcsKgTBNdkkqCDMIfVUfUQhiAzF6OXSPy/MWwTdbKdRkcMdDj5iGftv3eNTL9j7Iy+J0gXLTkv8QsZ/8FPPv4pdKV4xL7cX37VY2arDivkJnIppJ3KdG4HLMAxX8nwo9q779AY9/Z3Iy7lDbcd2TyCnKN0hyHKN/NlFzardwkcnhK4mWwromZyuCOVYs6+ne1jsSCXogcXc9qEuDNeXEexlvb+uQEpDfGrHESAsrvoL6MkiCRQNVOrvT2smiwIars0BKdUhJvUdP2A0lTG1Q4rZvX7rm9DNzqVVo/vcxMzpxfbAHBjm3TD1evYT+M8oc/F0Z9Id1fkGnzq6rddPrihIj9ZHug+URL6LG9K2qlOiiDbidXQWlglwhBlwWVWTqUx0TDLY+Nv3j5aW+W2u6vxphWY1tljnidMEYml5vvsIwXfA0rg5968bvIS6MkE/H1MYWPz5kL0i2AuE51T6n7fsn8ha5QtJN88eFCWcQ/iYBR4826NxAdGznE1zZ6zZ0rKDrWsJWcR7GSfgiXwf0becf6albeFCD0uMF2vRz8o/DNdIEAtRaCYPQL99i3UwzS1Lmi7aJ95pCjfUPqiz7B7rRXGnHgRF6RVHze7sJm6+ygV64u9EbK6CFBfIejDnHeEKJylr0BmA6bldkrHes+SssYNQdovXi0nc/zJNNduBPeYjayusQOb+dpdc3TC7JoPQ1H63Gx02O4/s/cbx09bfZn20Ju6apOBWLErvUIzWkKKS6+8WTUbLm0xwIO/pEmHqM+xble1VpVW+nbG3Gm7DmzTzkksqj4FxTn82AqwE0hWTYPZ0N2CX0SuL+Vg9O9jKQ/MiCkdjPTF9gw6b5nIaAs0eGOiGoU92wpyOwu2BnxzS0B6v0FcmWsXG9j5CYRDpq0146Df47YWLTz3Urk5dhivXmcmyAvl2dYfqyDT4BqXxUZOX+CqX8ywO597KuHwKSc6CBAx+bfREhlBroU6octdOa7uUZjwhcHriwMjA/lFr8wkWI2yDgEr5bEW7GI3O6QUnyoM+idKCilzTfrDPJeanG6WiXNYiFw5aBLJQ/JXP3wr6mS9cEgjNtELeU6Tc8nTbQ/Hs0eHbSIoVqCY9sAnADyBkTBJ9SF8Ir1a733eUS92zEm1qzRWWh4Gc7SepehqgZmD2OVOZvoy0SSGqA6M9u4Y2B/FlwypOzgy8bKO2P8hye7owX42IhASY7lJ0eojE6hsEWBRlwUyBn/hXS4jI1K31Gw3kyCegoidcI/88SqtFVjc03jT+NRoZKXtJoKWmUsFV0aBlHZ/oSTGiY8564PREjUm6/qucEAisBH32Ow1xGPQHwu+z+Ig7TJ9IFbwRw2C2dOwSI2e3/be17lpcHbYaqObJNzUTz+Qy26AMaNe8LJQbtUi53Hv2r0YDY2HGnT8s0e/cZMusq4UzCjSWh5J17LfoC0psokLvgfFL6FlCWg1Sa6f3tgdVmgqRJtIB/6+Y1Yrzt1wdIw4USldtrG+3k3/XDmO6VK/TmX8hGAFFBtdWcUFtNaGsy/TIsXff71NMWDJVKHqUHoZtwrd41boqkkPCrz2BI3EGm/OkZI7vgmixvyrr2+gjpxqU4CIi6+pkyhAD16p2zB1QpaUnxrl6k6RaNsAUvMDeS1J0AS/TzvQNr1ZyKOvkR/qVfxhQ4MFZbddNcbX/pV8Rmp44E9ese+HH2AlhMoaoGBoNXBBK2Yb7Gxw1r3XdiDdv22st1slh5pc0ZgE0FVFZLwrg8UqArdWg6dkKfK0k5u7UwjE5du+/5Fvl6Ym9sIvCzQNtOHrN6TA4KROdm7dw+C3ajCUmsN+j0ColCGFlXWKivcMEYW52B9NBfnb2Hfq6tSnKuQNgj3fy5KKLT0D9u2+DUXJ2aRlQ5KB01xtd1KINVXq7HJ4GOJJwAQNniW7u5F4UWgzXbSVeI+/OUh/lAS1IRW52jFyU6utOjUWnknq5PxZu/xSMSXIPBepRGm0c5qn0PdFo/5gfikjnR3PcW5XrdQBMktuSGOC71LPhX+afIgWCYRhfZmKZrBsNzMlj+0CA/f5q8bB4L1trAknPXO4A6J2zMf8/1s65tsTVdFrl3DTevzXbXi+AhMVSXHgm8BSdqBG1x+QVTAhsQDNGLvmuIniecxk23/Tx38dYd3l/moLQafptyzhr9Ph+jeVvCaGb+5iZbKNO4vvi8DEft4KhpTrwGZ1FoJUjDGyq0FYoo7vGOxuXvTxTH/o5dff/nG/gq5IXenNB/1SwsrxXnvYeP9oiX0aZFXmTPPU9uvHAqPiiLHwB+covhwv/KMaiC33rv9WvJCbdm+7a/GObeaP6I5VbimJSPJLYK744dkPZ/N9ENDyj+gNPste9MsFYANNJ6CmUE2EKfNYtLRsdqXJYSZcJ6d7fXtJXjgbX4ePTCotJhAc8oV9RJAOA/kaqDIH2mcUlSkl3TSMtSAmsvcljG2Ut2CeqKToumzpeRKXrcjebrd73Ov48ZmE94E2ktj/dXiyOCCAUhZkE99A9gIAVWvuN97POrihylkJ+nDbSDg6l3jGYp+JOjytQZvJNdUIADW81KXZHWa7K8Zr5P+tKopPHEWBWTSeMdQlY53cHMlFSU5H3ztuD77S25uxu3StuOh6Q9vZuESnpKR/yGDd+IGBmncJF1kdF3PSPF/TGbyPkoVCyn9ZjeoWPxAwzBgroWQVnOfyHRcM3IQ933nci2gcRiIy80cZ2fSj+C0CyqLMKYv8lzOl9hZCCz423YHGuG0LyVJi2jA+AWgMFHVjxcT2kh7hj6/CBvsL7tbClVYb3dRlRCzKNcGy4DD0Ras85XDyt8soioI3UoQxEKmwile2kKmuKuc+CboUwE8e5K/vvlU1Hqkm3lmcChgDZ5dEgKhc6q27s5+ABML1c22CcArxUgwVinS2+iNQjjE4NO+3gaZ6Xcn7pcvnRU8+/2KAApw6NJi8UKtjlhMW019A5yb6yEttL+fTdhwgVrawnssPUjGvwqxLH7Tzs5VvPZk43tdNGHb0DhbfE4z2kRGn/DFPJDDl+qSp3gCPpGFO8cSNwPCQSFQ6WAHOagAa9fDJmY/GYQqsZzc0HwXHcDI5gQXrEdKPbWQ0DwY1d/OU5cg9kNOKFwXubNFfuFzmyQ91LM/s7CE2svEOf4RVw4XvU1GFHx0XN1UaEYJr8JazZUlejLOP98KL2p1owwWiNtbFetgOrK4ZFdpK7q7Hz8ZUZsdL0Jke9ODLh3Mnlij4gv0HGW+ris5Y9tvdStzTSvHCdLHzLG3PpUtcWVsGWZOwWiP8kIyh90iwKyhFxpK5dPi5eImuR9EcwWLyeP2+YiXjpjvTvFlkUCAxywPmdar5Z877ddPTzydcJS6PWVe28O11YQqL2Y8Vr3N4mZdOmRyvE5ywtFsKGSdtVQGsOm8A0qmEai8VjSASSboQy3UkewdUbNLBdaSYLFnEj0dx+lFR8IF71qiH16tOoaJI/P+Qj2koFQX7ykpRkf8oyOMGmEyaeLEjXKp8hswEOvMmD0sm8URcIRknatP7WHBlFOiXdy/b1NPatZMurZDBaHfcmaLdtJq9V3eirlR95lo/vXEjJWSlirRt4dlA0H+W5DPWh2CQ6SpgHdQ1vmKcOR5oSGT1C97SNXHtWAn9V5ZrflwtgqWmMiujPSQIlYJDrPem+PbWS85qXKVGwvgoByoceUpAX48ogriV6pC8Z8LpOdcyVhNENaXDz8S1mjize6eqGs1PGxwGEwcw6JTm9pRYv1sOT+TjQelXNhoYJI/2iiA4ezSA+Tuyil6H1t1TarAr/uGw7DzY12uN/lFMx+ZSbcFwkMWhzc52tIPDZnBYWvCqxwbEVFBNv+vD/vYd2hxXhMo8+QR5rGudYnXkylaFE8cId1rCoW1pSTSFGX7OeFg9CE07Jd1yld8xUuEP0Cqkr72/7GfmKpbIHkFU9BcyrPWg9XoD0p70Bf7j1b1lS2QZjYLRgjMNKOcZfIY9xMVgcthHSPinY00paVVNNx+70Asj9jETSPknYMdTqTlKFORsq2fkI92z0fzjfOpFiX5+IQxytAr+C1TXmLhYXpkou6IZFGvxBwywAH4dQVdOuhpSj32s2HaqB5ZhkrpvrkvwG8wuMId+GU5Kv84cPBcGG4Ybtt9ygeCEI2JmP0cD4rxc5b4lKufAzPLYnnW2zidGJxdkhuJEXA3KOms7Km5Cp91rSr1/o+VapM+iKi+ytkYrIyvy40xKMBtO1jceFJJDX3++DPtuPv+2uxAOvRvWshvzWpzq0N5HDEVNjY6d2J4lLf0wNOAYB7KgKh0lgv2FJH4v8Z7J/ZMPdFahVgyZ69J3iEGjWtVM5EIiAI9kT0w+1wLi5Vby4zLmcnzUjA3MB8wBwYFKw4DAhoEFLZyKWFVp54xfxna9XqtunseMi46BBR+WW9t8BwrTuw52/ujD/7rdprarA==",
                CertificatePassword = "makina502",
                ProxyUrl = "http://192.168.1.9:5049"

            };

            BrokenClient = new FileThisProxy(BrokenConfiguration);
        }

        #region Accounts

        [Fact]
        public async void GetAllAccountsPositiveResult()
        {
            IGetAccountResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccounts());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void CreateAccountPositiveResult()
        {
            try
            {
                _partnerAccountId = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                IGetAccountByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateAccount(new AccountRequest() { PartnerAccountId = _partnerAccountId }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _accountId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void GetAccountByIdPositiveResult()
        {

            IGetAccountByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountById(_accountId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_accountId));
        }

        [Fact]
        public async void UpdateAccountPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateAccount(_accountId.ToString(),
                   new AccountRequest() { EmailCampaign = true, DailyUpdate = true, EmailFailure = true, EmailSuccess = true }));

            Assert.True(true);
        }

        [Fact(Skip = "Skipped to run other test cases.")]
        public async void DeleteAccountPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).DeleteAccount(_accountId.ToString()));

            Assert.True(true);
        }

        [Fact]
        public async void UnDeleteAccountPositiveResult()
        {
            await Assert.ThrowsAsync<FileThisNetworkException>(() => new FileThisService(Client, Configuration).UndeleteAccount(_accountId.ToString()));
        }

        #endregion

        #region Accounts By Partner

        [Fact]
        public async void GetAccountsByPartnerIdPositiveResult()
        {
            try
            {
                IGetAccountResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountsByPartnerId(_partnerId.ToString()));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void CreateAccountByPartnerIdPositiveResult()
        {
            try
            {
                _partnerAccountId = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                IGetAccountByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateAccountByPartnerId(_partnerId.ToString(), new AccountRequest() { PartnerAccountId = _partnerAccountId }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _accountId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact(Skip = "Skipped to run other test cases.")]
        public async void DeleteAccountByPartnerIdPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).DeleteAccountByPartnerId(_partnerId.ToString(), _accountId.ToString()));

                Assert.True(true);
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void GetAccountByPartnerIdPositiveResult()
        {
            try
            {
                IGetAccountByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountByPartnerId(_partnerId.ToString(), _accountId.ToString()));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                Assert.True(response.Result.Id.Equals(_accountId));
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        #endregion

        #region Partners

        [Fact]
        public async void CreatePartnerPositiveResult()
        {
            await Assert.ThrowsAsync<FileThisNetworkException>(() => new FileThisService(Client, Configuration).CreatePartner(new PartnerRequest() { Name = "New FileThis" }));
        }

        [Fact]
        public async void GetAllPartnersPositiveResult()
        {
            IGetPartnerResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllPartners());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetPartnerByIdPositiveResult()
        {

            IGetPartnerByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetPartnerById(_partnerId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_partnerId));
        }

        [Fact]
        public async void UpdatePartnerPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdatePartner(_partnerId.ToString(),
                 new PartnerRequest() { HistoricalPeriod = 36, MetadataOnly = true }));

            Assert.True(true);
        }

        [Fact(Skip = "Never tried so skipping")]
        public async void DeletePartnerPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).DeletePartner(_partnerId.ToString()));

                Assert.True(true);
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        #endregion

        #region Connections

        [Fact]
        public async void CreateConnectionPositiveResult()
        {
            try
            {
                int sourceId = 100100;
                string userName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string password = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                IGetConnectionByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateConnection(false, new ConnectionRequest() { SourceId = sourceId, Username = userName, Password = password }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _connectionId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void GetAllConnectionsPositiveResult()
        {
            IGetConnectionResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllConnections());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void ConnectionByIdPositiveResult()
        {
            IGetConnectionByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetConnectionById(_connectionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_connectionId));
        }

        [Fact]
        public async void UpdateConnectionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateConnection(_connectionId.ToString(),
                 new ConnectionRequest() { Enabled = true, FetchAll = false, HistoricalPeriod = 36 }));

            Assert.True(true);
        }

        [Fact]
        public async void UpdateConnectionCredentialsPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateConnectionCredentials(_connectionId.ToString(),
                 new ConnectionCredentialsRequest() { Username = DateTime.Now.ToString("yyyyMMddHHmmssfff"), Password = DateTime.Now.ToString("yyyyMMddHHmmssfff") }));

            Assert.True(true);
        }

        [Fact]
        public async void FetchConnectionPositiveResult()
        {
            await Assert.ThrowsAsync<FileThisNetworkException>(() => new FileThisService(Client, Configuration).FetchConnection(_connectionId.ToString(), false));
        }

        [Fact(Skip = "Skipped in order to run other test cases.")]
        public async void DeleteConnectionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).DeleteConnection(_connectionId.ToString(), false));
            Assert.True(true);
        }

        #endregion

        #region Account/Connections

        [Fact]
        public async void CreateAccountConnectionPositiveResult()
        {
            try
            {
                int sourceId = 100100;
                string userName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string password = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                IGetConnectionByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateAccountConnection(_accountId.ToString(), false, new ConnectionRequest() { SourceId = sourceId, Username = userName, Password = password }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _connectionId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void GetAllAccountConnectionsPositiveResult()
        {
            IGetConnectionResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccountConnection(_accountId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void AccountConnectionByIdPositiveResult()
        {
            IGetConnectionByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountConnectionById(_accountId.ToString(), _connectionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_connectionId));
        }

        [Fact]
        public async void UpdateAccountConnectionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateAccountConnection(_accountId.ToString(), _connectionId.ToString(),
                new ConnectionRequest() { Enabled = true, FetchAll = false, HistoricalPeriod = 36 }));

            Assert.True(true);
        }

        [Fact]
        public async void UpdateAccountConnectionCredentialsPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(_accountId.ToString(), _connectionId.ToString(),
                new ConnectionCredentialsRequest() { Username = DateTime.Now.ToString("yyyyMMddHHmmssfff"), Password = DateTime.Now.ToString("yyyyMMddHHmmssfff") }));

            Assert.True(true);
        }

        [Fact]
        public async void FetchAccountConnectionPositiveResult()
        {
            await Assert.ThrowsAsync<FileThisNetworkException>(() => new FileThisService(Client, Configuration).FetchAccountConnection(_accountId.ToString(), _connectionId.ToString(), false));
        }

        [Fact(Skip = "Skipped in order to run other test cases.")]
        public async void DeleteAccountConnectionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).DeleteAccountConnection(_accountId.ToString(), _connectionId.ToString(), false));
            Assert.True(true);
        }

        #endregion

        #region Documents

        [Fact]
        public async void GetAllAccountDocumentsPositiveResult()
        {
            IGetDocumentResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccountDocuments(_accountId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetAccountDocumentByIdPositiveResult()
        {
            IGetDocumentByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountDocumentById(_accountId.ToString(), _documentId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_documentId));
        }

        [Fact]
        public async void DeleteAccountDocumentPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).DeleteAccountDocument(_accountId.ToString(), _documentId.ToString()));
            Assert.True(true);
        }

        [Fact]
        public async void GetAllDocumentsPositiveResult()
        {
            IGetDocumentResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllDocuments());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetDocumentByIdPositiveResult()
        {
            try
            {
                IGetDocumentByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetDocumentById(_documentId.ToString()));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);

                Assert.True(response.Result.Id.Equals(_documentId));
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void DeleteDocumentPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).DeleteDocument(_documentId.ToString()));

                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        [Fact]
        public async void UpdateDocumentPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).UpdateDocument(_documentId.ToString(), new DocumentRequest() { Name = "New Doc Name.pdf" }));

                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        #endregion

        #region Subscribers
        [Fact]
        public async void GetAllSubscribersPositiveResult()
        {
            IGetSubscriberResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllSubscribers());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetSubscriberByIdPositiveResult()
        {
            IGetSubscriberByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetSubscriberById(_subscriberId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_subscriberId));
        }

        [Fact]
        public async void UpdateSubscriberPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).UpdateSubscriber(_subscriberId.ToString(), new SubscriberRequest() { Name = "New - FileThis Test subscriber callback" }));
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void CreateSubscriberPositiveResult()
        {
            try
            {
                IGetSubscriberByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateSubscriber(new SubscriberRequest() { Name = "Subscriber1", Receiver = "http://localhost:5000/subscriber-callback" }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _subscriberId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void DeleteSubscriberPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).DeleteSubscriber(_subscriberId.ToString()));

                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        #endregion

        #region Subscriptions

        [Fact]
        public async void GetAllSubscriptionsPositiveResult()
        {
            IGetSubscriptionResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllSubscriptions(_subscriberId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetSubscriptionByIdPositiveResult()
        {
            IGetSubscriptionByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetSubscriptionById(_subscriberId.ToString(), _subscriptionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id.Equals(_subscriptionId));
        }

        [Fact]
        public async void DeleteSubscriptionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).DeleteSubscription(_subscriberId.ToString(), _subscriptionId.ToString()));
            Assert.True(true);
        }

        [Fact]
        public async void UpdateSubscriptionPositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).UpdateSubscription(_subscriberId.ToString(), _subscriptionId.ToString(), new SubscriptionRequest() { ChangeType = "updated" }));
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void CreateSubscriptionPositiveResult()
        {
            try
            {
                IGetSubscriptionByIdResponse response = await Task.Run(() =>
                    new FileThisService(Client, Configuration)
                    .CreateSubscription(_subscriberId.ToString(), new SubscriptionRequest() { ChangeType = "created", ResourcePattern = "/accounts/*/connections/*/interactions/*" }));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
                if (response.Result != null)
                {
                    _subscriptionId = response.Result.Id;
                }
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        #endregion

        #region Interactions

        [Fact]
        public async void GetAllInteractionsPositiveResult()
        {
            IGetInteractionResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllInteractions(_accountId.ToString(), _connectionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetInteractionByIdPositiveResult()
        {
            IGetInteractionByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetInteractionById(_accountId.ToString(), _connectionId.ToString(), _interactionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void UpdateInteractionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateInteraction(_accountId.ToString(), _connectionId.ToString(), _interactionId.ToString(), new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
            Assert.True(true);
        }

        [Fact]
        public async void GetAllAccountInteractionsPositiveResult()
        {
            IGetInteractionResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccountInteractions(_accountId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetAccountInteractionByIdPositiveResult()
        {
            IGetInteractionByIdResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAccountInteractionById(_accountId.ToString(), _interactionId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void UpdateAccountInteractionPositiveResult()
        {
            await Task.Run(() => new FileThisService(Client, Configuration).UpdateAccountInteraction(_accountId.ToString(), _interactionId.ToString(), new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
            Assert.True(true);
        }

        #endregion

        #region Sources


        [Fact]
        public async void GetAllSourcesPositiveResult()
        {
            IGetSourceResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllSources());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        #endregion

        #region Services

        [Fact]
        public async void GetAllAccountServicesPositiveResult()
        {
            IGetServiceResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccountServices(_accountId.ToString()));
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void GetAllAccountConnectionServicesPositiveResult()
        {
            try
            {
                IGetServiceResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllAccountConnectionServices(_accountId.ToString(), _connectionId.ToString()));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        [Fact]
        public async void GetAllConnectionServicesPositiveResult()
        {
            try
            {
                IGetServiceResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllConnectionServices(_connectionId.ToString()));
                Assert.NotNull(response);
                Assert.NotNull(response.Result);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }
        }

        [Fact]
        public async void GetAllServicesPositiveResult()
        {
            IGetServiceResponse response = await Task.Run(() => new FileThisService(Client, Configuration).GetAllServices());
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
        }

        [Fact]
        public async void UpdateAccountServicePositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).UpdateAccountService(_accountId.ToString(), _serviceId, new ServiceRequest() { Name = "Test Site - All in one Checking -0001" }));
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        [Fact]
        public async void UpdateServicePositiveResult()
        {
            try
            {
                await Task.Run(() => new FileThisService(Client, Configuration).UpdateService(_serviceId, new ServiceRequest() { Name = "Test Site - All in one Checking -0001" }));
                Assert.True(true);
            }
            catch (FileThisNetworkException f)
            {
                Assert.True(true, f.Response);
            }
            catch (Exception e)
            {
                Assert.True(false, $"Unexpected exception of type {e.GetType()} caught : {e.Message}");
            }

        }

        #endregion

        #region Common

        [Fact]
        public void ClientNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new FileThisService(null, Configuration));
        }

        [Fact]
        public void ConfigurationNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new FileThisService(Client, null));
        }

        #endregion

        #region Null Arguments - Accounts

        [Fact]
        public async void RequestNullTestCreateAccount()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccount(null));
        }

        [Fact]
        public async void PartnerAccountIdNullTestCreateAccount()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccount(new AccountRequest() { PartnerAccountId = null }));
        }

        [Fact]
        public async void AccountIdNullTestGetAccountById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountById(null));
        }

        [Fact]
        public async void RequestNullTestUpdateAccount()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccount(_accountId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestUpdateAccount()
        {
            _partnerAccountId = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccount(null, new AccountRequest() { PartnerAccountId = _partnerAccountId }));
        }

        [Fact]
        public async void AccountIdNullTestDeleteAccount()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccount(null));
        }

        [Fact]
        public async void AccountIdNullTestUndeleteAccount()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UndeleteAccount(null));
        }

        #endregion

        #region Null Arguments - Partners/Accounts

        [Fact]
        public async void PartnerIdNullTestGetAccountsByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountsByPartnerId(null));
        }

        [Fact]
        public async void RequestNullTestCreateAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountByPartnerId(_partnerId.ToString(), null));
        }

        [Fact]
        public async void PartnerIdNullTestCreateAccountByPartnerId()
        {
            _partnerAccountId = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountByPartnerId(null, new AccountRequest() { PartnerAccountId = _partnerAccountId }));
        }

        [Fact]
        public async void PartnerAccountIdNullTestCreateAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountByPartnerId(null, new AccountRequest() { PartnerAccountId = null }));
        }

        [Fact]
        public async void AccountIdNullTestDeleteAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountByPartnerId(_partnerId.ToString(), null));
        }

        [Fact]
        public async void PartnerIdNullTestDeleteAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountByPartnerId(null, _accountId.ToString()));
        }

        [Fact]
        public async void AccountIdNullTestGetAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountByPartnerId(_partnerId.ToString(), null));
        }

        [Fact]
        public async void PartnerIdNullTestGetAccountByPartnerId()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountByPartnerId(null, _accountId.ToString()));
        }

        #endregion

        #region Null Arguments - Partners

        [Fact]
        public async void RequestNullTestCreatePartner()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreatePartner(null));
        }

        [Fact]
        public async void PartnerIdNullTestGetPartnerById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetPartnerById(null));
        }

        [Fact]
        public async void PartnerIdNullTestDeletePartner()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeletePartner(null));
        }

        [Fact]
        public async void RequestNullTestUpdatePartner()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdatePartner(_partnerId.ToString(), null));
        }

        [Fact]
        public async void PartnerIdNullTestUpdatePartner()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdatePartner(null, new PartnerRequest() { }));
        }

        #endregion

        #region Null Arguments - Account/Connections

        [Fact]
        public async void RequestNullTestCreateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountConnection(_accountId.ToString(), false, null));
        }

        [Fact]
        public async void AccountIdNullTestCreateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountConnection(null, false, new ConnectionRequest() { SourceId = 100100, Username = "usrnm123", Password = "pass1234" }));
        }

        [Fact]
        public async void SourceIdZeroTestCreateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountConnection(_accountId.ToString(), false, new ConnectionRequest() { SourceId = 0, Username = "usrnm123", Password = "pass1234" }));
        }

        [Fact]
        public async void UserNameNullTestCreateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountConnection(_accountId.ToString(), false, new ConnectionRequest() { SourceId = 100100, Username = null, Password = "pass1234" }));
        }

        [Fact]
        public async void PasswordNullTestCreateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateAccountConnection(_accountId.ToString(), false, new ConnectionRequest() { SourceId = 100100, Username = "usrnm123", Password = null }));
        }

        // 
        [Fact]
        public async void AccountIdNullTestGetAllAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountConnection(null));
        }

        [Fact]
        public async void AccountIdNullTestGetAccountConnectionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountConnectionById(null, _connectionId.ToString()));
        }

        [Fact]
        public async void ConnectionIdNullTestGetAccountConnectionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountConnectionById(_accountId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestDeleteAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountConnection(null, _connectionId.ToString()));
        }

        [Fact]
        public async void ConnectionIdNullTestDeleteAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountConnection(_accountId.ToString(), null));
        }

        [Fact]
        public async void RequestNullTestUpdateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnection(_accountId.ToString(), _connectionId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestUpdateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnection(null, _connectionId.ToString(), new ConnectionRequest() { }));
        }

        [Fact]
        public async void ConnectionIdNullTestUpdateAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnection(_accountId.ToString(), null, new ConnectionRequest() { }));
        }

        [Fact]
        public async void RequestNullTestUpdateAccountConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(_accountId.ToString(), _connectionId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestUpdateAccountConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(null, _connectionId.ToString(), new ConnectionCredentialsRequest() { }));
        }

        [Fact]
        public async void ConnectionIdNullTestUpdateAccountConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(_accountId.ToString(), null, new ConnectionCredentialsRequest() { }));
        }

        [Fact]
        public async void UserNameNullTestUpdateAccountConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(_accountId.ToString(), _connectionId.ToString(), new ConnectionCredentialsRequest() { Username = null, Password = "newpass" }));
        }

        [Fact]
        public async void PasswordNullTestUpdateAccountConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountConnectionCredentials(_accountId.ToString(), _connectionId.ToString(), new ConnectionCredentialsRequest() { Username = "newusername", Password = null }));
        }

        [Fact]
        public async void AccountIdNullTestFetchAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).FetchAccountConnection(null, _connectionId.ToString(), false));
        }

        [Fact]
        public async void ConnectionIdNullTestFetchAccountConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).FetchAccountConnection(_accountId.ToString(), null, false));
        }

        #endregion

        #region Null Arguments - Connections

        [Fact]
        public async void RequestNullTestCreateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateConnection(false, null));
        }

        [Fact]
        public async void SourceIdZeroTestCreateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateConnection(false, new ConnectionRequest() { SourceId = 0, Username = "usrnm123", Password = "pass1234" }));
        }

        [Fact]
        public async void UserNameNullTestCreateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateConnection(false, new ConnectionRequest() { SourceId = 100100, Username = null, Password = "pass1234" }));
        }

        [Fact]
        public async void PasswordNullTestCreateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateConnection(false, new ConnectionRequest() { SourceId = 100100, Username = "usrnm123", Password = null }));
        }

        [Fact]
        public async void ConnectionIdNullTestGetConnectionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetConnectionById(null));
        }

        [Fact]
        public async void ConnectionIdNullTestDeleteConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteConnection(null));
        }

        [Fact]
        public async void RequestNullTestUpdateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnection(_connectionId.ToString(), null));
        }

        [Fact]
        public async void ConnectionIdNullTestUpdateConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnection(null, new ConnectionRequest() { }));
        }

        [Fact]
        public async void RequestNullTestUpdateConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnectionCredentials(_connectionId.ToString(), null));
        }

        [Fact]
        public async void ConnectionIdNullTestUpdateConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnectionCredentials(null, new ConnectionCredentialsRequest() { }));
        }

        [Fact]
        public async void UserNameNullTestUpdateConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnectionCredentials(_connectionId.ToString(), new ConnectionCredentialsRequest() { Username = null, Password = "newpass" }));
        }

        [Fact]
        public async void PasswordNullTestUpdateConnectionCredentials()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateConnectionCredentials(_connectionId.ToString(), new ConnectionCredentialsRequest() { Username = "newusername", Password = null }));
        }

        [Fact]
        public async void ConnectionIdNullTestFetchConnection()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).FetchConnection(null, false));
        }

        #endregion

        #region Null Arguments - Documents

        [Fact]
        public async void AccountIdNullTestGetAllAccountDocuments()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountDocuments(null));
        }

        [Fact]
        public async void AccountIdNullTestGetAccountDocumentById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountDocumentById(null, _documentId.ToString()));
        }

        [Fact]
        public async void DocumentIdNullTestGetAccountDocumentById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountDocumentById(_accountId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestDeleteAccountDocument()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountDocument(null, _documentId.ToString()));
        }

        [Fact]
        public async void DocumentIdNullTestDeleteAccountDocument()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteAccountDocument(_accountId.ToString(), null));
        }

        [Fact]
        public async void DocumentIdNullTestGetDocumentById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetDocumentById(null));
        }

        [Fact]
        public async void DocumentIdNullTestDeleteDocument()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteDocument(null));
        }

        [Fact]
        public async void DocumentIdNullTestUpdateDocument()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateDocument(null, new DocumentRequest() { Name = "Next Doc.pdf" }));
        }

        [Fact]
        public async void RequestNullTestUpdateDocument()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateDocument(_documentId.ToString(), null));
        }
        #endregion

        #region Null Arguments - Subscribers

        [Fact]
        public async void SubscriberIdNullTestGetSubscriberById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetSubscriberById(null));
        }

        [Fact]
        public async void RequestNullTestCreateSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscriber(null));
        }

        [Fact]
        public async void NameNullTestCreateSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscriber(new SubscriberRequest() { Name = null }));
        }

        [Fact]
        public async void ReceiverNullTestCreateSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscriber(new SubscriberRequest() { Name = "New Name", Receiver = null }));
        }


        [Fact]
        public async void SubscriberIdNullTestUpdateSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateSubscriber(null, new SubscriberRequest() { }));
        }

        [Fact]
        public async void RequestNullTestUpdateSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateSubscriber(_subscriberId.ToString(), null));
        }

        [Fact]
        public async void SubscriberIdNullTestDeleteSubscriber()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteSubscriber(null));
        }

        #endregion

        #region Null Arguments - Subscriptions

        [Fact]
        public async void SubscriberIdNullTestGetAllSubscriptions()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllSubscriptions(null));
        }

        [Fact]
        public async void SubscriberIdNullTestCreateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscription(null, new SubscriptionRequest() { }));
        }

        [Fact]
        public async void RequestNullTestCreateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscription(_subscriberId.ToString(), null));
        }

        [Fact]
        public async void ChangeTypeNullTestCreateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscription(_subscriberId.ToString(), new SubscriptionRequest() { ChangeType = null, ResourcePattern = "/accounts/*/connections/*/interactions/*" }));
        }

        [Fact]
        public async void ResourcePatternNullTestCreateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).CreateSubscription(_subscriberId.ToString(), new SubscriptionRequest() { ChangeType = "updated", ResourcePattern = null }));
        }

        [Fact]
        public async void SubscriberIdNullTestGetSubscriptionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetSubscriptionById(null, _subscriptionId.ToString()));
        }

        [Fact]
        public async void SubscriptionIdNullTestGetSubscriptionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetSubscriptionById(_subscriberId.ToString(), null));
        }

        [Fact]
        public async void SubscriberIdNullTestUpdateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateSubscription(null, _subscriptionId.ToString(), new SubscriptionRequest() { ChangeType = "created" }));
        }

        [Fact]
        public async void SubscriptionIdNullTestUpdateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateSubscription(_subscriberId.ToString(), null, new SubscriptionRequest() { ChangeType = "created" }));
        }

        [Fact]
        public async void RequestNullTestUpdateSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateSubscription(_subscriberId.ToString(), _subscriptionId.ToString(), null));
        }

        [Fact]
        public async void SubscriberIdNullTestDeleteSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteSubscription(null, _subscriptionId.ToString()));
        }

        [Fact]
        public async void SubscriptionIdNullTestDeleteSubscription()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).DeleteSubscription(_subscriberId.ToString(), null));
        }


        #endregion

        #region Null Argument - Interactions

        [Fact]
        public async void AccountIdNullTestGetAllInteractions()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllInteractions(null, _connectionId.ToString()));
        }

        [Fact]
        public async void ConnectionIdNullTestGetAllInteractions()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllInteractions(_accountId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestGetInteractionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetInteractionById(null, _connectionId.ToString(), _interactionId.ToString()));
        }

        [Fact]
        public async void ConnectionIdNullTestGetInteractionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetInteractionById(_accountId.ToString(), null, _interactionId.ToString()));
        }

        [Fact]
        public async void InteractionIdNullTestGetInteractionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetInteractionById(_accountId.ToString(), _connectionId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestUpdateInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateInteraction(null, _connectionId.ToString(), _interactionId.ToString(), new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
        }

        [Fact]
        public async void ConnectionIdNullTestUpdateInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateInteraction(_accountId.ToString(), null, _interactionId.ToString(), new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
        }

        [Fact]
        public async void InteractionIdNullTestUpdateInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateInteraction(_accountId.ToString(), _connectionId.ToString(), null, new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
        }

        [Fact]
        public async void RequestNullTestUpdateInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateInteraction(_accountId.ToString(), _connectionId.ToString(), _interactionId.ToString(), null));
        }

        // account interaction
        [Fact]
        public async void AccountIdNullTestGetAllAccountInteractions()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountInteractions(null));
        }


        [Fact]
        public async void AccountIdNullTestGetAccountInteractionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountInteractionById(null, _interactionId.ToString()));
        }


        [Fact]
        public async void InteractionIdNullTestGetAccountInteractionById()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAccountInteractionById(_accountId.ToString(), null));
        }

        [Fact]
        public async void AccountIdNullTestUpdateAccountInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountInteraction(null, _interactionId.ToString(), new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
        }


        [Fact]
        public async void InteractionIdNullTestUpdateAccountInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountInteraction(_accountId.ToString(), null, new InteractionResponseRequest(new ResponsePart[] { new ResponsePart() { Id = "dXNlcm5hbWU=", Value = "abcdxyzn" } }) { Id = "1888073" }));
        }

        [Fact]
        public async void RequestNullTestUpdateAccountInteraction()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountInteraction(_accountId.ToString(), _interactionId.ToString(), null));
        }
        #endregion

        #region Null Argument - Services

        [Fact]
        public async void AccountIdNullTestGetAllAccountServices()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountServices(null));
        }

        [Fact]
        public async void AccountIdNullTestGetAllAccountConnectionServices()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountConnectionServices(null, _connectionId.ToString()));

        }

        [Fact]
        public async void ConnectionIdNullTestGetAllAccountConnectionServices()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllAccountConnectionServices(_accountId.ToString(), null));

        }

        [Fact]
        public async void ConnectionIdNullTestGetAllConnectionServices()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).GetAllConnectionServices(null));

        }

        [Fact]
        public async void AccountIdNullTestUpdateAccountService()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountService(null, _serviceId, new ServiceRequest() { }));

        }

        [Fact]
        public async void ServiceIdNullTestUpdateAccountService()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountService(_accountId.ToString(), null, new ServiceRequest() { }));

        }


        [Fact]
        public async void RequestNullTestUpdateAccountService()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateAccountService(_accountId.ToString(), _serviceId, null));

        }

        [Fact]
        public async void ServiceIdNullTestUpdateService()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateService(null, new ServiceRequest() { }));

        }

        [Fact]
        public async void RequestNullTestUpdateService()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => new FileThisService(Client, Configuration).UpdateService(_serviceId, null));

        }

        #endregion

    }
}
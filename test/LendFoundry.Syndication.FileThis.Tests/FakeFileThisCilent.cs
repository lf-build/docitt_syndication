﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using LendFoundry.Syndication.TransUnion.Proxy;
//using LendFoundry.Syndication.TransUnion.Request;
//using LendFoundry.Syndication.TransUnion.Response;

//namespace LendFoundry.Syndication.TransUnion.Tests
//{
//    public class FakeTransUnionCilent : ITransUnionClient
//    {
//        public string GetPrintableResponse(Request.creditBureau request)
//        {
//            return "Printable Report";
//        }

//        public Response.CreditBureau GetResponse(Request.creditBureau request)
//        {
//            Response.CreditBureau returnValue = new Response.CreditBureau();

//            returnValue.Document = "Document";
//            returnValue.SchemaLocation = "SchemaLocation";
//            returnValue.Version = "Version";
//            returnValue.Xmlns = "Xmlns";
//            returnValue.Xsi = "Xsi";

//            returnValue.TransactionControl = new Response.TransactionControl();
//            returnValue.TransactionControl.UserRefNumber = "UserRefNumber";
//            returnValue.TransactionControl.Options = new Response.Options();
//            returnValue.TransactionControl.Options.Country = "Country";
//            returnValue.TransactionControl.Options.Language = "Language";
//            returnValue.TransactionControl.Subscriber = new Response.Subscriber();
//            returnValue.TransactionControl.Subscriber.IndustryCode = "IndustryCode";
//            returnValue.TransactionControl.Subscriber.InquirySubscriberPrefixCode = "InquirySubscriberPrefixCode";
//            returnValue.TransactionControl.Subscriber.MemberCode = "MemberCode";
//            returnValue.TransactionControl.Subscriber.Name = new Response.Name();
//            returnValue.TransactionControl.Subscriber.Name.Person = new Response.Person();
//            returnValue.TransactionControl.Subscriber.Name.Person.First = "First";
//            returnValue.TransactionControl.Subscriber.Name.Person.Last = "Last";
//            returnValue.TransactionControl.Subscriber.Name.Person.Middle = "Middle";
//            returnValue.TransactionControl.Subscriber.Name.Person.Unparsed = "Unparsed";
//            returnValue.TransactionControl.Subscriber.Name.Qualifier = "Qualifier";
//            returnValue.TransactionControl.Subscriber.Name.Source = "Source";
//            returnValue.TransactionControl.Subscriber.Name.Unparsed = "Unparsed";
//            returnValue.TransactionControl.Tracking = new Response.Tracking();
//            returnValue.TransactionControl.Tracking.TransactionTimeStamp = "TransactionTimeStamp";
//            returnValue.TransactionControl.UserRefNumber = "UserRefNumber";

//            returnValue.Product = new Response.Product();
//            returnValue.Product.Code = "Code";
//            returnValue.Product.Subject = new Response.Subject();
//            returnValue.Product.Subject.Number = "Number";
//            returnValue.Product.Subject.SubjectRecord = new Response.SubjectRecord();

//            returnValue.Product.Subject.SubjectRecord.AddOnProduct = new List<Response.AddOnProduct>();
//            Response.AddOnProduct addOnProduct = new Response.AddOnProduct();
//            addOnProduct.ArmAlert = new Response.ArmAlert();
//            addOnProduct.ArmAlert.SearchStatus = "SearchStatus";
//            addOnProduct.AuthUserAlert = new Response.AuthUserAlert();
//            addOnProduct.AuthUserAlert.SearchStatus = "SearchStatus";
//            addOnProduct.HighRiskFraudAlert = new Response.HighRiskFraudAlert();
//            addOnProduct.HighRiskFraudAlert.IdentificationIssuance = new List<Response.IdentificationIssuance>();
//            Response.IdentificationIssuance identificationIssuance = new Response.IdentificationIssuance();
//            identificationIssuance.AgeObtained = new Response.AgeObtained();
//            identificationIssuance.AgeObtained.RangeEnd = "RangeEnd";
//            identificationIssuance.AgeObtained.RangeStart = "RangeStart";
//            identificationIssuance.AlertMessageCode = "AlertMessageCode";
//            identificationIssuance.Source = "Source";
//            identificationIssuance.State = "State";
//            identificationIssuance.Type = "Type";
//            identificationIssuance.YearRange = new Response.YearRange();
//            identificationIssuance.YearRange.StartYear = "StartYear";
//            identificationIssuance.YearRange.EndYear = "EndYear";
//            addOnProduct.HighRiskFraudAlert.IdentificationIssuance.Add(identificationIssuance);
//            addOnProduct.HighRiskFraudAlert.InquiryHistory = new Response.InquiryHistory();
//            addOnProduct.HighRiskFraudAlert.InquiryHistory.InquiryWithCurrentInputCount = "InquiryWithCurrentInputCount";
//            addOnProduct.HighRiskFraudAlert.InquiryHistory.MaxInquiryCount = "MaxInquiryCount";
//            addOnProduct.HighRiskFraudAlert.InquiryHistory.MessageCode = "MessageCode";
//            addOnProduct.HighRiskFraudAlert.InquiryHistory.Timeframe = "Timeframe";
//            addOnProduct.ScoreModel = new Response.ScoreModel();
//            addOnProduct.ScoreModel.Score = new Response.Score();
//            addOnProduct.ScoreModel.Score.DerogatoryAlert = "DerogatoryAlert";
//            addOnProduct.ScoreModel.Score.Factors = new Response.Factors();
//            addOnProduct.ScoreModel.Score.Factors.Factor = new List<Response.Factor>();
//            Response.Factor factor = new Response.Factor();
//            factor.Code = "Code";
//            factor.Rank = "Rank";
//            addOnProduct.ScoreModel.Score.Factors.Factor.Add(factor);
//            addOnProduct.ScoreModel.Score.FileInquiriesImpactedScore = "FileInquiriesImpactedScore";
//            addOnProduct.ScoreModel.Score.Results = "Results";
//            addOnProduct.Status = "Status";
//            returnValue.Product.Subject.SubjectRecord.AddOnProduct.Add(addOnProduct);

//            Response.AddOnProduct addOnProduct2 = new Response.AddOnProduct();
//            addOnProduct2.ArmAlert = new Response.ArmAlert();
//            addOnProduct2.ArmAlert.SearchStatus = "SearchStatus2";
//            addOnProduct2.AuthUserAlert = new Response.AuthUserAlert();
//            addOnProduct2.AuthUserAlert.SearchStatus = "SearchStatus2";
//            addOnProduct2.HighRiskFraudAlert = new Response.HighRiskFraudAlert();
//            addOnProduct2.HighRiskFraudAlert.IdentificationIssuance = new List<Response.IdentificationIssuance>();
//            Response.IdentificationIssuance identificationIssuance2 = new Response.IdentificationIssuance();
//            identificationIssuance2.AgeObtained = new Response.AgeObtained();
//            identificationIssuance2.AgeObtained.RangeEnd = "RangeEnd2";
//            identificationIssuance2.AgeObtained.RangeStart = "RangeStart2";
//            identificationIssuance2.AlertMessageCode = "AlertMessageCode2";
//            identificationIssuance2.Source = "Source2";
//            identificationIssuance2.State = "State2";
//            identificationIssuance2.Type = "Type2";
//            identificationIssuance2.YearRange = new Response.YearRange();
//            identificationIssuance2.YearRange.StartYear = "StartYear2";
//            identificationIssuance2.YearRange.EndYear = "EndYear2";
//            addOnProduct2.HighRiskFraudAlert.IdentificationIssuance.Add(identificationIssuance2);
//            addOnProduct2.HighRiskFraudAlert.InquiryHistory = new Response.InquiryHistory();
//            addOnProduct2.HighRiskFraudAlert.InquiryHistory.InquiryWithCurrentInputCount = "InquiryWithCurrentInputCount2";
//            addOnProduct2.HighRiskFraudAlert.InquiryHistory.MaxInquiryCount = "MaxInquiryCount2";
//            addOnProduct2.HighRiskFraudAlert.InquiryHistory.MessageCode = "MessageCode2";
//            addOnProduct2.HighRiskFraudAlert.InquiryHistory.Timeframe = "Timeframe2";
//            addOnProduct2.ScoreModel = new Response.ScoreModel();
//            addOnProduct2.ScoreModel.Score = new Response.Score();
//            addOnProduct2.ScoreModel.Score.DerogatoryAlert = "DerogatoryAlert2";
//            addOnProduct2.ScoreModel.Score.Factors = new Response.Factors();
//            addOnProduct2.ScoreModel.Score.Factors.Factor = new List<Response.Factor>();
//            Response.Factor factor2 = new Response.Factor();
//            factor2.Code = "Code2";
//            factor2.Rank = "Rank2";
//            addOnProduct2.ScoreModel.Score.Factors.Factor.Add(factor2);
//            addOnProduct2.ScoreModel.Score.FileInquiriesImpactedScore = "FileInquiriesImpactedScore2";
//            addOnProduct2.ScoreModel.Score.Results = "Results2";
//            addOnProduct2.Status = "Status2";

//            returnValue.Product.Subject.SubjectRecord.AddOnProduct.Add(addOnProduct2);

//            returnValue.Product.Subject.SubjectRecord.ConsumerFileData = new Response.ConsumerFileData();
//            returnValue.Product.Subject.SubjectRecord.ConsumerFileData.Statement = new Response.Statement();
//            returnValue.Product.Subject.SubjectRecord.ConsumerFileData.Statement.Text = "Text";
//            returnValue.Product.Subject.SubjectRecord.ConsumerID = "ConsumerID";
//            returnValue.Product.Subject.SubjectRecord.Custom = new Response.Custom();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit = new Response.Credit();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection = new Response.Collection();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.AccountNumber = "AccountNumber";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.AccountRating = "AccountRating";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.CurrentBalance = "CurrentBalance";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.DateEffective = new Response.DateEffective()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.DateOpened = new Response.DateOpened()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.ECOADesignator = "ECOADesignator";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Original = new Response.Original();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Original.Balance = "Balance";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Original.CreditGrantor = new Response.CreditGrantor();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Original.CreditGrantor.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Original.CreditorClassification = "CreditorClassification";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.PastDue = "PastDue";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.PortfolioType = "PortfolioType";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Remark = new Response.Remark();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Remark.Code = "Code";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Remark.Type = "Type";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber = new Response.Subscriber();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.IndustryCode = "IndustryCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.InquirySubscriberPrefixCode = "InquirySubscriberPrefixCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.MemberCode = "MemberCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name = new Response.Name();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Person = new Response.Person();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Person.First = "First";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Person.Last = "Last";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Person.Middle = "Middle";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Person.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Qualifier = "Qualifier";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Source = "Source";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.Subscriber.Name.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Collection.UpdateMethod = "UpdateMethod";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary = new Response.CreditSummary();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.InstallmentAmount = new Response.InstallmentAmount()
//            {
//                CreditLimit = "CreditLimit",
//                CurrentBalance = "CurrentBalance",
//                HighCredit = "HighCredit",
//                MonthlyPayment = "MonthlyPayment",
//                PastDue = "PastDue"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.MortgageAmount = new Response.MortgageAmount()
//            {
//                CreditLimit = "CreditLimit",
//                CurrentBalance = "CurrentBalance",
//                HighCredit = "HighCredit",
//                MonthlyPayment = "MonthlyPayment",
//                PastDue = "PastDue"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.OpenAmount = new Response.OpenAmount()
//            {
//                CreditLimit = "CreditLimit",
//                CurrentBalance = "CurrentBalance",
//                HighCredit = "HighCredit",
//                MonthlyPayment = "MonthlyPayment",
//                PastDue = "PastDue"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.RecordCounts = new Response.RecordCounts()
//            {
//                PublicRecordCount = "PublicRecordCount",
//                CollectionCount = "CollectionCount",
//                TotalTradeCount = "TotalTradeCount",
//                NegativeTradeCount = "NegativeTradeCount",
//                HistoricalNegativeTradeCount = "HistoricalNegativeTradeCount",
//                HistoricalNegativeOccurrencesCount = "HistoricalNegativeOccurrencesCount",
//                RevolvingTradeCount = "RevolvingTradeCount",
//                InstallmentTradeCount = "InstallmentTradeCount",
//                MortgageTradeCount = "MortgageTradeCount",
//                OpenTradeCount = "OpenTradeCount",
//                UnspecifiedTradeCount = "UnspecifiedTradeCount",
//                TotalInquiryCount = "TotalInquiryCount",
//                ReportingPeriod = "ReportingPeriod"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.RevolvingAmount = new Response.RevolvingAmount()
//            {
//                PercentAvailableCredit = "PercentAvailableCredit",
//                CreditLimit = "CreditLimit",
//                CurrentBalance = "CurrentBalance",
//                HighCredit = "HighCredit",
//                MonthlyPayment = "MonthlyPayment",
//                PastDue = "PastDue"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.CreditSummary.TotalAmount = new Response.TotalAmount()
//            {
//                CreditLimit = "CreditLimit",
//                CurrentBalance = "CurrentBalance",
//                HighCredit = "HighCredit",
//                MonthlyPayment = "MonthlyPayment",
//                PastDue = "PastDue"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Inquiry = new List<Response.Inquiry>();
//            Response.Inquiry inquiry = new Response.Inquiry();
//            inquiry.Date = new Response.Date()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            inquiry.ECOADesignator = "ECOADesignator";
//            inquiry.Subscriber = new Response.Subscriber();
//            inquiry.Subscriber.IndustryCode = "IndustryCode";
//            inquiry.Subscriber.InquirySubscriberPrefixCode = "InquirySubscriberPrefixCode";
//            inquiry.Subscriber.MemberCode = "MemberCode";
//            inquiry.Subscriber.Name = new Response.Name();
//            inquiry.Subscriber.Name.Person = new Response.Person();
//            inquiry.Subscriber.Name.Person.First = "First";
//            inquiry.Subscriber.Name.Person.Last = "Last";
//            inquiry.Subscriber.Name.Person.Middle = "Middle";
//            inquiry.Subscriber.Name.Person.Unparsed = "Unparsed";
//            inquiry.Subscriber.Name.Qualifier = "Qualifier";
//            inquiry.Subscriber.Name.Source = "Source";
//            inquiry.Subscriber.Name.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Inquiry.Add(inquiry);
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord = new Response.PublicRecord();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Attorney = "Attorney";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.DateFiled = new Response.DateFiled()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.DatePaid = new Response.DatePaid()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.DateReported = new Response.DateReported()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.DocketNumber = "DocketNumber";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.ECOADesignator = "ECOADesignator";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Liabilities = "Liabilities";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Plaintiff = "Plaintiff";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Source = new Response.Source()
//            {
//                Type = "Type"
//            };
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber = new Response.Subscriber();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.IndustryCode = "IndustryCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.InquirySubscriberPrefixCode = "InquirySubscriberPrefixCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.MemberCode = "MemberCode";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name = new Response.Name();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Person = new Response.Person();
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Person.First = "First";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Person.Last = "Last";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Person.Middle = "Middle";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Person.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Qualifier = "Qualifier";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Source = "Source";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Subscriber.Name.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.PublicRecord.Type = "Type";

//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Trade = new List<Response.Trade>();
//            Response.Trade trade = new Response.Trade();
//            trade.Account = new Response.Account();
//            trade.Account.Type = "Type";
//            trade.AccountNumber = "AccountNumber";
//            trade.AccountRating = "AccountRating";
//            trade.ClosedIndicator = "ClosedIndicator";
//            trade.CreditLimit = "CreditLimit";
//            trade.CurrentBalance = "CurrentBalance";
//            trade.DateClosed = new Response.DateClosed()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            trade.DateEffective = new Response.DateEffective()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            trade.DateOpened = new Response.DateOpened()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            trade.ECOADesignator = "ECOADesignator";
//            trade.HighCredit = "HighCredit";
//            trade.PastDue = "PastDue";
//            trade.PaymentHistory = new Response.PaymentHistory();
//            trade.PaymentHistory.HistoricalCounters = new Response.HistoricalCounters();
//            trade.PaymentHistory.HistoricalCounters.Late30DaysTotal = "Late30DaysTotal";
//            trade.PaymentHistory.HistoricalCounters.Late60DaysTotal = "Late60DaysTotal";
//            trade.PaymentHistory.HistoricalCounters.Late90DaysTotal = "Late90DaysTotal";
//            trade.PaymentHistory.HistoricalCounters.MonthsReviewedCount = "MonthsReviewedCount";
//            trade.PaymentHistory.MaxDelinquency = new Response.MaxDelinquency();
//            trade.PaymentHistory.MaxDelinquency.Earliest = "Earliest";
//            trade.PaymentHistory.PaymentPattern = new Response.PaymentPattern();
//            trade.PaymentHistory.PaymentPattern.StartDate = new Response.StartDate()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            trade.PaymentHistory.PaymentPattern.Text = "Text";
//            trade.PortfolioType = "PortfolioType";
//            trade.Remark = new Response.Remark()
//            {
//                Code = "Code",
//                Type = "Type"
//            };
//            trade.Subscriber = new Response.Subscriber();
//            trade.Subscriber.IndustryCode = "IndustryCode";
//            trade.Subscriber.InquirySubscriberPrefixCode = "InquirySubscriberPrefixCode";
//            trade.Subscriber.MemberCode = "MemberCode";
//            trade.Subscriber.Name = new Response.Name();
//            trade.Subscriber.Name.Person = new Response.Person();
//            trade.Subscriber.Name.Person.First = "First";
//            trade.Subscriber.Name.Person.Last = "Last";
//            trade.Subscriber.Name.Person.Middle = "Middle";
//            trade.Subscriber.Name.Person.Unparsed = "Unparsed";
//            trade.Subscriber.Name.Qualifier = "Qualifier";
//            trade.Subscriber.Name.Source = "Source";
//            trade.Subscriber.Name.Unparsed = "Unparsed";
//            trade.Terms = new Response.Terms();
//            trade.Terms.PaymentFrequency = "PaymentFrequency";
//            trade.Terms.PaymentScheduleMonthCount = "PaymentScheduleMonthCount";
//            trade.Terms.ScheduledMonthlyPayment = "ScheduledMonthlyPayment";
//            trade.UpdateMethod = "UpdateMethod";

//            returnValue.Product.Subject.SubjectRecord.Custom.Credit.Trade.Add(trade);

//            returnValue.Product.Subject.SubjectRecord.FileNumber = "FileNumber";
//            returnValue.Product.Subject.SubjectRecord.FileSummary = new Response.FileSummary();
//            returnValue.Product.Subject.SubjectRecord.FileSummary.ConsumerStatementIndicator = "ConsumerStatementIndicator";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus = new Response.CreditDataStatus();
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.Disputed = "Disputed";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.DoNotPromote = new Response.DoNotPromote();
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.DoNotPromote.Indicator = "Indicator";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.Freeze = new Response.Freeze();
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.Freeze.Indicator = "Indicator";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.Minor = "Minor";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.CreditDataStatus.Suppressed = "Suppressed";

//            returnValue.Product.Subject.SubjectRecord.FileSummary.FileHitIndicator = "FileHitIndicator";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.InFileSinceDate = new Response.InFileSinceDate()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            returnValue.Product.Subject.SubjectRecord.FileSummary.Market = "Market";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.SsnMatchIndicator = "SsnMatchIndicator";
//            returnValue.Product.Subject.SubjectRecord.FileSummary.Submarket = "Submarket";
//            returnValue.Product.Subject.SubjectRecord.Indicative = new Response.Indicative();
//            returnValue.Product.Subject.SubjectRecord.Indicative.Address = new List<Response.Address>();
//            Response.Address address = new Response.Address();
//            address.DateReported = new Response.DateReported()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            address.Location = new Response.Location()
//            {
//                City = "City",
//                State = "State",
//                ZipCode = "ZipCode"
//            };
//            address.Qualifier = "Qualifier";
//            address.Source = "Source";
//            address.Status = "Status";
//            address.Street = new Response.Street();
//            address.Street.Name = "Name";
//            address.Street.Number = "Number";
//            address.Street.PreDirectional = "PreDirectional";
//            address.Street.Type = "Type";

//            returnValue.Product.Subject.SubjectRecord.Indicative.Address.Add(address);

//            returnValue.Product.Subject.SubjectRecord.Indicative.DateOfBirth = new Response.DateOfBirth()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };

//            returnValue.Product.Subject.SubjectRecord.Indicative.Employment = new List<Response.Employment>();
//            Response.Employment employment = new Response.Employment();
//            employment.DateHired = new Response.DateHired()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            employment.DateOnFileSince = new Response.DateOnFileSince()
//            {
//                EstimatedCentury = "EstimatedCentury",
//                EstimatedDay = "EstimatedDay",
//                EstimatedMonth = "EstimatedMonth",
//                EstimatedYear = "EstimatedYear",
//                Text = "Text"
//            };
//            employment.Employer = new Response.Employer()
//            {
//              Unparsed = "Unparsed"
//            };
//            employment.Occupation = "Occupation";
//            employment.Source = "Source";

//            returnValue.Product.Subject.SubjectRecord.Indicative.Employment.Add(employment);

//            returnValue.Product.Subject.SubjectRecord.Indicative.Name = new List<Response.Name>();
//            Response.Name name = new Response.Name();
//            name.Person = new Response.Person();
//            name.Person.First = "First";
//            name.Person.Last = "Last";
//            name.Person.Middle = "Middle";
//            name.Person.Unparsed = "Unparsed";
//            name.Qualifier = "Qualifier";
//            name.Source = "Source";
//            name.Unparsed = "Unparsed";
//            returnValue.Product.Subject.SubjectRecord.Indicative.Name.Add(name);

//            returnValue.Product.Subject.SubjectRecord.Indicative.SocialSecurity = new Response.SocialSecurity();
//            returnValue.Product.Subject.SubjectRecord.Indicative.SocialSecurity.Number = "Number";
//            returnValue.Product.Subject.SubjectRecord.Indicative.SocialSecurity.Source = "Source";

//            return returnValue;
//        }
//    }
//}
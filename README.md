# Service Name : docitt_syndication

- The purpose of this service is to fetching the statements from the different institution like banking, tax , w2's , paystubs etc.

## Getting Started

- Require environment setup.
- Repository access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands.
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc LendFoundry.Email.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore LendFoundry.Email.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\LendFoundry.Email.Api\LendFoundry.Email.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which is shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  docittcapacityconnect
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/docittcapacityconnect
	* Sample configurations :
	
		```
			{
				"Certificate": "",
				"CertificatePassword": "",
				"ProxyUrl": "",
				"ServiceUrl": "{{server_url}}:5039",
				"Ticket": "",
				"CompanyId": <company_id>,
				"DefaultInstitutions": <defaultinstitutions>// Ex. ["Bank of America"],
				"DefaultTaxInstitutions": <defaulttaxinstitutions>// Ex. [		"H&R Block",		"TurboTax"	],
				"DefaultW2Institutions":  <defaultw2institutions_array>// Ex. [		"ADP Portal"	],
				"IsSimulatorOn": true,
				"UseSimulatorFor": {
					"Banking": false,
					"TaxReturn": false,
					"W2PayStub": false,
					"FileThisBanking": false
				},
				"MaxFilesToRequiredConditionRequest": 12,
				"FavouriteInstitutions": [{
						"description": "TurboTax",
						"institutionId": "603",
						"name": "TurboTax",
						"isDefault": true,
						"logo": "",
						"type": "TaxReturn"
					}
				],
				"DefaultProviderId": <id_string>,
				"DaefaultChallengeAnswer": <daefaultchallengeanswer>,
				"SampleBankAccountResponse": <sample_response_type_json>,
				"AcceptedBankPasswords": <accepted_passwords>,
				"SampleInvalidCredentialResponse": <sample_response_type_for_invalidcredential_json>,
				"SampleTaxW2DocumentResponse": <sample_taxw2_document_response>,
				"NoOfYearsW2Docments": 2,
				"NoOfPayStubDocments": 4,
				"MaxFileSizeInMB": "5",
				"PermissibleFileTypes": "pdf,jpg,png,gif,doc,docx,xls,xlsx",
				"WriteExplanationTemplates": {
					"Banking": {
						"TemplateName": <template_name>,
						"Version": <version>,
						"TemplateFormat": <format>
					}
				},
				"BankTransactionPeriodInDays": 60,
				"BankTransactionCountPerPage": 0,
				"Database": ""
			}
		```

## Service Rules

- NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by syndication service

NA

## External Services called

NA

## Environment Variables used by syndication service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different envirounment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the envirounment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\\<api_project>\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": <configuration name>,
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog